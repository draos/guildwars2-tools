unit uMain;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Grids, ExtCtrls, ComCtrls, Menus, uWebAPI, simpleinternet, xquery,
  uPrediction, LCLType;

type
  { TWvWPrediction }
  TWvWPrediction = class(TForm)
    // Controls
    Chances    :TStringGrid;
    Clock      :TTimer;
    MainMenu: TMainMenu;
    ExitItem: TMenuItem;
    SaveItem: TMenuItem;
    SingleView: TPanel;
    SplittedView: TPanel;
    ReloadItem: TMenuItem;
    EUItem: TMenuItem;
    NAItem: TMenuItem;
    SingleItem: TMenuItem;
    SplittedItem: TMenuItem;
    RegionItem: TMenuItem;
    ViewItem: TMenuItem;
    StatusBar: TStatusBar;
    Chances2D: TStringGrid;
    WorldBox   :TGroupBox;
    Worlds     :TListBox;
    Ratings    :TStringGrid;
    // Event-Handlers
    procedure Chances2DDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure Chances2DGetCellHint(Sender: TObject; ACol, ARow: Integer;
      var HintText: String);
    procedure  ClockTimer(Sender: TObject);
    procedure  FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure  FormCreate(Sender: TObject);
    procedure  FormDestroy(Sender: TObject);
    procedure  RatingsDrawCell(Sender:TObject;aCol,aRow:Integer;aRect:TRect;aState:TGridDrawState);
    procedure  RatingsGetCellHint(Sender:TObject;ACol,ARow:Integer;var HintText:String);
    procedure  RatingsHeaderClick(Sender:TObject;IsColumn:Boolean;Index:Integer);
    procedure  RegionsChanged(Sender: TObject);
    procedure ReloadItemClick(Sender: TObject);
    procedure SaveItemClick(Sender: TObject);
    procedure  ViewChange(Sender: TObject);
    procedure  WorldsSelectionChange(Sender: TObject; User: boolean);
  private
    // The current region
    FRegion    :Integer;
    // The predictor
    FPredict   :TPredictor;
    // The sorting method
    FSort      :TSorting;

    // Callback if a match is updated
    procedure  match(Sender:TObject);
    // Callback if the simulation is done
    procedure  simulated(Sender:TObject);
    // Callback if the state is changed
    procedure  state(Sender:TObject);
    // Updates the views
    procedure  updateInformation;
  end;

  { TWorkerThread }
  // Simulates the matchup assignment in a seperate thread
  TWorkerThread = class(TThread)
  private
    // The predictor instance
    FPredict    :TPredictor;
  public
    // Constructor
    constructor Create(Predict:TPredictor);overload;
  protected
    // Execution method
    procedure   Execute;override;
  end;

var
  WvWPrediction: TWvWPrediction;

implementation

// Simulation steps
const N  = 1000000;

{$R *.lfm}

// Make the stream human readable
function formatTable(content:String):String;
begin
  Result:=StringReplace(content, '{tr}', LineEnding + ' {tr}', [rfReplaceAll]);
  Result:=StringReplace(Result, '{/tr}', LineEnding + ' {/tr}', [rfReplaceAll]);
  Result:=StringReplace(Result, '{td}', LineEnding + '  {td}', [rfReplaceAll]);
  Result:=StringReplace(Result, '{/table}', LineEnding + '{/table}', [rfReplaceAll]);
end;

// Format the streamed strings of the predictor to BBCode
function toBBCode(x:String):String;
begin
  Result:=StringReplace(x, '{', '[', [rfReplaceAll]);
  Result:=StringReplace(Result, '}', ']', [rfReplaceAll]);
end;

// Format the streamed strings of the predictor to HTML
function toHTMLCode(x:String):String;
begin
  Result:=StringReplace(x, '{', '<', [rfReplaceAll]);
  Result:=StringReplace(Result, '}', '>', [rfReplaceAll]);
end;

// Format the streamed strings of the predictor to CSV
function toCSVCode(x:String):String;
begin
  Result:=StringReplace(x, '{table}', '', [rfReplaceAll]);
  Result:=StringReplace(Result, '{/table}', '', [rfReplaceAll]);
  Result:=StringReplace(Result, '{tr}', '', [rfReplaceAll]);
  Result:=StringReplace(Result, '{/td}{/tr}', LineEnding, [rfReplaceAll]);
  Result:=StringReplace(Result, '{/td}', ';', [rfReplaceAll]);
  Result:=StringReplace(Result, '{td}', '', [rfReplaceAll]);
end;

// Minimum function
function min(a, b:Integer):Integer;inline;
begin
  if a < b then
    Result:=a
  else
    Result:=b;
end;

function mix(a, b:TColor;t:Double):TColor;inline;
var ra, ga, ba :Byte;
    rb, gb, bb :Byte;
begin
  // Split the colors
  RedGreenBlue(a, ra, ga, ba);
  RedGreenBlue(b, rb, gb, bb);

  // Mix the colors
  Result:=RGBToColor(
    Round(rb * t + (1.0 - t) * ra),
    Round(gb * t + (1.0 - t) * ga),
    Round(bb * t + (1.0 - t) * ba));
end;

{ TWorkerThread }
constructor TWorkerThread.Create(Predict:TPredictor);
begin
  // Copies the predictor
  FPredict:=Predict;
  FreeOnTerminate:=True;

  inherited Create(False);
end;

// Execution method
procedure TWorkerThread.Execute;
begin
  // Run the simulation with N steps
  if (FPredict <> Nil) and (FPredict.State = psDownloaded) then
    FPredict.simulate(N);
end;

{ TWvWPrediction }
procedure TWvWPrediction.FormCreate(Sender: TObject);
begin
  // Default region
  FRegion:=2;
  // Sort after the old rating
  FSort:=sOld;
  // English float format
  FormatSettings.DecimalSeparator:='.';
  FormatSettings.ThousandSeparator:=',';

  // Create the predictor
  FPredict:=TPredictor.Create;

  Constraints.MaxWidth:=Width;
  Constraints.MinWidth:=Width;

  // Set up the allbacks
  FPredict.OnMatch:=@match;
  FPredict.OnSimulated:=@simulated;
  FPredict.OnStateChange:=@state;
  FPredict.OnWorlds:=@RegionsChanged;

  // Start the downloading process
  FPredict.update;

  // Default view
  ViewChange(SplittedItem);
end;

// Form close method
procedure TWvWPrediction.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // Don't close during the prediction
  if FPredict.State = psPredicting then
    CloseAction:=caNone;
end;

// Timer callback
procedure TWvWPrediction.ClockTimer(Sender: TObject);
begin
  // During the prediction phase it will update the list of chances
  if (FPredict.State = psPredicting) and(Worlds.ItemIndex >= 0) then begin
    FPredict.listChancesOf(Chances, Worlds.Items[Worlds.ItemIndex]);
    Ratings.Refresh;
  end;

  // Update the status bar
  if FPredict.State = psPredicting then begin
    state(FPredict);
    Chances2D.Refresh;
  end;
end;

// Draw the 2D chances
procedure TWvWPrediction.Chances2DDrawCell(Sender:TObject;aCol,aRow:Integer;aRect:TRect;aState:TGridDrawState);
var chance:Double;
    col   :TColor;
    a, b  :String;
begin
  if (aCol > 1) and (aRow > 0) and (aRow + 1 <> aCol) then begin
    // Get the server names
    a:=Chances2D.Cells[1, aRow];
    b:=Chances2D.Cells[1, aCol - 1];

    // Get the chance
    chance:=FPredict.ChanceByName[a, b];

    //
    if chance < 0.0 then
      col:=clBlue
    else if chance < 0.125 then
      col:=mix(clBlue, clAqua, chance * 8.0)
    else if chance < 0.25 then
      col:=mix(clAqua, clGreen, chance * 4.0)
    else if chance < 0.5 then
      col:=mix(clGreen, clYellow, chance * 4.0 - 1.0)
    else if chance < 1.0 then
      col:=mix(clYellow, clRed, chance * 2.0 - 1.0)
    else
      col:=clRed;

    Chances2D.Canvas.Brush.Color:=col;
    Chances2D.Canvas.FillRect(aRect);
  end;
end;

procedure TWvWPrediction.SaveItemClick(Sender: TObject);
var cont  :String;
    lines :TStrings;

    // Save a string to a file
  procedure saveTo(content, filename:String);
  begin
    with TStringList.Create do begin
      Append(content);
      SaveToFile(filename);
      Free;
    end;

    lines.Append('File ''' + filename + ''' saved.');
  end;
begin
  if FPredict.State <> psDone then begin
    ShowMessage('Can''t save because the program is loading.');
    Exit;
  end;

  lines:=TStringList.Create;

  /////////////////////////////////////////////////////////////
  // Stream the ratings into a string
  cont:=FPredict.streamInfo(FRegion);

  // Save the csv file
  saveTo(toCSVCode(cont), 'ratings.csv');

  // Format to be better human readble
  cont:=formatTable(cont);

  // Save the HTML table
  saveTo(toHTMLCode(cont), 'ratings-html.txt');
  // Save the BBCode
  saveTo(toBBCode(cont), 'ratings-bbcode.txt');

  /////////////////////////////////////////////////////////////
  // Stream the ratings into a string
  cont:=FPredict.streamChances2D(FRegion);

  // Save the csv file
  saveTo(toCSVCode(cont), 'chances.csv');

  // Format to be better human readble
  cont:=formatTable(cont);

  // Save the HTML table
  saveTo(toHTMLCode(cont), 'chances-html.txt');

  // Save the BBCode
  saveTo(toBBCode(cont), 'chances-bbcode.txt');

  /////////////////////////////////////////////////////////////
  // Stream the chances of a world to fight against others
  if Worlds.ItemIndex >= 0 then begin
    // Stream the chances table
    cont:=FPredict.streamChancesOf(Worlds.Items[Worlds.ItemIndex]);

    // Save the csv file
    saveTo(toCSVCode(cont), Worlds.Items[Worlds.ItemIndex] + '.csv');

    // Format to be better human readble
    cont:=formatTable(cont);

    // Save the HTML table
    saveTo(toHTMLCode(cont), Worlds.Items[Worlds.ItemIndex] + '-html.txt');

    // Save the BBCode
    saveTo(toBBCode(cont), Worlds.Items[Worlds.ItemIndex] + '-bbcode.txt');
  end;

  ShowMessage(lines.Text);

  lines.Free;
end;

procedure TWvWPrediction.Chances2DGetCellHint(Sender:TObject;ACol,ARow:Integer;var HintText:String);
var chance:Double;
    a, b  :String;
begin
  if (aCol > 1) and (aRow > 0) and (aRow + 1 <> aCol) then begin
    // Get the server names
    a:=Chances2D.Cells[1, aRow];
    b:=Chances2D.Cells[1, aCol - 1];

    // Get the chance
    chance:=FPredict.ChanceByName[a, b];

    // Create the hint text
    HintText:=Format('%s vs. %s%s%4.2f%%', [a, b, LineEnding, 100.0 * chance]);
  end;
end;

procedure TWvWPrediction.FormDestroy(Sender: TObject);
begin
  // Free the predictor
  FreeAndNil(FPredict);
end;

// Draw the cells
procedure TWvWPrediction.RatingsDrawCell(Sender:TObject;aCol,aRow:Integer;aRect:TRect;aState:TGridDrawState);
var chance:TColorChance;
    l,k,w :Integer;
begin
  if (aCol = 7) and (aRow > 0) then begin
    chance:=FPredict.ColorByName[Ratings.Cells[2, aRow]];

    // Calculate the width of the cell
    w:=aRect.Right - aRect.Left;

    // Draw the red part
    l:=aRect.Left;
    k:=min(l + Round(chance.Red * w), aRect.Right);
    Ratings.Canvas.Brush.Color:=clRed;
    Ratings.Canvas.FillRect(aRect.Left, aRect.Top, k, aRect.Bottom);

    // Draw the blue part
    l:=k;
    k:=min(k + Round(chance.Blue * w), aRect.Right);
    Ratings.Canvas.Brush.Color:=clBlue;
    Ratings.Canvas.FillRect(l, aRect.Top, k, aRect.Bottom);

    // Draw the green part
    l:=k;
    k:=min(k + Round(chance.Green * w), aRect.Right);
    Ratings.Canvas.Brush.Color:=clGreen;
    Ratings.Canvas.FillRect(l, aRect.Top, k, aRect.Bottom);

    // Draw the background part
    Ratings.Canvas.Brush.Color:=clGray;
    Ratings.Canvas.FillRect(k, aRect.Top, aRect.Right, aRect.Bottom);
  end;
end;

// Get the hint of a cell
procedure TWvWPrediction.RatingsGetCellHint(Sender:TObject;ACol,ARow:Integer;var HintText:String);
var chance:TColorChance;
begin
  if (ACol = 7) and (aRow > 0) then begin
    chance:=FPredict.ColorByName[Ratings.Cells[2, aRow]];

    HintText:=Format('Green: %4.2f%%%sBlue:  %4.2f%%%sRed:   %4.2f%%',
                     [100.0 * chance.Green, LineEnding,
                      100.0 * chance.Blue, LineEnding,
                      100.0 * chance.Red]);

  end else if aRow = 0 then begin
    case aCol of
      0: HintText:='Current rank of the each world.';
      1: HintText:='Current matchup tier of each world.';
      2: HintText:='Name of each world.';
      3: HintText:='Current population of each world.';
      4: HintText:='Current rating of each world.';
      5: HintText:='Predicted rating of each world.';
      6: HintText:='Difference of the predicted and current rating.';
      7: HintText:='Chance to get a specific color in the next match.';
    end;
  end else
    HintText:='This table shows informations about' + LineEnding +
              'each world in the selected region.';
end;

// Change the sorting method by clicking on the title row
procedure TWvWPrediction.RatingsHeaderClick(Sender: TObject; IsColumn: Boolean;
  Index: Integer);
begin
  case Index of
    // Sort by match
    1: FSort:=sMatch;
    // Sort by name
    2: FSort:=sName;
    // Sort by old ratings (Before)
    0, 3: FSort:=sOld;
    // Sort by new ratings (After)
    4: FSort:=sNew;
    // Sort by difference of the ratings
    5: FSort:=sDifference;
  end;

  // Update the table
  FPredict.listInfo(Ratings, FRegion, FSort);
end;

// Region change method
procedure TWvWPrediction.RegionsChanged(Sender: TObject);
begin
  if Sender = EUItem then
    FRegion:=2
  else if Sender = NAItem then
    FRegion:=1;

  // Update the views
  updateInformation;
end;

procedure TWvWPrediction.ReloadItemClick(Sender: TObject);
begin
  FPredict.update;
end;

// Change the view
procedure TWvWPrediction.ViewChange(Sender: TObject);
begin
  if (Sender is TMenuItem) and ((Sender as TMenuItem).Parent = ViewItem) then begin
    SingleView.Visible:=Sender = SingleItem;
    SplittedView.Visible:=Sender = SplittedItem;
  end;
end;

// World selection method
procedure TWvWPrediction.WorldsSelectionChange(Sender: TObject; User: boolean);
begin
  // Update the chances if a world is selected
  FPredict.listChancesOf(Chances, Worlds.Items[Worlds.ItemIndex]);
end;

// Match callback
procedure TWvWPrediction.match(Sender:TObject);
begin
  // Check if the sender is a predictor instance
  if Sender is TPredictor then begin
    // Update the views
    updateInformation;

    // If the download is done start the workerthread and predict
    if FPredict.State = psDownloaded then
      TWorkerThread.Create(FPredict);
  end;
end;

// Callback if the state is changed
procedure TWvWPrediction.state(Sender:TObject);
begin
  if Sender is TPredictor then
    case FPredict.State of
      psNone:
        StatusBar.SimpleText:='';
      psWorlds:
        StatusBar.SimpleText:='Downloading names of the worlds...';
      psLeaderboards:
        StatusBar.SimpleText:='Downloading leaderboards...';
      psMatchups:
        StatusBar.SimpleText:='Downloading WvW matches...';
      psDownloaded:
        StatusBar.SimpleText:='Download done.';
      psPredicting:
        StatusBar.SimpleText:=Format('Simulating the match assignment... %4.2f%%', [100.0 * Double(FPredict.Steps) / N]);
      psDone:
        StatusBar.SimpleText:='Done.';
    end;
end;

// Update the information views
procedure TWvWPrediction.updateInformation;
var i, k :Integer;
begin
  // Store the current world list entries
  i:=Worlds.ItemIndex;
  k:=Worlds.TopIndex;

  // Update the world list
  FPredict.listWorlds(Worlds.Items, FRegion);

  // Restore the old entries
  if i >= 0 then Worlds.ItemIndex:=i;
  Worlds.TopIndex:=k;

  // Update the rating table
  FPredict.listInfo(Ratings, FRegion, FSort);

  // Update the 2D view
  FPredict.listChances2D(Chances2D, FRegion);

  // Refresh the list
  Worlds.Refresh;
end;

// Simulated callback
procedure TWvWPrediction.simulated(Sender:TObject);
begin
  // If a world is chosen update the chances
  if Worlds.ItemIndex >= 0 then
    FPredict.listChancesOf(Chances, Worlds.Items[Worlds.ItemIndex]);

  Chances2D.Refresh;
end;

end.

