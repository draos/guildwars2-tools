unit uWebAPI;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, simpleinternet, CustomTimer, fgl;

type
  TRequestState    = (rsIdle, rsDownloading, rsDownloaded);

  // True => Terminate Thread
  // False=> Redo the request
  TRequestCallback = function(Data:String):Boolean of object;

  TRequested    = class
    Url         :String;
    Callback    :TRequestCallback;
    Delay       :Integer;

    constructor Create(Url_:String;Callback_:TRequestCallback;Delay_:Integer);
  end;

  TRequestThread   = class(TThread)
  private
    FCallback   :TRequestCallback;
    FData       :String;
    FDelay      :Integer;
    FState      :TRequestState;
    FUrl        :String;
    FRedo       :Boolean;
  protected
    procedure   Execute;override;
  public
    constructor Create(Url:String;Callback:TRequestCallback;Delay:Integer);overload;
    constructor Create(Requested:TRequested);overload;

    function    Response:Boolean;
  end;

  TRequestQueue     = specialize TFPGObjectList<TRequested>;
  TRequestList      = specialize TFPGList<TRequestThread>;

  { TWebAPI }
  TWebAPI       = class
  private
    FLimit      :Integer;
    FRequests   :TRequestList;
    FQueue      :TRequestQueue;
    FKey        :String;
    FTimer      :TCustomTimer;

    function    getOpenRequests:Integer;

    procedure   setKey(Value:String);
  private
    procedure   TimerProc(Sender:TObject);
  public
    constructor Create(Limit:Integer=10);
    destructor  Destroy;override;

    // Endpoint .. API Endpoint with needed API version and arguments
    // Callback .. Will be called if data is downloaded
    // Auth     .. The access token will automatically added if true
    // Delay    .. Delay in seconds until the downloading starts
    procedure   RequestJSON(Endpoint:String;Callback:TRequestCallback;Auth:Boolean=False;Delay:Integer=0);

    procedure   RequestUrl(Url:String;Callback:TRequestCallback);

    property    AccessKey:String read FKey write setKey;
    property    Limit:Integer read FLimit write FLimit;
    property    OpenRequests:Integer read getOpenRequests;
  end;

// Web API
var WebAPI      :TWebAPI = Nil;

implementation

const
  SERVER = 'https://api.guildwars2.com';

function HTTPEncode(const s:String):String;
const
  allowed = ['A'..'Z', 'a'..'z', '0'..'9', '*', '@', '.', '-', '_', '=', '/', '?', '&'];
var pS, pD :PChar;
begin
  SetLength(Result, 3 * Length(s));

  pS:=PChar(s);
  pD:=PChar(Result);

  while pS^ <> #0 do begin
    if pS^ in allowed then
      pD^:=pS^
    else begin
      FormatBuf(pD^, 3, '%%%.2x', 6, [Ord(pS^)]);
      Inc(pD, 2);
    end;

    Inc(pS);
    inc(pD);
  end;

  SetLength(Result, pD - PChar(Result));
end;

constructor TWebAPI.Create(Limit:Integer);
begin
  FLimit:=Limit;

  FKey:='';

  FRequests:=TRequestList.Create;
  FQueue:=TRequestQueue.Create;

  FTimer:=TCustomTimer.Create(Nil);
  FTimer.Interval:=250;
  FTimer.Enabled:=True;
  FTimer.OnTimer:=@TimerProc;
end;

destructor TWebAPI.Destroy;
var i      :Integer;
begin
  FTimer.Enabled:=False;
  i:=0;
  while i < FRequests.Count do begin
    FRequests[i].Terminate;
    Inc(i);
  end;

  FreeAndNil(FRequests);
  FreeAndNil(FQueue);
  inherited Destroy;
end;

function TWebAPI.getOpenRequests:Integer;
begin
  Result:=FQueue.Count + FRequests.Count;
end;

procedure TWebAPI.setKey(Value:String);
var pS, pD :PChar;
begin
  SetLength(FKey, Length(Value));
  pD:=PChar(FKey);
  pS:=PChar(Value);

  while pS^ <> #0 do begin
    if pS^ in ['0'..'9', 'A'..'F', 'a'..'f', '-'] then begin
      pD^:=pS^;
      Inc(pD);
    end;

    Inc(pS);
  end;

  SetLength(FKey, pD - PChar(FKey));
end;

procedure TWebAPI.TimerProc(Sender:TObject);
var i     :Integer;
begin
  i:=FRequests.Count;

  while 0 < i do begin
    Dec(i);

    if FRequests[i].Response then begin
      FRequests[i].Terminate;
      FRequests.Delete(i);
    end;
  end;

  while (FRequests.Count < FLimit) and (FQueue.Count > 0) do begin
    FRequests.Add(TRequestThread.Create(FQueue[0]));
    FQueue.Delete(0);
  end;
end;

procedure TWebAPI.RequestJSON(Endpoint:String;Callback:TRequestCallback;Auth:Boolean;Delay:Integer);
var url   :String;
begin
  if (Callback <> Nil) and ((not Auth) or (FKey <> '')) then begin
    if (Pos('?', Endpoint) > 0) and Auth then
      url:=Format('%s&access_token=%s', [Endpoint, FKey])
    else if Auth then
      url:=Format('%s?access_token=%s', [Endpoint, FKey])
    else
      url:=Endpoint;

    if (url <> '') and (url[1] <> '/') then
      url:=SERVER + '/' + HTTPEncode(url)
    else
      url:=SERVER + HTTPEncode(url);

    if FRequests.Count >= FLimit then
      FQueue.Add(TRequested.Create(url, Callback, Delay))
    else
      FRequests.Add(TRequestThread.Create(url, Callback, Delay));
  end;
end;

procedure TWebAPI.RequestUrl(Url:String;Callback:TRequestCallback);
begin
  if Callback <> Nil then begin
    if FRequests.Count >= FLimit then
      FQueue.Add(TRequested.Create(url, Callback, 0))
    else
      FRequests.Add(TRequestThread.Create(url, Callback, 0));
  end;
end;

{ TRequestThread }
constructor TRequestThread.Create(Url:String;Callback:TRequestCallback;Delay:Integer);
begin
  inherited Create(False);
  FreeOnTerminate:=True;

  FCallback:=Callback;
  FData:='';
  FDelay:=Delay;
  FRedo:=False;
  FState:=rsIdle;
  FUrl:=Url;
end;

constructor TRequestThread.Create(Requested:TRequested);
begin
  inherited Create(False);
  FreeOnTerminate:=True;

  FCallback:=Requested.Callback;
  FData:='';
  FDelay:=Requested.Delay;
  FRedo:=False;
  FState:=rsIdle;
  FUrl:=Requested.Url;
end;

procedure TRequestThread.Execute;
begin
  repeat
    if FRedo then
      Sleep(1000);

    if FDelay > 0 then
      Sleep(FDelay * 1000);

    FRedo:=False;
    FState:=rsDownloading;
    FData:='';

    try
      FData:=retrieve(FUrl);
    except

    end;

    FState:=rsDownloaded;

    while (not Terminated) and (not FRedo) do
      Sleep(250);
  until Terminated;
end;

function TRequestThread.Response:Boolean;
begin
  Result:=FState = rsDownloaded;

  if Result and (FCallback <> Nil) then begin
    Result:=FCallback(FData);

    if not Result then
      FRedo:=True;
  end;
end;

{ TRequested }
constructor TRequested.Create(Url_:String;Callback_:TRequestCallback;Delay_:Integer);
begin
  Url:=Url_;
  Callback:=Callback_;
  Delay:=Delay_;
end;

initialization
  WebAPI:=TWebAPI.Create;

finalization
  FreeAndNil(WebAPI);

end.

