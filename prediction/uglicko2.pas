unit uGlicko2;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, math;

type
  TValueList    = Array of Double;

  TPlayer       = class
  private
    FTau        :Double;
    FN          :Integer;
    FRating     :Double;
    FRD         :Double;
    FVol        :Double;

    function    calcDelta(ratings, rds, outcomes:TValueList; v:Double):Double;
    function    calcE(p2rating, p2rd:Double):Double;
    function    calcG(rd:Double):Double;
    function    calcV(ratings, rds:TValueList):Double;
    function    newVolatility(ratings, rds, outcomes:TValueList; v:Double):Double;

    function    getRating:Double;
    function    getRD:Double;

    procedure   setRating(rating:Double);
    procedure   setRD(rd:Double);

    procedure   preRatingRD;
  public
    constructor Create(Rating, Deviation, Volatility:Double);

    procedure   update(Ratings, RDs, Outcomes:TValueList);

    property    Rating:Double read getRating write setRating;
    property    RD:Double read getRD write setRD;
  end;

implementation

const
  SCALE = 173.7178;
  BASE  = 1500.0;

function sqr(x:Double):Double;inline;
begin
  Result:=x*x;
end;

constructor TPlayer.Create(Rating, Deviation, Volatility:Double);
begin
  FTau:=0.6;
  FN:=1000;
  FVol:=Volatility;

  setRating(rating);
  setRD(Deviation);
end;

// Glicko delta function
function TPlayer.calcDelta(ratings, rds, outcomes:TValueList; v:Double):Double;
var i    :Integer;
begin
  Result:=0.0;
  i:=0;
  while i < Length(ratings) do begin
    Result:=Result + calcG(rds[i]) * (outcomes[i] - calcE(ratings[i], rds[i]));
    Inc(i);
  end;

  Result:=v * Result;
end;

// Glicko E function
function TPlayer.calcE(p2rating, p2rd:Double):Double;
begin
  Result:=1 / (1 + exp(calcG(p2rd) * (p2rating - FRating)));
end;

// Glicko g(RD) function
function TPlayer.calcG(rd:Double):Double;
begin
  Result:=1 / sqrt(1 + 3 * sqr(rd / pi));
end;

// Glicko v function
function TPlayer.calcV(ratings, rds:TValueList):Double;
var i    :Integer;
    te   :Double;
begin
  Result:=0.0;
  i:=0;
  while i < Length(ratings) do begin
    te:=calcE(ratings[i], rds[i]);
    Result:=Result + sqr(calcG(rds[i])) * te * (1 - te);

    Inc(i);
  end;

  if not SameValue(Result, 0.0) then
    Result:=1.0 / Result;
end;

// Calculating the new volatility
function TPlayer.newVolatility(ratings, rds, outcomes:TValueList; v:Double):Double;
var delta  :Double;
    a, d   :Double;
    x0, x1 :Double;
    h1, h2 :Double;
    tau    :Double;
    i      :Integer;
begin
  delta:=calcDelta(ratings, rds, outcomes, v);
  if not SameValue(FVol, 0.0) then
    a:=ln(sqr(FVol))
  else
    a:=0.0;

  x0:=a;
  x1:=0.0;
  i:=0;
  tau:=1 / sqr(FTau);

  while (not SameValue(x0, x1)) and (i < FN) do begin
    x0:=x1;
    d:=sqr(FRating) + v + exp(x0);
    h1:=-(x0 - a) * tau - 0.5 * (1 - sqr(delta / d)) * exp(x0);
    h2:=-tau - 0.5 * exp(x0) * (sqr(FRating)+ v) / sqr(d) + 0.5 * sqr(delta) * exp(x0) * (sqr(FRating) + v - exp(x0)) / power(d, 3);

    x1:=x0 - (h1 / h2);

    Inc(i);
  end;

  Result:=exp(x1 / 2);
end;

function TPlayer.getRating:Double;
begin
  Result:=FRating * SCALE + BASE;
end;

function TPlayer.getRD:Double;
begin
  Result:=FRD * SCALE;
end;

procedure TPlayer.setRating(rating:Double);
begin
  FRating:=(rating - BASE) / SCALE;
end;

procedure TPlayer.setRD(rd:Double);
begin
  FRD:=rd / SCALE;
end;

// Calculates and updates the player's rating deviation for the beginning of the rating period
procedure TPlayer.preRatingRD;
begin
  FRD:=sqrt(sqr(FRD) + sqr(FVol));
end;

procedure TPlayer.update(Ratings, RDs, Outcomes:TValueList);
var trate :TValueList;
    trd   :TValueList;
    i     :Integer;

    v     :Double;
    sum   :Double;
begin
  SetLength(trate, Length(Ratings));
  i:=0;
  // Convert the rating values for internal uses
  while i < Length(Ratings) do begin
    trate[i]:=(Ratings[i] - BASE) / SCALE;
    Inc(i);
  end;

  // Convert the rating deviation values for internal use
  SetLength(trd, Length(RDs));
  i:=0;
  while i < Length(RDs) do begin
    trd[i]:=RDs[i] / SCALE;
    Inc(i);
  end;

  v:=calcV(trate, trd);
  FVol:=newVolatility(trate, trd, Outcomes, v);
  preRatingRD;

  FRD:=1 / sqrt((1 / sqr(FRD)) + 1 / v);

  sum:=0.0;
  i:=0;
  while i < Length(trate) do begin
    sum:=sum + calcG(trd[i]) * (Outcomes[i] - calcE(trate[i], trd[i]));

    Inc(i);
  end;

  FRating:=FRating + sqr(FRD) * sum;

  SetLength(trate, 0);
  SetLength(trd, 0);
end;

end.

