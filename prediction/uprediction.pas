unit uPrediction;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, fpjson, jsonparser, simpleinternet, xquery, uWebAPI,
  uGlicko2, math, fgl, Grids, Graphics;

type
  // States of the predictor
  TPredictState = (psNone, psWorlds, psLeaderboards, psMatchups, psDownloaded, psPredicting, psDone);

  // Selectable columns for saving operations
  TSaveColumn   = (scRank, scMatch, scName, scOld, scNew, scDiff, scColor);
  TSaveColumns  = Set of TSaveColumn;

const
  SaveAll       = [scRank, scMatch, scName, scOld, scNew, scDiff, scColor];
  psDownloading = [psWorlds, psLeaderboards, psMatchups];


type
  // Sorting methods of the rating view
  TSorting      = (sOld, sNew, sMatch, sName, sDifference);

  // Structure to store the chance to get a color
  TColorChance = record
    Red        :Double;
    Blue       :Double;
    Green      :Double;
  end;

  { TWorld }
  PWorld       = ^TWorld;
  TWorld       = record
    // World ID
    ID          :Integer;
    // World Name
    Name        :String;
    // Match ID
    MatchID     :String;
    // Population
    Population  :String;
    // Colors
    Colors      :Array[0..2] of Integer;
    // Score of the match
    Score       :Integer;
    // Rating from the leaderboard
    Rating      :Double;
    // Deviation from the leaderboard
    Deviation   :Double;
    // Volatility from the leaderboard
    Volatility  :Double;
    // New calculated rating
    NewRating   :Double;
    // New calculated deviation
    NewDeviation:Double;
  end;

  { TMatch }
  TMatch        = record
    // Match ID with the format region-tier
    ID          :String;
    // The worlds in the match with red=0, blue=1, green=2
    Worlds      :Array[0..2] of Integer;
  end;

  { TPredictor }
  TPredictor   = class
  private
    // The current state of the prediction
    FState      :TPredictState;
    // The server list
    FWorlds    :Array of TWorld;
    // The matches list
    FMatches    :Array of TMatch;
    // The chances table
    FChances    :Array of Array of Integer;
    // The done calculation steps
    FSteps      :Integer;

    // The needed steps in the current state
    FNeeded     :Integer;
    // The done steps in the current state
    FDone       :Integer;

    // The callbacks
    FOnLeader   :TNotifyEvent;
    FOnMatch    :TNotifyEvent;
    FOnMatches  :TNotifyEvent;
    FOnSimulate :TNotifyEvent;
    FOnState    :TNotifyEvent;
    FOnWorlds   :TNotifyEvent;

    // Get the chance by a given ID or Name
    function    getChanceByID(ID1, ID2:Integer):Double;
    function    getChanceByName(Name1, Name2:String):Double;
    // Get the chance to get a color
    function    getChanceByID(ID:Integer):TColorChance;
    function    getChanceByName(Name:String):TColorChance;
    // Get the world by a given ID or Name
    function    getWorldByName(Name:String):TWorld;
    function    getWorldByID(ID:Integer):TWorld;
    // Get the index of the world by a given ID or Name
    function    getWorldIdxByID(ID:Integer):Integer;
    function    getWorldIdxByName(Name:String):Integer;

    // Download callback handlers
    function    handleMatches(Data:String):Boolean;
    function    handleMatch(Data:String):Boolean;
    function    handleLeaderboard(Data:String):Boolean;
    function    handleWorlds(Data:String):Boolean;

    // Change the state
    procedure   changeState(V:TPredictState);

    // Generate the chances table (not filled)
    procedure   generateChances;
    // Increment one entry of the chances table (updates [a, b] and [b, a])
    procedure   incChance(a, b:Integer);
    // Zero the chances table
    procedure   resetChances;

    // Predict the matchup of the servers with given indexes
    procedure   predict(a, b, c:Integer);

    // List the chances of a given World in a table with 2 columns
    procedure   listChances(Grid:TStringGrid;Idx:Integer);overload;

    // Stream the chances of a given world
    function    streamChances(Idx:Integer):String;
  public
    // Constructor
    constructor Create;
    // Destructor
    destructor  Destroy;override;

    // Stream the chances of a given World
    function    streamChancesOf(World:Integer):String;overload;
    function    streamChancesOf(World:String):String;overload;
    // Stream the information of a given region
    function    streamInfo(Region:Integer;Sort:TSorting=sOld;Save:TSaveColumns=SaveAll):String;
    // Stream the chances of a given Region
    function    streamChances2D(Region:Integer):String;

    // Update the informations
    procedure   update;

    // Reset the chances and simulate N steps
    procedure   simulate(N:Integer);

    // List the chances of a given Region
    procedure   listChances2D(Grid:TStringGrid;Region:Integer);
    // List the chances of a given World in a table with
    procedure   listChancesOf(Grid:TStringGrid;World:Integer);overload;
    procedure   listChancesOf(Grid:TStringGrid;World:String);overload;
    // List the information of a given region in a table
    procedure   listInfo(Grid:TStringGrid;Region:Integer;Sort:TSorting=sOld);
    // List all worlds of a region in a stringlist
    procedure   listWorlds(Lines:TStrings;Region:Integer;Sort:TSorting=sOld);

    // Get the chance of the fight of 2 given servers
    // given by IDs
    property    ChanceByID[ID1,ID2:Integer]:Double read getChanceByID;
    // given by Names
    property    ChanceByName[Name1,Name2:String]:Double read getChanceByName;

    // Get the chance to get a color
    // given by ID
    property    ColorByID[ID:Integer]:TColorChance read getChanceByID;
    // given by Name
    property    ColorByName[Name:String]:TColorChance read getChanceByName;

    // Callback setter
    property    OnLeaderboard:TNotifyEvent write FOnLeader;
    property    OnMatch:TNotifyEvent write FOnMatch;
    property    OnMatches:TNotifyEvent write FOnMatches;
    property    OnSimulated:TNotifyEvent write FOnSimulate;
    property    OnStateChange:TNotifyEvent write FOnState;
    property    OnWorlds:TNotifyEvent write FOnWorlds;

    // Get the done steps of the simulation
    property    Steps:Integer read FSteps;
    // Get the state of the predictor
    property    State:TPredictState read FState;

    // Get the world information of a given server
    property    WorldByID[ID:Integer]:TWorld read getWorldByID;
    property    WorldByName[Name:String]:TWorld read getWorldByName;
  end;

implementation

type
  // Parsed rating from the leaderboard website
  TRating       = record
    Name        :String;
    Rating      :String;
    Deviation   :String;
    Volatility  :String;
  end;

  // Class used in the prediction
  TPredicted    = class
  private
    // Index in the world array
    FIdx        :Integer;
    // Calculated randomized Rating
    FRating     :Double;
    // Region
    FRegion     :Integer;
  public
    // Constructor (Zero everything)
    constructor Create(Idx:Integer=-1;Rating_:Double=0.0;Region_:Integer=0);

    // Getter and setter properties
    property    Index:Integer read FIdx write FIdx;
    property    Rating:Double read FRating write FRating;
    property    Region:Integer read FRegion write FRegion;
  end;

  // Class used for the sorting of the list after the rating
  TWorldInfo    = class
  private
    FWorld      :PWorld;
    FRank       :Integer;

    function    getDiff:Double;Inline;
    function    getMatchID:String;Inline;
    function    getName:String;Inline;
    function    getNewRating:Double;Inline;
    function    getOldRating:Double;Inline;
    function    getPopulation:String;Inline;
  public
    // Constructor
    constructor Create(World:PWorld);

    // Getter properties
    property    Difference:Double read getDiff;
    property    MatchID:String read getMatchID;
    property    Name:String read getName;
    property    NewRating:Double read getNewRating;
    property    OldRating:Double read getOldRating;
    property    Population:String read getPopulation;
    property    Rank:Integer read FRank write FRank;
  end;

  // Specialized objectlists
  TPredictions  = specialize TFPGObjectList<TPredicted>;
  TWorldInfos   = specialize TFPGObjectList<TWorldInfo>;

// Compare function for the TPredicted used for the prediction of NA and EU
function compare(const a, b:TPredicted):Integer;
begin
  // If the regions are equal
  if a.Region = b.Region then
    // Compare with the rating
    Result:=CompareValue(b.Rating, a.Rating)
  else
    // else compare the regions
    Result:=b.Region - a.Region;
end;

// Compare the TWorldInfo using the old ratings
function compareOld(const a, b:TWorldInfo):Integer;
begin
  Result:=CompareValue(b.OldRating, a.OldRating);
end;

// Compare the TWorldInfo using the new ratings
function compareNew(const a, b:TWorldInfo):Integer;
begin
  Result:=CompareValue(b.NewRating, a.NewRating);
end;

// Compare the TWorldInfo using the match id and the old rating
function compareMatch(const a, b:TWorldInfo):Integer;
begin
  if a.MatchID = b.MatchID then
    Result:=CompareOld(a, b)
  else
    Result:=CompareStr(a.MatchID, b.MatchID);
end;

// Compare the TWorldInfo using the name
function compareName(const a, b:TWorldInfo):Integer;
begin
  Result:=CompareStr(a.Name, b.Name);
end;

// Compare the TWorldInfo using the difference of the ratings
function compareDiff(const a, b:TWorldInfo):Integer;
begin
  Result:=CompareValue(b.Difference, a.Difference);
end;

// Clean a string by erasing heading and tailing spaces
function clean(x:String):String;
// All legit characters (Must allow numbers and server names)
const legit = ['a'..'z', 'A'..'Z', '[', ']', '0'..'9', '.', '-'];
var i, k :Integer;
begin
  i:=1;
  // Skip all leading spaces
  while (i < Length(x)) and (not (x[i] in legit)) do
    Inc(i);

  k:=Length(x);
  // Skip all tailing spaces
  while (0 < k) and (not (x[k] in legit)) do
    Dec(k);

  // Copy only the valid part
  Result:=Copy(x, i, k - i + 1);
end;

// Calculate the world region from the given ID
function regionOf(ID:Integer):Integer;
begin
  Result:=ID div 1000;
end;

// Extract the region of a match id
function regionOf(ID:String):Integer;
var i    :Integer;
begin
  Result:=0;
  try
    SScanf(ID, '%d-%d', [@Result, @i]);
  finally
  end;
end;

// Extract the tier of a match id
function tierOf(ID:String):Integer;
var i    :Integer;
begin
  Result:=0;
  try
    SScanf(ID, '%d-%d', [@i, @Result]);
  finally
  end;
end;

// GuildWars2 scoring function to transform the scores into the interval [0; 1]
// 0.0: Loss
// 0.5: Drawn
// 1.0: Win
function sc(scoreA, score:Double):Double;
begin
  // Sum both scores
  score:=scoreA + score;
  // Check and handle non zero
  if not SameValue(score, 0.0) then
    // Calculation over sin
    Result:=0.5 * (1.0 + sin(pi * (scoreA / score - 0.5)))
  else
    Result:=0.5;
end;

{ TWorldInfo }
constructor TWorldInfo.Create(World:PWorld);
begin
  FWorld:=World;
  FRank:=0;
end;

function TWorldInfo.getDiff:Double;
begin
  if FWorld <> Nil then
    Result:=FWorld^.NewRating - FWorld^.Rating
  else
    Result:=0.0;
end;

function TWorldInfo.getMatchID:String;
begin
  if FWorld <> Nil then
    Result:=FWorld^.MatchID
  else
    Result:='0-0';
end;

function TWorldInfo.getName:String;
begin
  if FWorld <> Nil then
    Result:=FWorld^.Name
  else
    Result:='';
end;

function TWorldInfo.getNewRating:Double;
begin
  if FWorld <> Nil then
    Result:=FWorld^.NewRating
  else
    Result:=0.0;
end;

function TWorldInfo.getPopulation:String;
begin
  if FWorld = Nil then
    Result:=''
  else case FWorld^.Population of
    'VeryHigh': Result:='Very High';
  else
    Result:=FWorld^.Population;
  end;
end;

function TWorldInfo.getOldRating:Double;
begin
  if FWorld <> Nil then
    Result:=FWorld^.Rating
  else
    Result:=0.0;
end;

{ TPredicted }
constructor TPredicted.Create(Idx:Integer;Rating_:Double=0.0;Region_:Integer=0);
begin
  FIdx:=Idx;
  FRating:=Rating_;
  FRegion:=Region_;
end;

{ TPredictor }
constructor TPredictor.Create;
begin
  // Set up the arrays
  SetLength(FWorlds, 0);
  SetLength(FMatches, 0);
  SetLength(FChances, 0);

  // Set up the default values
  FState:=psNone;
  FSteps:=0;
  FDone:=0;
  FNeeded:=0;

  // Set up the callbacks
  FOnLeader:=Nil;
  FOnMatches:=Nil;
  FOnMatch:=Nil;
  FOnSimulate:=Nil;
  FOnState:=Nil;
  FOnWorlds:=Nil;

  Randomize;
end;

// Destructor
destructor TPredictor.Destroy;
begin
  SetLength(FWorlds, 0);
end;

// Get the chance with given server IDs
function TPredictor.getChanceByID(ID1, ID2:Integer):Double;
var a, b :Integer;
begin
  Result:=0.0;

  // Only if the prediction done steps
  if FSteps > 0 then begin
    // Get the index of the world ID
    a:=getWorldIdxByID(ID1);
    b:=getWorldIdxByID(ID2);

    // Check it and return the chance
    if (a >= 0) and (b >= 0) then
      Result:=FChances[a, b] / FSteps;
  end;
end;

// Get the chance with given server names
function TPredictor.getChanceByName(Name1, Name2:String):Double;
var a, b :Integer;
begin
  Result:=0.0;

  // Only if the prediction done steps
  if FSteps > 0 then begin
    // Get the index of the world name
    a:=getWorldIdxByName(Name1);
    b:=getWorldIdxByName(Name2);

    // Check it and return the chance
    if (a >= 0) and (b >= 0) then
      Result:=FChances[a, b] / FSteps;
  end;
end;

// Get the chance to get a color
function TPredictor.getChanceByID(ID:Integer):TColorChance;
var k    :Integer;
begin
  // Default values
  Result.Red:=0.0;
  Result.Blue:=0.0;
  Result.Green:=0.0;

  if FSteps <= 0 then
    Exit;

  // Get the index of the world
  k:=getWorldIdxByID(ID);

  if k >= 0 then begin
    // Calculate the chances
    Result.Red:=Double(FWorlds[k].Colors[0]) / FSteps;
    Result.Blue:=Double(FWorlds[k].Colors[1]) / FSteps;
    Result.Green:=Double(FWorlds[k].Colors[2]) / FSteps;
  end;
end;

function TPredictor.getChanceByName(Name:String):TColorChance;
var k    :Integer;
begin
  // Default values
  Result.Red:=0.0;
  Result.Blue:=0.0;
  Result.Green:=0.0;

  if FSteps <= 0 then
    Exit;

  // Get the index of the world
  k:=getWorldIdxByName(Name);

  if k >= 0 then begin
    // Calculate the chances
    Result.Red:=Double(FWorlds[k].Colors[0]) / FSteps;
    Result.Blue:=Double(FWorlds[k].Colors[1]) / FSteps;
    Result.Green:=Double(FWorlds[k].Colors[2]) / FSteps;
  end;
end;

// Get the world index of the given ID
function TPredictor.getWorldIdxByID(ID:Integer):Integer;
var i    :Integer;
begin
  // Default is -1
  Result:=-1;
  // Iterate over all worlds
  for i:=0 to Length(FWorlds) - 1 do
    // If the ID matches then update the result
    if FWorlds[i].ID = ID then
      Result:=i;
end;

// Get the world index of the given name
function TPredictor.getWorldIdxByName(Name:String):Integer;
var i    :Integer;
begin
  // Default is -1
  Result:=-1;
  // Iterate over all worlds
  for i:=0 to Length(FWorlds) -1 do
    // If the name matches update the result
    if FWorlds[i].Name = Name then
      Result:=i;
end;

// Get the world structure of the given ID
function TPredictor.getWorldByID(ID:Integer):TWorld;
var i    :Integer;
begin
  // Default values
  Result.ID:=0;
  Result.Name:='';

  for i:=0 to Length(FWorlds) - 1 do
    // If the ID match update the result
    if ID = FWorlds[i].ID then
      Result:=FWorlds[i];
end;

// Get the world structure of the given name
function TPredictor.getWorldByName(Name:String):TWorld;
var i    :Integer;
begin
  // Default values
  Result.ID:=0;
  Result.Name:='';

  for i:=0 to Length(FWorlds) - 1 do
    // If the ID match update the result
    if Name = FWorlds[i].Name then
      Result:=FWorlds[i];
end;

// Change the state
procedure TPredictor.changeState(V:TPredictState);
begin
  FState:=V;

  if FOnState <> Nil then
    FOnState(Self);
end;

// List the chances of a given World in a N x N table
procedure TPredictor.listChances2D(Grid:TStringGrid;Region:Integer);
var i     :Integer;
    rates :TPredictions;
begin
  // Exit if the arguments are invalid
  if (Grid = Nil) or not(Region in [1, 2]) then
    Exit;

  rates:=TPredictions.Create;
  // Create the world list
  for i:=0 to Length(FWorlds) - 1 do
    if Region = regionOf(FWorlds[i].ID) then
      rates.Add(TPredicted.Create(i, FWorlds[i].NewRating, Region));

  // Sort
  rates.sort(@compare);

  // Begin the update of the grid
  Grid.BeginUpdate;

  // Update the size
  Grid.ColCount:=rates.Count + 2;
  Grid.RowCount:=rates.Count + 1;

  // Fixed cells
  Grid.FixedCols:=2;
  Grid.FixedRows:=1;

  // Set the width of the first column
  Grid.ColWidths[1]:=160;
  Grid.Cells[0, 1]:='Name';

  for i:=0 to rates.Count - 1 do begin
    Grid.Cells[1, i + 1]:=FWorlds[rates[i].Index].Name;
    Grid.Cells[0, i + 1]:=IntToStr(i + 1);
    Grid.Cells[i + 2, 0]:=IntToStr(i + 1);
  end;

  // End of the update and refreshthe grid
  Grid.EndUpdate;

  // Free the rates
  rates.Free;
end;

// Stream the chances of a given Region
function TPredictor.streamChances2D(Region:Integer):String;
var i, j  :Integer;
    rates :TPredictions;
    cell  :String;
begin
  Result:='';

  // Exit if the arguments are invalid
  if not(Region in [1, 2]) then
    Exit;

  rates:=TPredictions.Create;
  // Create the world list
  for i:=0 to Length(FWorlds) - 1 do
    if Region = regionOf(FWorlds[i].ID) then
      rates.Add(TPredicted.Create(i, FWorlds[i].NewRating, Region));

  // Sort
  rates.sort(@compare);

  // Cell format string
  cell:='{td}%.2f%%{/td}';

  // Start the table and add the header
  Result:='{table}{tr}{td}{/td}';
  for i:=0 to rates.Count - 1 do
    Result:=Result + '{td}' + FWorlds[rates[i].Index].Name + '{/td}';

  // Close the header row
  Result:=Result + '{/tr}';

  // Append the table
  for i:=0 to rates.Count do begin
    Result:=Result + '{tr}';
    for j:=0 to rates.Count - 1 do
      Result:=Result + Format(cell, [100.0 * FChances[rates[i].Index, rates[i].Index] / FSteps]);

    Result:=Result + '{/tr}';
  end;

  // Close the table
  Result:=Result + '{table}';

  // Free the rates
  rates.Free;
end;

// List the chances of a given World in a table with 2 columns given by index
procedure TPredictor.listChances(Grid:TStringGrid;Idx:Integer);
var i, j  :Integer;
    rates :TPredictions;
begin
  // Check the grid, the index and the done prediction steps
  if (Grid = Nil) or (Idx < 0) or (FSteps <= 0) then
    Exit;

  // Create the TWorldInfo list
  rates:=TPredictions.Create;

  for i:=0 to Length(FWorlds) - 1 do
    // Only append the list with world of the same region
    if (FWorlds[Idx].ID <> FWorlds[i].ID) and (regionOf(FWorlds[Idx].ID) = regionOf(FWorlds[i].ID)) then
      // Store the chance in the old rating information
      rates.Add(TPredicted.Create(i, 100.0 * FChances[Idx, i] / FSteps, 0));

  // Sort the ratings by the stored chances
  rates.Sort(@compare);

  // Begin the updating of the grid
  Grid.BeginUpdate;

  // Set the column count if smaller
  if Grid.ColCount < 2 then Grid.ColCount:=2;
  // Set the row count
  Grid.RowCount:=rates.Count + 1;

  // Update the information in the grid
  j:=1;
  for i:=0 to rates.Count - 1 do
    if rates[i].Rating > 0.009 then begin
      // First cell of each row is the world name
      Grid.Cells[0, j]:=FWorlds[rates[i].Index].Name;
      // Second cell is the chance in % rounded
      Grid.Cells[1, j]:=Format('%.2f%%', [rates[i].Rating]);
      Inc(j);
    end;

  // Cut the grid
  Grid.RowCount:=j;

  // End the update and refresh the grid
  Grid.EndUpdate;
end;

// List the chances of a given World in a table with 2 columns given by ID
procedure TPredictor.listChancesOf(Grid:TStringGrid;World:Integer);
begin
  listChances(Grid, getWorldIdxByID(World));
end;

// List the chances of a given World in a table with 2 columns given by name
procedure TPredictor.listChancesOf(Grid:TStringGrid;World:String);
begin
  listChances(Grid, getWorldIdxByName(World));
end;

// List the chances of a given World in a table with 2 columns given by index
function TPredictor.streamChances(Idx:Integer):String;
var i     :Integer;
    rates :TPredictions;
    line  :String;
begin
  Result:='';

  // Check the grid, the index and the done prediction steps
  if (Idx < 0) or (FSteps <= 0) then
    Exit;

  // Create the TWorldInfo list
  rates:=TPredictions.Create;

  for i:=0 to Length(FWorlds) - 1 do
    // Only append the list with world of the same region
    if (FWorlds[Idx].ID <> FWorlds[i].ID) and (regionOf(FWorlds[Idx].ID) = regionOf(FWorlds[i].ID)) then
      // Store the chance in the old rating information
      rates.Add(TPredicted.Create(i, 100.0 * FChances[Idx, i] / FSteps, 0));

  // Sort the ratings by the stored chances
  rates.Sort(@compare);

  // Start the table
  Result:='{table}';

  // Header
  Result:=Result + '{tr}{td}Opponent{/td}{td}Chance{/td}{/tr}';

  // Line format string
  line:='{tr}{td}%s{/td}{td}%.2f%%{/td}{/tr}';

  // Update the information in the grid
  for i:=0 to rates.Count - 1 do
    Result:=Result + Format(line,[FWorlds[rates[i].Index].Name, rates[i].Rating]);

  // End of the table
  Result:=Result + '{/table}';

  // Free the list
  rates.Free;
end;

// Stream the chances of a given World given by ID
function TPredictor.streamChancesOf(World:Integer):String;
begin
  Result:=streamChances(getWorldIdxByID(World));
end;

// List the chances of a given World given by name
function TPredictor.streamChancesOf(World:String):String;
begin
  Result:=streamChances(getWorldIdxByName(World));
end;

// List the information of a region in a table
procedure TPredictor.listInfo(Grid:TStringGrid;Region:Integer;Sort:TSorting);
var i     :Integer;
    rates :TWorldInfos;
begin
  // Check the region and the table (currently only 1 for NA and 2 for EU)
  if (Grid = Nil) or not(Region in [1, 2]) then
    Exit;

  // Create the TWorldInfo list
  rates:=TWorldInfos.Create;
  for i:=0 to Length(FWorlds) - 1 do
    // Only worlds in this region
    if Region = regionOf(FWorlds[i].ID) then
      // Append with index, rating and new rating
      rates.Add(TWorldInfo.Create(@FWorlds[i]));

  // Sort by old to calculate the rank of each server
  rates.Sort(@compareOld);
  // Store the ranks of the worlds
  for i:=0 to rates.Count - 1 do
    rates[i].Rank:=i + 1;

  // Sort the list with the given method
  case Sort of
    sNew: rates.Sort(@compareNew);
    sMatch: rates.Sort(@compareMatch);
    sName: rates.Sort(@compareName);
    sDifference: rates.Sort(@compareDiff);
  end;

  // Begin the grid update
  Grid.BeginUpdate;

  // Set the column count if necessary
  if Grid.ColCount < 6 then Grid.ColCount:=6;

  // Set the row count
  Grid.RowCount:=rates.Count + 1;

  // Update the font style of the titles to underline the sorted value
  for i:=0 to 5 do
    Grid.Columns[i].Title.Font.Style:=[fsBold];

  case Sort of
    sMatch: begin
      Grid.Columns[1].Title.Font.Style:=[fsBold, fsUnderline];
      Grid.Columns[4].Title.Font.Style:=[fsBold, fsUnderline];
    end;
    sName:
      Grid.Columns[2].Title.Font.Style:=[fsBold, fsUnderline];
    sOld:
      Grid.Columns[4].Title.Font.Style:=[fsBold, fsUnderline];
    sNew:
      Grid.Columns[5].Title.Font.Style:=[fsBold, fsUnderline];
    sDifference:
      Grid.Columns[6].Title.Font.Style:=[fsBold, fsUnderline];
  end;

  // Update the informations
  for i:=0 to rates.Count - 1 do begin
    // 1st cell of each row is the current rank
    Grid.Cells[0, i + 1]:=IntToStr(rates[i].Rank);
    // 2nd cell of each row is the current match tier
    Grid.Cells[1, i + 1]:=IntToStr(tierOf(rates[i].MatchID));
    // 3rd cell of each row is the current world name
    Grid.Cells[2, i + 1]:=rates[i].Name;
    // 4th cell of each row is population
    Grid.Cells[3, i + 1]:=rates[i].Population;
    // 5th cell of each row is the old rating
    Grid.Cells[4, i + 1]:=Format('%.2f', [rates[i].OldRating]);
    // 6th cell of each row is the new rating
    Grid.Cells[5, i + 1]:=Format('%.2f', [rates[i].NewRating]);
    // 7th cell of each row is the difference of the ratings
    if 0 < rates[i].Difference then
      Grid.Cells[6, i + 1]:=Format('+%.2f', [rates[i].Difference])
    else
      Grid.Cells[6, i + 1]:=Format('%.2f', [rates[i].Difference]);
  end;
  // End the grid updating and refresh
  Grid.EndUpdate;

  // Free the list
  rates.Free;
end;

// List the world names of a region
procedure TPredictor.listWorlds(Lines:TStrings;Region:Integer;Sort:TSorting);
var i     :Integer;
begin
  // Begin the updating
  Lines.BeginUpdate;
  // Clear the list
  Lines.Clear;

  // Only append world name in the given region
  for i:=0 to Length(FWorlds) - 1 do
    if Region = regionOf(FWorlds[i].ID) then
      Lines.Append(FWorlds[i].Name);

  // End the update and refresh
  Lines.EndUpdate;
end;

// Stream the information of a given region
function TPredictor.streamInfo(Region:Integer;Sort:TSorting;Save:TSaveColumns):String;
var i     :Integer;
    rates :TWorldInfos;
    col   :TColorChance;
begin
  Result:='';

  // Check the region and the table (currently only 1 for NA and 2 for EU)
  if not(Region in [1, 2]) then
    Exit;

  // Create the TWorldInfo list
  rates:=TWorldInfos.Create;
  // Only worlds in this region
  for i:=0 to Length(FWorlds) - 1 do
    if Region = regionOf(FWorlds[i].ID) then
      // Append with index, rating and new rating
      rates.Add(TWorldInfo.Create(@FWorlds[i]));

  // Sort by old to calculate the rank of each server
  rates.Sort(@compareOld);
  // Store the ranks of the worlds
  for i:=0 to rates.Count -1 do
    rates[i].Rank:=i + 1;

  // Sort the list with the given method
  case Sort of
    sNew: rates.Sort(@compareNew);
    sMatch: rates.Sort(@compareMatch);
    sName: rates.Sort(@compareName);
    sDifference: rates.Sort(@compareDiff);
  end;

  // Open the table
  Result:='{table}';

  // Table header
  Result:=Result + '{tr}';

  // World rank
  if scRank in Save then
    Result:=Result + '{td}#{/td}';

  // Match tier
  if scMatch in Save then
    Result:=Result + '{td}Match{/td}';

  // World name
  if scName in Save then
    Result:=Result + '{td}World{/td}';

  // Old rating
  if scOld in Save then
    Result:=Result + '{td}Old Rating{/td}';

  // New Rating
  if scNew in Save then
    Result:=Result + '{td}New Rating{/td}';

  // Difference
  if scDiff in Save then
    Result:=Result + '{td}Difference{/td}';

  // Colors
  if scColor in Save then begin
    Result:=Result + '{td}Chance of Green{/td}';
    Result:=Result + '{td}Chance of Blue{/td}';
    Result:=Result + '{td}Chance of Red{/td}';
  end;
  Result:=Result + '{/tr}';

  // Update the informations
  for i:=0 to rates.Count - 1 do begin
    Result:=Result + '{tr}';
    // 1st cell of each row is the current rank
    if scRank in Save then
      Result:=Result + Format('{td}%d{/td}', [rates[i].Rank]);

    // 2nd cell of each row is the current match tier
    if scMatch in Save then
      Result:=Result + Format('{td}%d{/td}', [tierOf(rates[i].MatchID)]);

    // 3rd cell of each row is the current world name
    if scName in Save then
      Result:=Result + Format('{td}%s{/td}', [rates[i].Name]);

    // 4th cell of each row is the old rating
    if scOld in Save then
      Result:=Result + Format('{td}%.2f{/td}', [rates[i].OldRating]);

    // 5th cell of each row is the new rating
    if scNew in Save then
      Result:=Result + Format('{td}%.2f{/td}', [rates[i].NewRating]);

    // 6th cell of each row is the difference of the ratings
    if scDiff in Save then begin
      if 0 < rates[i].Difference then
        Result:=Result + Format('{td}+%.2f{/td}', [rates[i].Difference])
      else
        Result:=Result + Format('{td}%.2f{/td}', [rates[i].Difference]);
    end;

    // 7-9th cell of each row is the chance to get a color (green, blue, red)
    if scColor in Save then begin
      col:=getChanceByName(rates[i].Name);
      Result:=Result + Format('{td}%.2f%%{/td}', [100.0 * col.Green]);
      Result:=Result + Format('{td}%.2f%%{/td}', [100.0 * col.Blue]);
      Result:=Result + Format('{td}%.2f%%{/td}', [100.0 * col.Red]);
    end;

    // Close the table row
    Result:=Result + '{/tr}';
  end;

  // Close the table
  Result:=Result +'{/table}';
  // Free the list
  rates.Free;
end;

// Generate the chances table
procedure TPredictor.generateChances;
var i     :Integer;
begin
  SetLength(FChances, Length(FWorlds));
  for i:=0 to Length(FWorlds) - 1 do
    SetLength(FChances[i], Length(FWorlds));
end;

// Increment the entries of the servers
procedure TPredictor.incChance(a, b:Integer);
begin
  // Check the indices
  if (0 <= a) and (a < Length(FWorlds)) and (0 <= b) and (b < Length(FWorlds)) then begin
    // Increment reflexive
    Inc(FChances[a, b]);
    Inc(FChances[b, a]);
  end;
end;

// Zero all chances
procedure TPredictor.resetChances;
var i, j  :Integer;
begin
  // Reset the table
  for i:=0 to Length(FChances) - 1 do
    for j:=0 to Length(FChances[i]) - 1 do
      FChances[i, j]:=0;

  // Reset the colors
  for i:=0 to Length(FWorlds) - 1 do
    for j:=0 to 2 do
      FWorlds[i].Colors[j]:=0;
end;

// Start the updating process
procedure TPredictor.update;
begin
  // Only possible in defined states
  if FState in [psNone, psDownloaded, psDone] then begin
    // Set up the state and the process variables
    FDone:=0;
    FNeeded:=1;

    // Start the requests and change the state
    WebAPI.RequestJSON('/v2/worlds?ids=all', @handleWorlds);
    changeState(psWorlds);
  end;
end;

// Handle the received world data
function TPredictor.handleWorlds(Data:String):Boolean;
var root  :TJSONData;
    arr   :TJSONArray;
    obj   :TJSONObject;
    i, k  :Integer;
begin
  Result:=False;

  // Try to parse the json data
  root:=Nil;
  with TJSONParser.Create(Data) do
    try
      root:=Parse;
    finally
      Free;
    end;

  // Check if the parsed succeed
  if (root <> Nil) and (root.JSONType = jtArray) then begin
    arr:=root as TJSONArray;
    // Update the length of the worlds array
    SetLength(FWorlds, arr.Count);

    i:=0;
    k:=0;
    for i:=0 to arr.Count - 1 do
      // Check the type and update the data
      if arr[i].JSONType = jtObject then begin
        obj:=arr[i] as TJSONObject;
        FWorlds[k].ID:=obj.Get('id', 0);
        FWorlds[k].Name:=obj.Get('name', '');
        FWorlds[k].MatchID:='';
        FWorlds[k].Population:=obj.Get('population', '');

        // If the update succeed then increment the index
        if (FWorlds[k].ID > 0) and (FWorlds[k].Name <> '') then
          Inc(k);
      end;

    // Reset the length of the array
    SetLength(FWorlds, k);

    // Generate the chances table
    generateChances;
    // Zero the chances table
    resetChances;

    // Update the number of requests
    Inc(FNeeded, 2);
    // Start the download
    WebAPI.RequestUrl('https://leaderboards.guildwars2.com/en/eu/wvw', @handleLeaderboard);
    WebAPI.RequestUrl('https://leaderboards.guildwars2.com/en/na/wvw', @handleLeaderboard);

    // Change the state
    changeState(psLeaderboards);
    Result:=True;

    // Done one step
    Inc(FDone);

    // Call the callback if given
    if FOnWorlds <> Nil then
      FOnWorlds(Self);
  end;

  // Free the json data if possible
  if root <> Nil then
    FreeAndNil(root);
end;

// Handle the received leaderboard data
function TPredictor.handleLeaderboard(Data:String):Boolean;
var val   :IXQValue;
    i,k,j :Integer;
    rates :Array[0..26] of TRating;
begin
  Result:=False;

  if Data = '' then Exit;

  // k will hold the number of readed servers
  k:=0;
  // i will hold the index in the array
  i:=0;
  // Process over the XPath module
  for val in process(Data, '//td[@class="name text"]') do
    // Read the names of the worlds
    if val.kind = pvkNode then begin
      // Clean the inner html
      rates[i].Name:=clean(val.toNode.innerHTML);

      Inc(k);
      Inc(i);
    end;

  i:=0;
  // Process over the XPath module
  for val in process(Data, '//td[contains(@class, "rating number")]//span[@class="cell-inner after-arrow"]') do
    // Read the ratings
    if val.kind = pvkNode then begin
      // Clean the inner html
      rates[i].Rating:=clean(val.toNode.innerHTML);

      Inc(i);
    end;

  i:=0;
  // Process over the XPath module
  for val in process(Data, '//td[@class="deviation number"]') do
    // Read the deviation
    if val.kind = pvkNode then begin
      // Clean the inner html
      rates[i].Deviation:=clean(val.toNode.innerHTML);

      Inc(i);
    end;

  i:=0;
  // Process over the XPath module
  for val in process(Data, '//td[@class="volatility number"]') do
    // Read the volatility
    if val.kind = pvkNode then begin
      // Clean the inner html
      rates[i].Volatility:=clean(val.toNode.innerHTML);

      Inc(i);
    end;

  i:=0;
  // Update the server information with the parsed values
  while i < k do begin
    // Get the world index
    j:=getWorldIdxByName(rates[i].Name);

    if j >= 0 then begin
      try
        // Try to parse the floats
        FWorlds[j].Rating:=StrToFloat(rates[i].Rating);
        FWorlds[j].Deviation:=StrToFloat(rates[i].Deviation);
        FWorlds[j].Volatility:=StrToFloat(rates[i].Volatility);
      except
      end;
    end;
    Inc(i);
  end;

  // One step is done
  Inc(FDone);

  // If all requests are done
  if FDone = FNeeded then begin
    Inc(FNeeded);

    // Start the request of the matches
    WebAPI.RequestJSON('/v1/wvw/matches.json', @handleMatches, False);

    // Change the state
    changeState(psMatchups);
  end;

  Result:=True;

  // Call the callback if possible
  if FOnLeader <> Nil then
    FOnLeader(Self);
end;

// Handle the received matches data
function TPredictor.handleMatches(Data:String):Boolean;
var root  :TJSONData;
    i, k  :Integer;
    arr   :TJSONArray;
    obj   :TJSONObject;
begin
  Result:=False;

  // Try to parse the json of the matches
  root:=Nil;
  with TJSONParser.Create(Data) do
    try
      root:=Parse;
    finally
      Free;
    end;

  // Read the json
  if (root <> Nil)and(root.JSONType = jtObject) then begin
    obj:=root as TJSONObject;
    arr:=obj.Find('wvw_matches', jtArray) as TJSONArray;

    // Update the length of the matches array if possible
    if arr <> Nil then
      SetLength(FMatches, arr.Count);

    i:=0;
    k:=0;
    // Iterate over the json matches
    while (arr <> Nil) and (i < arr.Count) do begin
      if (arr[i] <> Nil) and (arr[i].JSONType = jtObject) then begin
        obj:=arr[i] as TJSONObject;
        // Copy the match ID
        FMatches[k].ID:=obj.Get('wvw_match_id', '0-0');
        // Copy the servers fighting against each other in the match
        // Red = 0, Blue = 1, Green = 2
        FMatches[k].Worlds[0]:=obj.Get('red_world_id', 0);
        FMatches[k].Worlds[1]:=obj.Get('blue_world_id', 0);
        FMatches[k].Worlds[2]:=obj.Get('green_world_id', 0);

        // Download the match if valid
        if FMatches[k].ID <> '0-0' then begin
          WebAPI.RequestJSON('/v1/wvw/match_details.json?match_id='+FMatches[k].ID, @handleMatch);

          Inc(FNeeded);
          Inc(k);
        end;
      end;

      Inc(i);
    end;

    // Fixes the array length
    SetLength(FMatches, k);

    // One download is done
    Inc(FDone);

    // Call the callback if possible
    if FOnMatches <> Nil then
      FOnMatches(Self);

    Result:=True;
  end;

  // Free the json data
  if root <> Nil then
    FreeAndNil(root);
end;

// Handle the received match data
function TPredictor.handleMatch(Data:String):Boolean;
var root  :TJSONData;
    obj   :TJSONObject;
    i,k,j :Integer;
    id    :String;
    scores:TJSONArray;
begin
  Result:=False;

  // Try to parse the received data as json
  root:=Nil;
  with TJSONParser.Create(Data) do
    try
      root:=Parse;
    finally
      Free;
    end;

  if (root <> Nil) and (root.JSONType = jtObject) then begin
    // Read the match id
    obj:=root as TJSONObject;
    id:=obj.Get('match_id', '0-0');

    // Get the match index
    k:=-1;
    for i:=0 to Length(FMatches) -1 do
      if FMatches[i].ID = id then
        k:=i;

    // Find the scores
    scores:=obj.Find('scores', jtArray) as TJSONArray;

    if (k >= 0) and (scores <> Nil) and (scores.Count >= 3) then
      for i:=0 to 2 do begin
        // Update the score of the worlds
        j:=getWorldIdxByID(FMatches[k].Worlds[i]);

        if (j >= 0) and (scores[i] <> Nil) and (scores[i].JSONType = jtNumber) then begin
          FWorlds[j].Score:=scores[i].AsInteger;
          FWorlds[j].MatchID:=id;
        end;
      end;

    // Free the json data if possible because it is not needed anymore
    if root <> Nil then
      FreeAndNil(root);

    // Get the indices of the worlds in the match
    i:=getWorldIdxByID(FMatches[k].Worlds[0]);
    j:=getWorldIdxByID(FMatches[k].Worlds[1]);
    k:=getWorldIdxByID(FMatches[k].Worlds[2]);

    if (i >= 0) and (j >= 0) and (k >= 0) then begin
      // Predict the new rating of each server
      predict(i, j, k);
      predict(j, k, i);
      predict(k, i, j);
    end;

    // One step done
    Inc(FDone);

    // If the download is complete then reset the state
    if FDone = FNeeded then
      changeState(psDownloaded);

    // Call the callback if possible
    if FOnMatch <> Nil then
      FOnMatch(Self);

    Result:=True;
  end;

  // Free the json data if possible
  if root <> Nil then
    FreeAndNil(root);
end;

// Predict the new rating and deviation of the server a with the opponent b, c
procedure TPredictor.predict(a, b, c:Integer);
var player:TPlayer;
    score :Integer;

    // Arguments for the update
    rates :Array[0..1] of Double;
    rds   :Array[0..1] of Double;
    outs  :Array[0..1] of Double;
begin
  // Create the Glicko2 player
  with FWorlds[a] do
    player:=TPlayer.Create(Rating, Deviation, Volatility);
  // Read the score
  score:=FWorlds[a].Score;

  // The ratings of the opponents
  rates[0]:=FWorlds[b].Rating;
  rates[1]:=FWorlds[c].Rating;

  // The deviation of the opponents
  rds[0]:=FWorlds[b].Deviation;
  rds[1]:=FWorlds[c].Deviation;

  // The outcomes of the opponents in the interval [0; 1]
  outs[0]:=sc(score, FWorlds[b].Score);
  outs[1]:=sc(score, FWorlds[c].Score);

  // Update the Glock2 player
  player.update(rates, rds, outs);

  // Update the new rating and deviation
  FWorlds[a].NewRating:=player.Rating;
  FWorlds[a].NewDeviation:=player.RD;

  // Free the player
  player.Free;
end;

// Simulation of the match assignment with N steps
procedure TPredictor.simulate(N:Integer);
var i  :Integer;
    a,b,c :Integer;
    r     :Double;
    rates :TPredictions;
begin
  // Only in certain states
  if not (FState in [psDownloaded, psDone])or(FDone < FNeeded) then Exit;

  // Update the state
  changeState(psPredicting);

  // Reset the chances
  resetChances;

  // Create the used table
  rates:=TPredictions.Create;
  for i:=0 to Length(FWorlds) - 1 do
    rates.Add(TPredicted.Create);

  FSteps:=0;
  while FSteps < N do begin
    // Reset the rates
    for i:=0 to rates.Count -1 do begin
      // Random number for the deviation
      r:=2.0 * Random - 1.0;
      rates[i].Index:=i;
      // Use the new rating and the randomized deviation for the rating
      rates[i].Rating:=FWorlds[i].NewRating + r * FWorlds[i].NewDeviation;
      // Region ID
      rates[i].Region:=regionOf(FWorlds[i].ID);
    end;

    // Sort the rates
    rates.Sort(@compare);

    i:=0;
    // Update the chances
    while i < rates.Count - 2 do begin
      // Get the server indices
      a:=rates[i].Index;
      b:=rates[i+1].Index;
      c:=rates[i+2].Index;

      // Increment the chances to have a certain color (Red 0, Blue 1, Green 2)
      Inc(FWorlds[a].Colors[2]);
      Inc(FWorlds[b].Colors[1]);
      Inc(FWorlds[c].Colors[0]);

      // Increment the chances of each combination
      incChance(a, b);
      incChance(a, c);
      incChance(b, c);

      Inc(i, 3);
    end;

    Inc(FSteps);
  end;

  // Free the table
  rates.Free;

  // Update the state
  changeState(psDone);

  // Call the callback if possible
  if FOnSimulate <> Nil then
    FOnSimulate(Self);
end;

end.

