unit uButtons;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, Buttons, uOption, Graphics, uTypes, uData;

type
  { TOwnButton }
  // Simple toogle buttons with use of the color theme
  TOwnButton        = class(TSpeedButton)
  private
    FToogle         :Boolean;
    FShowToogle     :Boolean;
    FText           :Integer;
  protected
    procedure       Paint;override;
    procedure       setToogle(Toogle:Boolean);
    procedure       setShowToogle(Value:Boolean);
  public
    constructor     Create(TheOwner: TComponent;Value:Integer);overload;
    procedure       Toogle;
    procedure       Click;override;
    property        ShowSelected:Boolean read FShowToogle write setShowToogle;
    property        Selected:Boolean read FToogle write setToogle;
  end;

  { TImageButton }
  // Button with images specific by TImageType
  TImageButton      = class(TSpeedButton)
  private
    FType           :TImageType;
  protected
    procedure       Paint;override;
  public
    constructor     Create(TheOwner:TComponent;Value:TImageType);overload;
  end;

implementation

function min(a, b:Integer):Integer;
begin
  if a < b then
    Result:=a
  else
    Result:=b;
end;

{ TImageButton }
constructor TImageButton.Create(TheOwner:TComponent;Value:TImageType);
begin
  FType:=Value;
  inherited Create(TheOwner);
end;

procedure TImageButton.Paint;
var size  :Integer;
    poly  :Array[0..3]of TPoint;
begin
  size:=min(Width, Height)-4;

  Canvas.Brush.Color:=Colors.Back;
  Canvas.Brush.Style:=bsSolid;
  Canvas.Pen.Style:=psSolid;
  Canvas.Pen.Color:=Colors.Front;
  Canvas.Rectangle(0, 0, Width, Height);
  Canvas.Pen.Width:=2;
  Canvas.Font.Color:=Colors.Front;
  Canvas.Font.Size:=3*Size div 4;
  Canvas.Font.Name:='Webdings';

  poly[0].x:=0;poly[0].y:=0;

  case FType of
    IT_EXIT:begin
      Canvas.GetTextSize('r', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, 'r');
    end;
    IT_RESET:begin
      Canvas.GetTextSize(#96, poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, #96);
    end;
    IT_SAVE:begin
      Canvas.GetTextSize('w', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, 'w');
    end;
  end;

  Canvas.Brush.Style:=bsClear;
  Canvas.Pen.Color:=Colors.Front;
  Canvas.Pen.Style:=psSolid;
  Canvas.Rectangle(0, 0, Width, Height);
end;

{ TOwnButton }
constructor TOwnButton.Create(TheOwner:TComponent;Value:Integer);
begin
  FToogle:=True;
  FText:=Value;
  inherited Create(TheOwner);
end;

procedure TOwnButton.Toogle;
begin
  FToogle:=not FToogle;
end;

procedure TOwnButton.setToogle(Toogle:Boolean);
begin
  FToogle:=Toogle;
  Invalidate;
end;

procedure TOwnButton.setShowToogle(Value:Boolean);
begin
  FShowToogle:=Value;
  Invalidate;
end;

procedure TOwnButton.Click;
begin
  Toogle;
  inherited Click;
end;

procedure TOwnButton.Paint;
var cx, cy     :Integer;
    s          :String;
begin
  cx:=0;cy:=0;
  if FToogle then begin
    Canvas.Brush.Color:=Colors.Front;
    Canvas.Pen.Color:=Colors.Back;
  end else begin
    Canvas.Brush.Color:=Colors.Back;
    Canvas.Pen.Color:=Colors.Front;
  end;
  Canvas.Font.Color:=Colors.Font;
  Canvas.Rectangle(0, 0, Width, Height);

  s:='';
  Canvas.GetTextSize(s, cx, cy);
  Canvas.TextOut((Width-cx)div 2, (Height-cy)div 2, s);
end;

end.

