unit uTypes;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses Sockets;

type
  { Enumerations }
  // Image button types
  TImageType        = (IT_EXIT, IT_RESET, IT_SAVE);

  // Race
  TRace             = (R_ASURA, R_CHARR, R_HUMAN, R_NORN, R_SYLVARI, R_UNKNOWN);

  // Professions
  TProfession       = (PF_UNKNOWN, PF_GUARDIAN, PF_WARRIOR, PF_ENGINEER,
                       PF_RANGER, PF_THIEF, PF_ELEMENTALIST, PF_MESMER,
                       PF_NECROMANCER, PF_REVENANT);

  { TFieldOfView }
  TFieldOfView      = record
    Horizontal      :Single;
    Vertical        :Single;
  end;

  { TCoordinate }
  TCoordinate       = record
    x, y            :Integer;
    Visible         :Boolean;
  end;

  { Callbacks }
  TProcCallback     = procedure of object;
  TConvCallback     = function(x, y, z:Single):TCoordinate of object;


  { TMumbleInfo }
  // packed vector type, (x, y and z)
  TVector           = packed Array[0..2] of Single;

  // packed record for avatar data, position and direction
  TAvatar           = packed record
    Position        : TVector;
    Front           : TVector;
    Top             : TVector;
  end;
  TCamera           = TAvatar;

  // packed record for the context data
  TContextData      = packed record
    Address         : packed record
      case Boolean of
        True: (IPv4 : sockaddr_in);
        False:(IPv6 : sockaddr_in6);
    end;
    MapID           : DWORD;
    MapType         : DWORD;
    ShardID         : DWORD;
    Instance        : DWORD;
    BuildID         : DWORD;
  end;

  // Mumble names, identity and description
  TNameMem          = packed Array[0..255] of Widechar;
  TIdentityMem      = TNameMem;
  TDescriptionMem   = packed Array[0..2047] of Widechar;

  // mumble definition of the linked memory
  TLinkedMem        = packed record
    uiVersion       : DWORD;
    uiTick          : DWORD;
    Avatar          : TAvatar;
    Name            : TNameMem;
    Camera          : TCamera;
    Identity        : TIdentityMem;
    ContextLen      : DWORD;
    Context         : packed Array[0..255] of Char;
    Description     : TDescriptionMem;
  end;

  // internal mumble info
  TMumbleInfo       = record
    Name            : AnsiString;
    Profession      : TProfession;
    Race            : TRace;
    TeamColor       : Integer;
    FoVVertical     : Single;
    Commander       : Boolean;
    Map             : Integer;
    World           : Integer;
    Avatar          : TAvatar;
    Camera          : TCamera;
    Context         : TContextData;
  end;

  { Pointer }
  PLinkedMem        = ^TLinkedMem;
  PAvatar           = ^TAvatar;
  PCamera           = ^TCamera;

implementation

end.

