unit uMumble;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, ExtCtrls, jsonparser, fpjson, uTypes,
  {$IF defined(win32)}Windows{$ELSE}unix, BaseUnix{$ENDIF};

type
{ TMumble }
  TMumble           = class
  private
    FFPSTimer       :TTimer;
    FTimer          :TIdleTimer;
    FData           :TMumbleInfo;
    FLinkedMem      :PLinkedMem;
    FLastTick       :DWORD;
    FFPS            :Integer;
    {$IF defined(win32)}
    FMemFile        :Handle;
    {$ELSE}
    FMemName        :String;
    FMemFD          :Integer;
    {$ENDIF}
    FCallback       :TProcCallback;
    FError          :String;
    FLoaded         :Boolean;
    function        getAvatar:TAvatar;
    function        getCamera:TCamera;
    function        getCommander:Boolean;
    function        getContext:TContextData;
    function        getEnable:Boolean;
    function        getFoV:Single;
    function        getName:AnsiString;
    function        getMap:Integer;
    function        getProfession:TProfession;
    function        getProfessionName:String;
    function        getRace:TRace;
    function        getRaceName:String;
    function        getTeamColor:Integer;
    function        getWorld:Integer;

    procedure       ConvertIdentity(Mem:TIdentityMem);
    procedure       Init;
    procedure       RefreshFPS(Sender:TObject);
    procedure       Refresh(Sender:TObject);
    procedure       setEnable(Enabled:Boolean);
  public
    constructor     Create(Callback:TProcCallback;Period:Integer=25);
    destructor      Destroy;override;
    property        LastError:String read FError;
    property        Avatar:TAvatar read getAvatar;
    property        Camera:TCamera read getCamera;
    property        Commander:Boolean read getCommander;
    property        Context:TContextData read getContext;
    property        Enabled:Boolean read getEnable write setEnable;
    property        FoVVertical:Single read getFoV;
    property        FPS:Integer read FFPS;
    property        Name:AnsiString read getName;
    property        Map:Integer read getMap;
    property        Profession:TProfession read getProfession;
    property        ProfessionName:String read getProfessionName;
    property        Race:TRace read getRace;
    property        RaceName:String read getRaceName;
    property        TeamColor:Integer read getTeamColor;
    property        World:Integer read getWorld;
  end;

{$IF defined(win32)}
{$ELSE}
  function shm_open(__name:PChar; __oflag:LongInt; __mode:MODE_T):LongInt;
           cdecl; external 'rt' name 'shm_open';
  function shm_unlink(__name:PChar):LongInt; cdecl; external 'rt' name 'shm_unlink';
{$ENDIF}

implementation

function Max(a, b:Longint):Longint;
begin
  if a < b then
    Result:=b
  else
    Result:=a;
end;

procedure memcpy(dest: Pointer; const source: Pointer; size: Integer);
var i      :Integer;
    s, d   :PByte;
begin
  s := PByte(source);
  d := PByte(dest);

  for i:=0 to size-1 do begin
    d^:=s^;
    Inc(d);
    Inc(s);
  end;
end;

{ TMumble}
// Constructor
constructor TMumble.Create(Callback:TProcCallback;Period:Integer);
begin
  inherited Create;

  // Create TTimer
  FTimer:=TIdleTimer.Create(Nil);
  FTimer.Enabled:=False;
  FTimer.Interval:=Max(0, Period);
  FTimer.OnTimer:=@Refresh;

  // Create the Timer fot the fps calculation
  FFPSTimer:=TTimer.Create(Nil);
  FFPSTimer.Enabled:=False;
  FFPSTimer.Interval:=1000;
  FFPSTimer.OnTimer:=@RefreshFPS;

  FLastTick:=0;
  FFPS:=0;

  // Set callback
  FCallback:=Callback;

  // Set default values
{$IF defined(win32)}
  FMemFile:=0;
{$ELSE}
  FMemName:=Format('/MumbleLink.%d', [fpgetuid()]);
  FMemFD:=-1;
{$ENDIF}
  FLinkedMem:=Nil;
  FLoaded:=False;

  // Initialize
  Init;
end;

// Destructor
destructor TMumble.Destroy;
begin
  // Free timer
  FreeAndNil(FTimer);

{$IF defined(win32)}
  // Unmap linked memory
  if FLinkedMem <> nil then
     UnmapViewOfFile(FLinkedMem);

  // Close memory file handle
  if FMemFile <> 0 then
     CloseHandle(FMemFile);
{$ELSE}
  if FMemFD >= 0 then
      shm_unlink(PChar(FMemName));

  if FLinkedMem <> Nil then
    fpmUnMap(FLinkedMem, sizeof(TLinkedMem));
{$ENDIF}
end;

// Initialize mumble
procedure TMumble.Init;
begin
{$IF defined(win32)}
  // Try to open file mapping
  if FMemFile = 0 then
    FMemFile := OpenFileMapping(FILE_MAP_ALL_ACCESS, False, PChar('MumbleLink'));

  // else try to create file mapping
  if FMemFile = 0 then
    FMemFile:= CreateFileMapping(INVALID_HANDLE_VALUE, nil, PAGE_READWRITE, 0, sizeof(TLinkedMem), PChar('MumbleLink'));

  // Map the memory file
  if FMemFile <> 0 then begin
    FLinkedMem:=PLinkedMem(MapViewOfFile(FMemFile, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(TLinkedMem)));

    // Update memory
    if FLinkedMem = nil then begin
      CloseHandle(FMemFile);
      FMemFile:=0;
    end else begin
      Refresh(Nil);
      FLoaded:=True;
    end;
  end;
{$ELSE}
  FMemName := Format('/MumbleLink.%d', [fpgetuid()]);
  FMemFD := shm_open(PChar(FMemName), O_RDONLY, S_IRUSR or S_IWUSR);

  if FMemFD >= 0 then begin
    FLinkedMem :=PLinkedMem(fpmmap(Nil, sizeof(TLinkedMem), PROT_READ, MAP_SHARED, FMemFD, 0));

    if FLinkedMem = PLinkedMem(-1) then begin
      FLinkedMem := Nil;
      FError:='Error: Can not map memory file';
    end else begin
      Refresh(Nil);
      FLoaded:=True;
    end;
  end else
    FError:='Error: Can not open memory file';
{$ENDIF}
end;

// Convert mumble identity and parse data
procedure TMumble.ConvertIdentity(Mem:TIdentityMem);
  function cmp(A,B:String):Boolean;
  begin
    Result:=(Pos(A,B)=1) and (Pos(B,A)=1);
  end;

var Key       :AnsiString;
    i          :Integer;
    json       :TJSONParser;
    j          :TJSONData;
begin
  json:=TJSONParser.Create(Mem);
  j:=json.Parse;
  if (j<>Nil)and(not j.isNull)and(j.JSONType = jtObject) then begin
    for i:=0 to j.Count-1 do begin
      Key:=TJSONObject(j).Names[i];

      // Read avatar name
      if cmp('name', Key) and (j.Items[i].JSONType = jtString) then
         FData.Name:=j.Items[i].AsString

      // Read avatar profession
      else if cmp('profession', Key) and (j.Items[i].JSONType = jtNumber) then
         FData.Profession:=TProfession(j.Items[i].AsInteger)

      // Read avatar race
      else if cmp('race', Key) and (j.Items[i].JSONType = jtNumber) then
         FData.Race:=TRace(j.Items[i].AsInteger)

      // Read current map id
      else if cmp('map_id', Key) and (j.Items[i].JSONType = jtNumber) then
         FData.Map:=j.Items[i].AsInteger

      // Read current world id (overflow set the highest significant bit
      else if cmp('world_id', Key) and (j.Items[i].JSONType = jtNumber) then
         FData.World:=j.Items[i].AsInteger

      // Read field of view
      else if cmp('fov', Key) and (j.Items[i].JSONType = jtNumber) then
         FData.FoVVertical:=j.Items[i].AsFloat

      // Read team color
      else if cmp('team_color_id', Key) and (j.Items[i].JSONType = jtNumber) then
         FData.TeamColor:=j.Items[i].AsInteger

      // Read commander status
      else if cmp('commander', Key) and (j.Items[i].JSONType = jtBoolean) then
         FData.Commander:=j.Items[i].AsBoolean;
    end;
    j.Free;
  end;
  json.Free;
end;

// Mumble refresh procedure, called by timer
procedure TMumble.Refresh(Sender:TObject);
var fcheck :Boolean;
begin
{$IF defined(win32)}
  fcheck:=FMemFile<>0;
{$ELSE}
  fcheck:=FMemFD >= 0;
{$ENDIF}
  if (fcheck)and(FLinkedMem <> nil)and(FLinkedMem^.uiVersion=2) then begin
    // Fill mumble structure
    ConvertIdentity(FLinkedMem^.Identity);
    Memcpy(@(FData.Avatar), @(FLinkedMem^.Avatar), sizeof(TAvatar));
    Memcpy(@(FData.Camera), @(FLinkedMem^.Camera), sizeof(TCamera));
    Memcpy(@(FData.Context), @(FLinkedMem^.Context), sizeof(TContextData));

    // if loaded, run callback
    if (FCallback <> Nil)and(FLoaded) then
      FCallback;
  end else if (not fcheck)or(FLinkedMem=Nil) then
    // No linked memory, than reinitalize
    Init;
end;

// Mumble refresh fps procedure, called by timer
procedure TMumble.RefreshFPS(Sender:TObject);
var fcheck :Boolean;
begin
{$IF defined(win32)}
  fcheck:=FMemFile<>0;
{$ELSE}
  fcheck:=FMemFD >= 0;
{$ENDIF}
  if (fcheck)and(FLinkedMem <> nil)and(FLinkedMem^.uiVersion=2) then begin
    // Only update if the last tick is valid
    if FLastTick <> 0 then
      FFPS:=(FLinkedMem^.uiTick - FLastTick);
    FLastTick:=FLinkedMem^.uiTick;
  end;
end;

// Getter
function TMumble.getAvatar:TAvatar;
begin
  if FLoaded then
    Result:=FData.Avatar;
end;

function TMumble.getCamera:TCamera;
begin
  if FLoaded then
    Result:=FData.Camera;
end;

function TMumble.getCommander:Boolean;
begin
  if FLoaded then
    Result:=FData.Commander
  else
    Result:=False;
end;

function TMumble.getContext:TContextData;
begin
  if FLoaded then
    Result:=FData.Context;
end;

function TMumble.getEnable:Boolean;
begin
  Result:=FTimer.Enabled;
end;

function TMumble.getFoV:Single;
begin
  Result:=FData.FoVVertical;
end;

function TMumble.getName:AnsiString;
begin
  if FLoaded then
    Result:=FData.Name
  else
    Result:='';
end;

function TMumble.getMap:Integer;
begin
  if FLoaded then
    Result:=FData.Map
  else
    Result:=0;
end;

function TMumble.getProfession:TProfession;
begin
  if FLoaded then
    Result:=FData.Profession
  else
    Result:=PF_UNKNOWN;
end;

function TMumble.getProfessionName:String;
begin
  case FData.Profession of
    PF_ELEMENTALIST:
      Result:='Elementalist';
    PF_ENGINEER:
      Result:='Engineer';
    PF_GUARDIAN:
      Result:='Guardian';
    PF_MESMER:
      Result:='Mesmer';
    PF_NECROMANCER:
      Result:='Necromancer';
    PF_RANGER:
      Result:='Ranger';
    PF_REVENANT:
      Result:='Revenant';
    PF_THIEF:
      Result:='Thief';
    PF_WARRIOR:
      Result:='Warrior';
  else
    Result:='';
  end;
end;

function TMumble.getRace:TRace;
begin
  Result:=FData.Race;
end;

function TMumble.getRaceName:String;
begin
  case FData.Race of
    R_ASURA:
      Result:='Asura';
    R_CHARR:
      Result:='Charr';
    R_HUMAN:
      Result:='Human';
    R_NORN:
      Result:='Norn';
    R_SYLVARI:
      Result:='Sylvari';
  else
    Result:='';
  end;
end;

function TMumble.getTeamColor:Integer;
begin
  if FLoaded then
    Result:=FData.TeamColor
  else
    Result:=0;
end;

function TMumble.getWorld:Integer;
begin
  if FLoaded then
    Result:=FData.World
  else
    Result:=0;
end;

// Timer setter
procedure TMumble.setEnable(Enabled:Boolean);
begin
  FTimer.Enabled:=Enabled;
  FFPSTimer.Enabled:=Enabled;
end;

end.

