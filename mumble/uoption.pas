unit uOption;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Graphics;

type
  { TColors }
  // Background color: back
  // Front color: front
  // Font color: font
  TColors          =record
    Back,Front     :TColor;
    Font           :TColor;
  end;

var
  // Set the default colors
  Colors               :TColors = (
    // Back is Black
    Back        :$000000;
    // Front is Deep Pine (from GuildWars2 color api)
    Front       :$4F6E9D;
    // Font color is a light gray
    Font        :$EEEEEE;
  );


implementation

end.

