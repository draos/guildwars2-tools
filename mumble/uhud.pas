unit uHUD;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, uOption, uTypes, uButtons, uMumble, Sockets, DateUtils, windows,
  IniFiles;

type

  { THUD }
  THUD               = class(TForm)
    MainBox          :TPanel;
    TopBack          :TPaintBox;
    TopPanel         :TPanel;
    View             :TTreeView;
    procedure        BackPaint(Sender:TObject);
    procedure        FormCreate(Sender:TObject);
    procedure        FormDestroy(Sender:TObject);
    procedure        TopBackPaint(Sender: TObject);
    procedure        ControlClick(Sender:TObject);
    procedure        DragMouseLeave(Sender:TObject);
    procedure        DragMouseMove(Sender:TObject;{%H-}Shift:TShiftState;X,Y:Integer);
    procedure        DragMouseUp(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);
    procedure        DragMouseDown(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);
  private
    FIconButton      :Array[TImageType] of TImageButton;
    FMove            :record
      X,Y            :Integer;
      Drag           :Boolean;
    end;
    FLast            :record
      Time           :TDateTime;
      Pos            :TVector;
      Enable         :Boolean;
    end;
    FTracker         :record
      ini            :TIniFile;
      idx            :Integer;
    end;
    FMaxVelocity     :Double;
    FDistance        :Double;
    FStart           :TDateTime;

    FMumble          :TMumble;
    procedure        InitButtons;
    procedure        ReleaseButtons;
    procedure        MumbleRefresh;
    procedure        SavePosition;
  public
  end;

var
  HUD: THUD;

const
  TITLE    = 'GuildWars2 Mumble Reader';

implementation

{$R *.lfm}

{ THUD }

// Implements a moving form
procedure THUD.DragMouseLeave(Sender: TObject);
begin
  FMove.Drag:=False;
end;

procedure THUD.DragMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if FMove.Drag then begin
    Left:=Left+X-FMove.X;
    Top:=Top+Y-FMove.Y;
  end;
end;

procedure THUD.DragMouseDown(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  FMove.Drag:=True;
  FMove.X:=X;
  FMove.Y:=Y;
end;

procedure THUD.DragMouseUp(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  FMove.Drag:=False;
end;

// Paint procedure for the Gradients
procedure THUD.BackPaint(Sender: TObject);
var rect  :TRect;
begin
  if Sender is TPaintBox then
    with Sender as TPaintBox do begin
      rect.Top:=0;
      rect.Left:=0;
      rect.Right:=Width;
      rect.Bottom:=Height;
      Canvas.GradientFill(rect, Colors.Back, Colors.Front, gdVertical);
    end;
end;

// Initialize the Form
procedure THUD.FormCreate(Sender: TObject);
begin
  // Create TOverlay and show
  FMumble:= TMumble.Create(@MumbleRefresh);

  // Initialize buttons
  InitButtons;

  View.BackgroundColor:=Colors.Back;
  View.Font.Color:=Colors.Font;
  View.Color:=Colors.Front;;
  View.SelectionColor:=Colors.Front;
  View.ExpandSignColor:=Colors.Front;
  View.TreeLineColor:=Colors.Front;

  FMumble.Enabled:=True;
  FMaxVelocity:=0.0;
  FDistance:=0.0;
  FStart:=Now;
  FLast.Enable:=False;

  FTracker.ini:=TIniFile.Create('Tracking.ini');
  FTracker.idx:=0;
end;

procedure THUD.SavePosition;
var section :String;
begin
  section:=IntToStr(FTracker.idx);
  with FTracker.ini do begin
    WriteInteger(section, 'MapID', FMumble.Map);
    WriteFloat(section, 'X', FMumble.Avatar.Position[0]);
    WriteFloat(section, 'Y', FMumble.Avatar.Position[1]);
    WriteFloat(section, 'Z', FMumble.Avatar.Position[2]);
  end;
  Inc(FTracker.idx);
end;

procedure THUD.MumbleRefresh;
var s   :String;
    i   :DWORD;
    r, t:Double;
    d   :Double;
const
  Info    = 0;
  Context = 9;
  Avatar  = 17;
  Camera  = 30;
  Velocity= 43;
  Records = 49;
begin
  View.BeginUpdate;

  View.Items[Info+1].Text:=Format('Name:%19S', [FMumble.Name]);
  View.Items[Info+2].Text:=Format('Commander:%14S', [BoolToStr(FMumble.Commander, true)]);
  View.Items[Info+3].Text:=Format('Field of View: %9.4F', [FMumble.FoVVertical]);
  View.Items[Info+4].Text:=Format('Map ID:  %15U', [FMumble.Map]);
  View.Items[Info+5].Text:=Format('Profession:%13S', [FMumble.ProfessionName]);
  View.Items[Info+6].Text:=Format('Race:      %13S', [FMumble.RaceName]);
  View.Items[Info+7].Text:=Format('Team Color:%12.8Xh', [FMumble.TeamColor]);
  View.Items[Info+8].Text:=Format('World:   %14.8Xh', [FMumble.World]);

  // Fill Context
  with FMumble.Context.Address do
    if IPv6.sin6_family = AF_INET6 then
      s:=NetAddrToStr6(IPv6.sin6_addr)
    else
      s:=NetAddrToStr(IPv4.sin_addr);
  View.Items[Context+1].Text:=Format('Address: %15S', [s]);
  View.Items[Context+2].Text:=Format('Port:    %15U', [FMumble.Context.Address.IPv4.sin_port]);
  View.Items[Context+3].Text:=Format('Map ID:  %15U', [FMumble.Context.MapID]);
  View.Items[Context+4].Text:=Format('Map Type:%14.8Xh', [FMumble.Context.MapType]);
  View.Items[Context+5].Text:=Format('Shard:   %14.8Xh', [FMumble.Context.ShardID]);
  View.Items[Context+6].Text:=Format('Instance:%14.8Xh', [FMumble.Context.Instance]);
  View.Items[Context+7].Text:=Format('Build:   %15.0N', [Single(FMumble.Context.BuildID)]);

  // Fill Avatar
  with FMumble.Avatar do begin
    for i:=0 to 2 do begin
      View.Items[Avatar+i+2].Text:=Format('%S:          %10.2F', [Chr(Ord('X')+i), Position[i]]);
      View.Items[Avatar+i+6].Text:=Format('%S:          %10.2F', [Chr(Ord('X')+i), Front[i]]);
      View.Items[Avatar+i+10].Text:=Format('%S:          %10.2F', [Chr(Ord('X')+i), Top[i]]);
    end;
  end;

  // Fill Camera
  with FMumble.Camera do begin
    for i:=0 to 2 do begin
      View.Items[Camera+i+2].Text:=Format('%S:          %10.2F', [Chr(Ord('X')+i), Position[i]]);
      View.Items[Camera+i+6].Text:=Format('%S:          %10.2F', [Chr(Ord('X')+i), Front[i]]);
      View.Items[Camera+i+10].Text:=Format('%S:          %10.2F', [Chr(Ord('X')+i), Top[i]]);
    end;
  end;

  // Fill Velocity
  t:=MilliSecondsBetween(Now, FLast.Time) / 1000.0;
  if t >= 0.1 then begin
    FLast.Time:=Now;
    r:=0;
    for i:=0 to 2 do begin
      d:=FMumble.Avatar.Position[i]-FLast.Pos[i];
      r:=r+Sqr(d);
      View.Items[Velocity+i+1].Text:=Format('%S:            %10.2F', [Chr(Ord('X')+i), d / t]);
      FLast.Pos[i]:=FMumble.Avatar.Position[i];
    end;
    if FLast.Enable then begin
      r:=Sqrt(r);
      if r <= 12 then
        FDistance:=FDistance+r;
      d:=r/t;
      if (r<=6)and(d > FMaxVelocity) then
        FMaxVelocity:=d;

      View.Items[Velocity+4].Text:=Format('Average:      %10.2F', [d]);
      View.Items[Velocity+5].Text:=Format('Maximal:      %10.2F', [FMaxVelocity]);
    end else
      FLast.Enable:=True;
  end;

  t:=Now-FStart;
  View.Items[Records+1].Text:=Format('FPS:          %10U', [FMumble.FPS]);
  View.Items[Records+2].Text:=Format('Distance:     %10.2F', [FDistance]);
  View.Items[Records+3].Text:=FormatDateTime('"Time:       "hh:nn:ss:zz', t);
  t:=MilliSecondsBetween(Now, FStart) / 1000;
  View.Items[Records+4].Text:=Format('Velocity:     %10.2F', [FDistance / t]);

  View.EndUpdate;
end;

// Destructor
procedure THUD.FormDestroy(Sender: TObject);
begin
  // Release buttons
  ReleaseButtons;
end;

// Paints the top panel
procedure THUD.TopBackPaint(Sender: TObject);
var cx, cy       :Integer;
begin
  with TopBack do begin
    Color:=Colors.Back;
    Canvas.Brush.Color:=Colors.Back;
    Canvas.Brush.Style:=bsSolid;
    Canvas.Pen.Color:=Colors.Back;
    Canvas.Font.Color:=Colors.Font;
    Canvas.Rectangle(0, 0, Width, Height);

    cx:=0;cy:=0;
    Canvas.GetTextSize(TITLE, cx, cy);
    Canvas.TextOut((Width-cx)div 2, (Height-cy)div 2, TITLE);
  end;
end;

// Procedure for the MainBox controls like close, minimize and maximize
procedure THUD.ControlClick(Sender:TObject);
begin
  if Sender = FIconButton[IT_EXIT] then
    Close
  else if Sender = FIconButton[IT_RESET] then begin
    FMaxVelocity:=0.0;
    FDistance:=0.0;
    FStart:=Now;
  end else if Sender = FIconButton[IT_SAVE] then
    SavePosition;
end;

// Initialize the buttons
procedure THUD.InitButtons;
var it    :TImageType;
begin
  // Create the control buttons
  for it in TImageType do begin
    FIconButton[it]:=TImageButton.Create(Self, it);

    with FIconButton[it] do begin
      // Exit, Min and Max Button on the right corner of the top panel
      Parent:=TopPanel;
      Align:=alRight;
      Width:=TopPanel.Height;
      Top:=0;
      OnClick:=@ControlClick;
      // Position of each button
      case it of
        IT_EXIT: Left:=150;
        IT_RESET:Left:=149;
      end;
      TabStop:=False;
    end;
  end;
end;

// Release buttons
procedure THUD.ReleaseButtons;
var it    :TImageType;
begin
  for it in TImageType do
    FreeAndNil(FIconButton[it]);
end;

end.

