#GuildWars2 Tools

**Emblem:** Guild Emblem Creator and Editor

**Mumble Reader:** Viewer for the Mumble Interface of GuildWars2.

**Map Viewer:** Tile-based Map Viewer and Printer.

**Prediction:** Predict the new WvW Ratings of each Server and the Chance to fight against.


##Using

**IDE:** [Lazarus 1.0.14](http://www.lazarus.freepascal.org)

**Compiler:** fpc 2.6.2

**API:** [*GuildWars 2 API*](http://wiki.guildwars2.com/wiki/API:Main) and [*Mumble*](http://wiki.guildwars2.com/wiki/Mumble)

**Libaries:** [*Internet Tools*](http://wiki.lazarus.freepascal.org/Internet_Tools)


##[GuildWars 2 API Terms of Use](http://wiki.guildwars2.com/wiki/API:Main#Guild_Wars_2_API_Terms_of_Use)
These APIs are wholly owned by ArenaNet, LLC ("ArenaNet"). Any use of the APIs must comply with the [Website Terms of Use](https://www.guildwars2.com/en/legal/website-terms-of-use/) and [Content Terms of Use](https://www.guildwars2.com/en/legal/guild-wars-2-content-terms-of-use/), however you may use the APIs to make commercial products so long as they are otherwise compliant and do not compete with ArenaNet. ArenaNet may revoke your right to use the APIs at any time. In addition, ArenaNet may create and/or amend any terms or conditions applicable to the APIs or their use at any time and from time to time. You understand and agree that ArenaNet is in the process of developing a full license agreement for these APIs and ArenaNet will publish it when it is complete. Your continued use of the APIs constitutes acceptance of the full license agreement and any related terms or conditions when they are posted.