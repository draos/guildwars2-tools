unit uTileMap;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, Graphics, uWebAPI, ExtCtrls, math, Controls, types,
  syncobjs;

type
  // Forward declarations
  TTile         = class;
  TTileMap      = class;

  TTileCallback = procedure(Tile:TTile) of object;

  // States of a tile
  TTileState    = (tsNone, tsLoading, tsCached, tsError);

  { TTile }
  TTile         = class
  private
    FCallback   :TTileCallback;
    FX, FY, FZ  :Integer;
    FKey        :String;
    FState      :TTileState;
    FPicture    :TGraphic;

    function    response(Data, Key:String; Tries:Integer):Boolean;
  public
    constructor Create(x, y, z:Integer; url, key:String; Callback:TTileCallback);
    destructor  Destroy;override;

    property    Picture:TGraphic read FPicture;
    property    State:TTileState read FState;
    property    X:Integer read FX;
    property    Y:Integer read FY;
    property    Zoom:Integer read FZ;
  end;

  // Event handler types
  TTileMapEvent = procedure(Map:TTileMap) of object;
  TTileMapLoaded= procedure(Map:TTileMap; x, y, Zoom:Integer) of object;

  // Position of the map
  TTileMapPos   = (
      tmpTopLeft, tmpTopRight, tmpBottomLeft, tmpBottomRight, tmpLeft, tmpTop,
      tmpRight, tmpBottom, tmpCenter, tmpPosition
  );

  { TTileMap }
  TTileMap = class(TPaintBox)
  private
    // Cache of all tiles in the map. Index can be calculated with indexOf(...)
    FCache      :Array of TTile;
    FCacheDir   :String;
    FCacheEnable:Boolean;
    // The cached background image
    FBack       :TBitmap;
    FBackLoaded :Boolean;
    // Area of the current zoom level
    FArea       :TRect;
    // Rectangle of the visible area
    FView       :TRect;
    // The url with modifier {s}, {x}, {y}, {z}
    FUrl        :String;
    // The Zoom constraints and current value
    FMinZoom,
    FMaxZoom    :Integer;
    FZoom       :Integer;
    // Mouse control
    FMouse      :record
      X, Y        :Integer;
      Dragging    :Boolean;
      LeftButton  :Boolean;
      RightButton :Boolean;
    end;
    // Minimal and maximal border of the domains for {s}
    FMinDomain,
    FMaxDomain  :Integer;

    // Events
    FOnMove     :TTileMapEvent;
    FOnZoom     :TTileMapEvent;
    FOnLoaded   :TTileMapLoaded;
    FOnError    :TTileMapLoaded;

    // Calculate the mouse position in world space
    function    getMouseCoord:TPoint;
    // Calculate the offset index of the zoom level
    function    offset(z:Integer):Integer;
    // Calculate the index of the tile
    function    indexOf(x, y, z:Integer):Integer;
    // Convert the view space to the tile space (zoom level dependent)
    function    viewToTileX(x:Integer; z:Integer=-1):Integer;
    function    viewToTileY(y:Integer; z:Integer=-1):Integer;
    // Convert the tile space into the view space (zoom level dependent)
    function    tileToViewX(x:Integer; z:Integer=-1):Integer;
    function    tileToViewY(y:Integer; z:Integer=-1):Integer;
    // Current visible tile rect
    function    tileRect:TRect;

    // Draw a tile
    procedure   DrawTile(x, y, z:Integer);overload;

    // Callback if a tile is loaded
    procedure   DoLoadedTile(Tile:TTile);
    // Repaint the back buffer
    procedure   DoRepaint;
    // Check the view
    procedure   DoCheckView;

    // Move the view to the given position
    procedure   moveView(x, y:Integer);
    // Move the view relative to the current position
    procedure   moveViewRel(dx, dy:Integer);
    // Set the cache and create the directory if needed
    procedure   setCacheDir(Value:String);
    // Set the zoom level
    procedure   setZoom(Value:Integer);
  protected
    // Mouse functions
    function    DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;
    procedure   MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure   MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure   MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    // Other procedures
    procedure   Paint; override;
    procedure   Resize; override;
  public
    constructor Create(Url:String; MinZoom, MaxZoom:Integer; MinDomain:Integer=0; MaxDomain:Integer=0);overload;
    destructor  Destroy; override;

    // Conversion methods
    function    CoordToView(x, y:Integer):TPoint;
    function    viewable(const r:TRect):Boolean;

    // Move the view to specified positions
    procedure   Move(Position:TTileMapPos; x:Integer=0; y:Integer=0);

    // Draw a label on the map
    procedure   TextOut(const s:String; x, y:Integer);overload;
    procedure   TextOut(const s:String; x, y:Integer; Outer, Inner:TColor);overload;

    // Draw a tile on a canvas
    procedure   DrawRect(ACanvas:TCanvas; Zoom:Integer; ARect:TRect);
    procedure   DrawTo(ACanvas:TCanvas; x, y, Zoom:Integer);
    procedure   DrawTile(ACanvas:TCanvas; x, y, Zoom:Integer);overload;

    // Draw a graphic
    procedure   Draw(x, y:Integer; const Pic:TGraphic; ScaleIcon:Integer=1);

    // Load an entire zoom level
    procedure   LoadZoom(Zoom:Integer);

    // Cache
    property    EnableCache:Boolean read FCacheEnable write FCacheEnable;
    property    Cache:String read FCacheDir write setCacheDir;

    // Get the minimal and maximal domain
    property    MinDomain:Integer read FMinDomain;
    property    MaxDomain:Integer read FMaxDomain;

    // Get the minimal and maximal zoom level
    property    MinZoom:Integer read FMinZoom;
    property    MaxZoom:Integer read FMaxZoom;

    // Mouse coordinate
    property    MouseCoordinate:TPoint read getMouseCoord;

    // Events
    property    OnErrorTile:TTileMapLoaded write FOnError;
    property    OnLoadedTile:TTileMapLoaded write FOnLoaded;
    property    OnMove:TTileMapEvent write FOnMove;
    property    OnZoom:TTileMapEvent write FOnZoom;

    // Size of the current map zoom
    property    Area:TRect read FArea;
    // Current visible area
    property    VisibleArea:TRect read FView;
    // Current zoom level
    property    Zoom:Integer read FZoom write setZoom;
  end;

  { TTilePrinter }
  TPrinterState = (psNone, psLoading, psPrinted, psError);
  TTilePrinter  = class;
  TPrinterEvent = procedure(Sender:TTilePrinter) of object;
  TTilePrinter  = class
  private
    // Event
    FOnPre      :TPrinterEvent;
    FOnEvent    :TPrinterEvent;
    FOnPost     :TPrinterEvent;
    // Caches
    FCache      :Array of TTile;
    FState      :Array of TPrinterState;
    // Image cache
    FCacheDir   :String;
    FCacheEnable:Boolean;
    // Loaded tiles
    FLoaded     :Integer;
    FErrors     :Integer;
    FCount      :Integer;
    // Loaded levels
    FLevel      :Integer;
    // The url with modifier {s}, {x}, {y}, {z}
    FUrl        :String;
    // Zooms
    FMinZoom    :Integer;
    FMaxZoom    :Integer;
    // Domains
    FMinDomain  :Integer;
    FMaxDomain  :Integer;
    FLock       :TCriticalSection;
    // Buffer
    FBuffer     :TPortableNetworkGraphic;
    // Printed are
    FArea       :TRect;

    // Calculate the offset index of the zoom level
    function    offset(z:Integer):Integer;
    // Calculate the index of the tile
    function    indexOf(x, y, z:Integer):Integer;
    function    tileRect(x, y, Zoom:Integer):TRect;

    procedure   setCache(Value:String);
    procedure   Init;
    procedure   DoLoadedTile(Tile:TTile);
    procedure   LoadTile(x, y, Zoom:Integer; Url:String);
  public
    constructor Create(Tiles:TTileMap);overload;
    constructor Create(Url:String; MinZoom, MaxZoom:Integer; MinDomain:Integer=0; MaxDomain:Integer=0);overload;
    destructor  Destroy;override;

    // Fill the buffer with a color
    procedure   Fill(Color:TColor;Style:TBrushStyle=bsSolid);

    // Set up the font
    procedure   SetupFont(Font:TFont);
    // Draw a label on the map
    procedure   TextOut(const s:String; x, y:Integer);overload;
    procedure   TextOut(const s:String; x, y:Integer; Outer, Inner:TColor);overload;
    // Draw a graphic
    procedure   Draw(x, y:Integer; const Pic:TGraphic; ScaleIcon:Integer=1);

    // Save the buffer
    procedure   Save(const AFileName:String);

    // Load all tiles on the given level inside the rectangle
    procedure   LoadLevel(Level:Integer; Area:TRect);

    // Caching
    property    Cache:String read FCacheDir write setCache;
    property    EnableCache:Boolean read FCacheEnable write FCacheEnable;

    // Tile statistic
    property    DamagedTiles:Integer read FErrors;
    property    PrintedTiles:Integer read FLoaded;
    property    CountTiles:Integer read FCount;

    // Events
    property    OnChange:TPrinterEvent write FOnEvent;
    property    OnPostProcessing:TPrinterEvent write FOnPost;
    property    OnPreProcessing:TPrinterEvent write FOnPre;
  end;

// Constants
const
  // Maximum tries before a tile is damaged
  MaxTries      = 5;
  // Tile dimensions
  TileWidth     = 256;
  TileHeight    = 256;

implementation

{ TTilePrinter }
// Copy the details from a tile map
constructor TTilePrinter.Create(Tiles:TTileMap);
begin
  FUrl:=Tiles.FUrl;

  // Domains
  FMinDomain:=Tiles.FMinDomain;
  FMaxDomain:=Tiles.FMaxDomain;

  // Zooms
  FMinZoom:=Tiles.FMinZoom;
  FMaxZoom:=Tiles.FMaxZoom;

  // Current level
  FLevel:=0;

  Init;
end;

// Constructor with details
constructor TTilePrinter.Create(Url:String; MinZoom, MaxZoom, MinDomain, MaxDomain:Integer);
begin
  FUrl:=Url;

  // Domains
  FMinDomain:=Min(MinDomain, MaxDomain);
  FMaxDomain:=Max(MinDomain, MaxDomain);

  // Zooms
  FMinZoom:=MinZoom;
  FMaxZoom:=MaxZoom;

  // Current level
  FLevel:=0;

  Init;
end;

// Destructor
destructor TTilePrinter.Destroy;
var i      :Integer;
begin
  WebAPI.FilterOutKeys('printer');

  // Destroy the cache
  for i:=Low(FCache) to High(FCache) do
    if Assigned(FCache[i]) then
      FreeAndNil(FCache[i]);

  FreeAndNil(FBuffer);

  // Inherited destroy
  inherited Destroy;
end;

// Calculate the index offset of a zoom level
function TTilePrinter.offset(z:Integer):Integer;
begin
  Result:=(1 shl (2 * z)) div 3;
end;

// Calculate the coordinate x, y and z into the index of the cache
function TTilePrinter.indexOf(x, y, z:Integer):Integer;
begin
  Result:=offset(z) - offset(FMinZoom) + (1 shl z) * y + x;
end;

// Returns the rectangle of a specific tile
function TTilePrinter.tileRect(x, y, Zoom:Integer):TRect;
var f    :Integer;
begin
  if Zoom < FLevel then
    f:=1 shl (FLevel - Zoom)
  else
    f:=1;

  // Return the rectangle
  Result:=Bounds(x * TileWidth * f, y * TileHeight * f, TileWidth * f, TileHeight * f);
end;

// Create the cache directory
procedure TTilePrinter.setCache(Value:String);
begin
  if DirectoryExists(Value) or CreateDir(Value) then
    FCacheDir:=Value;
end;

// Initialize the data
procedure TTilePrinter.Init;
var i     :Integer;
begin
  // Default events
  FOnEvent:=Nil;
  FOnPost:=Nil;
  FOnPre:=Nil;

  // Critical section
  FLock:=TCriticalSection.Create;

  // Buffer picture
  FBuffer:=TPortableNetworkGraphic.Create;

  // Setup the cache
  SetLength(FCache, offset(FMaxZoom + 1) - offset(FMinZoom));
  SetLength(FState, offset(FMaxZoom + 1) - offset(FMinZoom));
  for i:=Low(FCache) to High(FCache) do begin
    FCache[i]:=Nil;
    FState[i]:=psNone;
  end;
end;

// Set up the font
procedure TTilePrinter.SetupFont(Font:TFont);
begin
  FBuffer.Canvas.Font.Assign(Font);
end;

// Fill the buffer with a color
procedure TTilePrinter.Fill(Color:TColor; Style:TBrushStyle);
begin
  with FBuffer do begin
    Canvas.Brush.Color:=Color;
    Canvas.Brush.Style:=Style;
    Canvas.Pen.Style:=psClear;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
end;

// Draw a text
procedure TTilePrinter.TextOut(const s:String; x, y:Integer);
var size  :TSize;
    f     :Integer;
begin
  // Calculate the zoom
  f:=1 shl (FMaxZoom - FLevel);
  // Calculate the text size
  size:=FBuffer.Canvas.TextExtent(s);

  // Setup the canvas
  FBuffer.Canvas.Brush.Style:=bsClear;

  // Adjust the position
  x:=x div f - size.cx div 2 - FArea.Left;
  y:=y div f - size.cy div 2 - FArea.Top;

  // Draw the text
  FBuffer.Canvas.TextOut(x, y, s);
end;

// Draw a text with an outline
procedure TTilePrinter.TextOut(const s:String; x, y:Integer; Outer, Inner:TColor);
var size  :TSize;
    f     :Integer;
    p     :TPoint;
    i, j  :Integer;
begin
  // Calculate the zoom
  f:=1 shl (FMaxZoom - FLevel);
  // Create the point
  p:=Point(x div f, y div f);

  // Calculate the text size
  size:=FBuffer.Canvas.TextExtent(s);
  Dec(p.x, size.cx div 2 + FArea.Left);
  Dec(p.y, size.cy div 2 + FArea.Top);

  // Setup the canvas
  FBuffer.Canvas.Brush.Style:=bsClear;
  FBuffer.Canvas.Font.Color:=Outer;

  // Draw the border
  for i:=-1 to 1 do
    for j:=-1 to 1 do begin
      if (i = 0) and (j = 0) then
        Continue;

      FBuffer.Canvas.TextOut(p.x + i, p.y + j, s);
    end;

  // Draw the text
  FBuffer.Canvas.Font.Color:=Inner;
  FBuffer.Canvas.TextOut(p.x, p.y, s);
end;

// Draw a graphic
procedure TTilePrinter.Draw(x, y:Integer; const Pic:TGraphic; ScaleIcon:Integer);
var f     :Integer;
    w, h  :Integer;
begin
  // Calulate the position
  f:=1 shl (FMaxZoom - FLevel);

  // Get the image size
  w:=Pic.Width;
  h:=Pic.Height;
  // Apply the scaling
  if ScaleIcon > 1 then begin
    w:=w div ScaleIcon;
    h:=h div ScaleIcon;
  end;

  // Adjust with the area rectangle
  x:=x div f - w div 2 - FArea.Left;
  y:=y div f - h div 2 - FArea.Top;
  // Draw the picture
  FBuffer.Canvas.StretchDraw(Bounds(x, y, w, h), Pic);
end;

// Save the buffer
procedure TTilePrinter.Save(const AFileName:String);
begin
  // Lock the access on the buffer
  FLock.Acquire;
  try
    FBuffer.SaveToFile(AFileName);
  finally
    // Release the access
    FLock.Release;
  end;
end;

// Callback procedure if a tile is loaded
procedure TTilePrinter.DoLoadedTile(Tile:TTile);
var r     :TRect;
    idx   :Integer;
begin
  // Generate the index
  idx:=indexOf(Tile.X, tile.Y, Tile.Zoom);

  // Check the index
  if (idx < 0) or (Length(FCache) <= idx) then
    Exit;

  if Tile.State = tsCached then begin
    // Cache the tile if enabled
    if FCacheEnable and (FCacheDir <> '') then
      Tile.Picture.SaveToFile(Format('%s/%d-%d-%d.jpg', [FCacheDir, Tile.Zoom, Tile.X, Tile.Y]));

    // Get the rectangle of a tile
    r:=tileRect(Tile.X, Tile.Y, Tile.Zoom);

    // Adjust the rectangle
    Dec(r.Left, FArea.Left);
    Dec(r.Right, FArea.Left);
    Dec(r.Top, FArea.Top);
    Dec(r.Bottom, FArea.Top);

    // Lock the cache access
    FLock.Acquire;
    try
      FBuffer.Canvas.StretchDraw(r, Tile.Picture);

      FState[idx]:=psPrinted;
      FreeAndNil(FCache[idx]);
      Inc(FLoaded);
    finally
      // Release the lock
      FLock.Release;
    end;
  end else if Tile.State = tsError then begin
    // Lock the cache access
    FLock.Acquire;
    try
      FState[idx]:=psError;
      FreeAndNil(FCache[idx]);
      Inc(FErrors);
    finally
      // Release the lock
      FLock.Release;
    end;
  end;

  // Call the event of a loading tile
  if Assigned(FOnEvent) then
    FOnEvent(Self);

  // Start the post processing
  if (FLoaded + FErrors = FCount) and Assigned(FOnPost) then
    FOnPost(Self);
end;

// Load all tiles within the rectangle
procedure TTilePrinter.LoadLevel(Level:Integer; Area:TRect);
var x, y  :Integer;
    k     :String;
    url   :String;
begin
  if (Level < FMinZoom) or (FMaxZoom < Level) then
    Exit;

  // Initialize the data
  FLevel:=Level;
  FLoaded:=0;
  FErrors:=0;

  // Define the area
  FArea.Left:=Max(0, Min(Area.Left, Area.Right));
  FArea.Top:=Max(0, Min(Area.Top, Area.Bottom));
  FArea.Right:=Min(TileWidth shl Level, Max(Area.Left, Area.Right));
  FArea.Bottom:=Min(TileHeight shl Level, Max(Area.Top, Area.Bottom));

  // Initialize the buffer size
  FBuffer.SetSize(FArea.Right - FArea.Left, FArea.Bottom - FArea.Top);

  // Do the pre processing step
  if Assigned(FOnPre) then
    FOnPre(Self);

  // Start loading the tiles
  FCount:=0;
  x:=FArea.Left - FArea.Left mod TileWidth;
  while x < FArea.Right do begin
    y:=FArea.Top - FArea.Top mod TileHeight;
    while y < FArea.Right do begin
      if FMinDomain <> FMaxDomain then
        k:=IntToStr(RandomRange(FMinDomain, FMaxDomain + 1))
      else
        k:='';

      // Set up the url
      url:=StringReplace(FUrl, '{s}', k, [rfReplaceAll, rfIgnoreCase]);

      // Load the tile
      LoadTile(x div TileWidth, y div TileHeight, Level, url);
      Inc(FCount);

      Inc(y, TileHeight);
    end;

    Inc(x, TileWidth);
  end;
end;

// Load a tile from an url
procedure TTilePrinter.LoadTile(x, y, Zoom:Integer; Url:String);
var idx   :Integer;
begin
  // Generate the index
  idx:=indexOf(x, y, Zoom);

  // Check the index
  if (idx < 0) or (Length(FCache) <= idx) then
    Exit;

  // Lock the cache
  FLock.Acquire;
  try
    // Download the tile
    if FCache[idx] = Nil then begin
      FCache[idx]:=TTile.Create(x, y, Zoom, url, 'printer', @DoLoadedTile);
      FState[idx]:=psLoading;
    end;
  finally
    // Release the lock
    FLock.Release;
  end;
end;

{ TTileMap }
constructor TTileMap.Create(Url:String; MinZoom, MaxZoom, MinDomain, MaxDomain:Integer);
var i       :Integer;
begin
  // Events
  FOnMove:=Nil;
  FOnZoom:=Nil;
  FOnLoaded:=Nil;
  FOnError:=Nil;

  // Initialize the mouse
  FMouse.Dragging:=False;
  FMouse.X:=0;
  FMouse.Y:=0;

  // Minimal and maximal domain number to replace {s} in the url
  FMinDomain:=Min(MinDomain, MaxDomain);
  FMaxDomain:=Max(MinDomain, MaxDomain);

  // The formatting url
  //  {s} .. Subdomain
  //  {z} .. Current zoom
  //  {x} .. Current x tile
  //  {y} .. Current y tile
  FUrl:=Url;

  // Minimal and maximal zoom level
  FMinZoom:=MinZoom;
  FMaxZoom:=MaxZoom;
  FZoom:=0;

  // Area of the current zoom level
  FArea:=Rect(0, 0, TileWidth shl FZoom, TileHeight shl FZoom);
  // Visible area
  FView:=Rect(0, 0, Width, Height);

  // Create the cache and fill with Nil
  SetLength(FCache, offset(FMaxZoom + 1) - offset(FMinZoom));
  for i:=Low(FCache) to High(FCache) do
    FCache[i]:=Nil;

  // The bitmap buffer
  FBack:=TBitmap.Create;
  // Set the size
  FBack.SetSize(Width, Height);
  // Not drawn
  FBackLoaded:=False;

  // Call the inherited constructor
  inherited Create(Nil);
end;

// Destructor
destructor TTileMap.Destroy;
var i      :Integer;
begin
  // Destroy the background image
  FreeAndNil(FBack);

  // Destroy the cache
  for i:=Low(FCache) to High(FCache) do
    if Assigned(FCache[i]) then
      FreeAndNil(FCache[i]);

  // Inherited destroy
  inherited Destroy;
end;

// Check if a rectangle is viewable
function TTileMap.viewable(const r:TRect):Boolean;
var k    :Integer;
begin
  k:=1 shl (FMaxZoom - FZoom);

  Result:=True;
  if (r.Right div k < FView.Left) or (FView.Right < r.Left div k) then
    Result:=False;

  if (r.Bottom div k < FView.Top) or (FView.Bottom < r.Top div k) then
    Result:=False;
end;

// Conversion methods
function TTileMap.CoordToView(x, y:Integer):TPoint;
var k   :Integer;
begin
  k:=1 shl (FMaxZoom - FZoom);
  Result.x:=Round(x / k - FView.Left);
  Result.y:=Round(y / k - FView.Top);
end;

// Calculate the mouse position in world space
function TTileMap.getMouseCoord:TPoint;
begin
  Result.X:=(FMouse.X + FView.Left - FArea.Left);
  Result.X:=(Result.X * (TileWidth shl FMaxZoom)) div (FArea.Right - FArea.Left);
  Result.Y:=FMouse.Y + FView.Top - FArea.Top;
  Result.Y:=(Result.Y * (TileHeight shl FMaxZoom)) div (FArea.Bottom - FArea.Top);
end;

// Convert the x coordinate in view space into the tile space
function TTileMap.viewToTileX(x:Integer; z:Integer):Integer;
var m    :Integer;
begin
  // Check the zoom level
  if (MinZoom <= z) and (z <= MaxZoom) then
    m:=TileWidth shl (FZoom - z)
  else begin
    m:=TileWidth;
    z:=FZoom;
  end;

  // Stay in the boundaries
  Result:=Max(0, Min((1 shl z) - 1, x div m));
end;

// Convert the y coordinate in view space into the tile space
function TTileMap.viewToTileY(y:Integer; z:Integer):Integer;
var m    :Integer;
begin
  // Check the zoom level
  if (MinZoom <= z) and (z <= MaxZoom) then
    m:=TileHeight shl (FZoom - z)
  else begin
    m:=TileHeight;
    z:=FZoom;
  end;

  // Stay in the boundaries
  Result:=Max(0, Min((1 shl z)  - 1, y div m));
end;

// Convert the x coordinate in the tile space into the view space
function TTileMap.tileToViewX(x:Integer; z:Integer):Integer;
begin
  // Check the zoom level
  if (MinZoom <= z) and (z <= MaxZoom) then
    z:=1 shl (FZoom - z)
  else
    z:=1;

  // Stay in the boundaries
  Result:=TileWidth * x * z;
end;

// Convert the y coordinate in the tile space into the view space
function TTileMap.tileToViewY(y:Integer; z:Integer):Integer;
begin
  // Check the zoom level
  if (MinZoom <= z) and (z <= MaxZoom) then
    z:=1 shl (FZoom - z)
  else
    z:=1;

  // Stay in the boundaries
  Result:=TileHeight * y * z;
end;

// Convert the visible area from the view space to the tile space
function TTileMap.tileRect:TRect;
begin
  Result.Left:=viewToTileX(FView.Left);
  Result.Top:=viewToTileY(FView.Top);
  Result.Right:=viewToTileX(FView.Right);
  Result.Bottom:=viewToTileY(FView.Bottom);
end;

// Calculate the index offset of a zoom level
function TTileMap.offset(z:Integer):Integer;
begin
  Result:=(1 shl (2 * z)) div 3;
end;

// Calculate the coordinate x, y and z into the index of the cache
function TTileMap.indexOf(x, y, z:Integer):Integer;
begin
  Result:=offset(z) - offset(FMinZoom) + (1 shl z) * y + x;
end;

// Handle the mouse wheel
function TTileMap.DoMouseWheel(Shift:TShiftState; WheelDelta:Integer; MousePos:TPoint):Boolean;
var pos  :TPoint;
    z    :Integer;
begin
  Result:=inherited DoMouseWheel(Shift, WheelDelta, MousePos);

  // Get the mouse position
  pos:=getMouseCoord;
  z:=FZoom;
  // Update the zoom level
  setZoom(FZoom + WheelDelta div 100);
  // Goto mouse position
  if z <> FZoom then
    move(tmpPosition, pos.x, pos.y);
end;

// Handle a mouse down
procedure TTileMap.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Inherited MouseDown(Button, Shift, X, Y);

  // Store mouse position
  FMouse.X:=X;
  FMouse.Y:=Y;

  // Update the mouse buttons
  case Button of
    mbLeft: FMouse.LeftButton:=True;
    mbRight: FMouse.RightButton:=True;
  end;
end;

// Handle a mouse move
procedure TTileMap.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);

  // Only move the visible area if dragging is active
  if FMouse.LeftButton and (not FMouse.RightButton) then begin
    moveViewRel(FMouse.X - X, FMouse.Y - Y);
    // Check the boundaries
    DoCheckView;

    // DoRepaint the view
    DoRepaint;

    if Assigned(FOnMove) then
      FOnMove(Self);
  end;

  // Update the position
  FMouse.X:=X;
  FMouse.Y:=Y;
end;

// Handle a mouse up
procedure TTileMap.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);

  // Update the mouse buttons
  case Button of
    mbLeft: FMouse.LeftButton:=False;
    mbRight: FMouse.RightButton:=False;
  end;
end;

// Move the view to the given location
procedure TTileMap.moveView(x, y:Integer);
begin
  FView.Left:=x;
  FView.Top:=y;
  FView.Right:=x + Width;
  FView.Bottom:=y + Height;
end;

// Move the view relativ
procedure TTileMap.moveViewRel(dx, dy:Integer);
begin
  Inc(FView.Left, dx);
  Inc(FView.Top, dy);
  Inc(FView.Right, dx);
  Inc(FView.Bottom, dy);
end;

// Move the view to specific positoins
procedure TTileMap.Move(Position:TTileMapPos; x, y:Integer);
var k     :Integer;
begin
  case Position of
    tmpTopLeft:
      moveView(FArea.Left, FArea.Top);
    tmpTopRight:
      moveView(FArea.Right - Width, FArea.Top);
    tmpBottomLeft:
      moveView(FArea.Left, FArea.Bottom - Height);
    tmpBottomRight:
      moveView(FArea.Right - Width, FArea.Bottom - Height);
    tmpLeft:
      moveView(FArea.Left, (FArea.Bottom + FArea.Top - Height) div 2);
    tmpTop:
      moveView((FArea.Left + FArea.Right - Width) div 2, FArea.Top);
    tmpRight:
      moveView(FArea.Right - Width, (FArea.Bottom + FArea.Top - Height) div 2);
    tmpBottom:
      moveView((FArea.Left + FArea.Right - Width) div 2, FArea.Bottom - Height);
    tmpCenter:
      moveView((FArea.Left + FArea.Right - Width) div 2, (FArea.Bottom + FArea.Top - Height) div 2);
    tmpPosition:begin
      k:=1 shl (FMaxZoom - FZoom);
      moveView(x div k - Width div 2, y div k - Height div 2);
    end;
  end;

  // Repaint
  DoRepaint;

  if Assigned(FOnMove) then
    FOnMove(Self);
end;

// Print a text
procedure TTileMap.TextOut(const s:String; x, y:Integer);
var p     :TPoint;
    size  :TSize;
begin
  p:=CoordToView(x, y);

  size:=Canvas.TextExtent(s);
  Canvas.TextOut(p.x - size.cx div 2, p.y - size.cy div 2, s);
end;

// Print a text with outline
procedure TTileMap.TextOut(const s:String; x, y:Integer; Outer, Inner:TColor);
var p     :TPoint;
    size  :TSize;
begin
  p:=CoordToView(x, y);

  size:=Canvas.TextExtent(s);

  Canvas.Font.Color:=Outer;
  Canvas.TextOut(p.x - size.cx div 2, p.y - size.cy div 2, s);
  Canvas.TextOut(p.x - size.cx div 2 + 1, p.y - size.cy div 2, s);
  Canvas.TextOut(p.x - size.cx div 2 + 1, p.y - size.cy div 2 + 1, s);
  Canvas.TextOut(p.x - size.cx div 2, p.y - size.cy div 2 + 1, s);

  Canvas.Font.Color:=Inner;
  Canvas.TextOut(p.x - size.cx div 2, p.y - size.cy div 2, s);
end;

// Check the boundaries of the view
procedure TTileMap.DoCheckView;
var dx, dy :Integer;
begin
  // Horizontal
  if FView.Left > FArea.Right - 128 then
    dx:=FArea.Right - 128 - FView.Left
  else if FView.Right < FArea.Left + 128 then
    dx:=FArea.Left + 128 - FView.Right
  else
    dx:=0;

  // Vertical
  if FView.Top > FArea.Bottom - 128 then
    dy:=FArea.Bottom - 128 - FView.Top
  else if FView.Bottom < FArea.Top + 128 then
    dy:=FArea.Top + 128 - FView.Bottom
  else
    dy:=0;

  moveViewRel(dx, dy);
end;

// Set the cache and create the directory if needed
procedure TTileMap.setCacheDir(Value:String);
begin
  if not DirectoryExists(Value) then
    CreateDir(Value);

  if LastDelimiter('\/', Value) <> Length(Value) then
    FCacheDir:=Value + '\'
  else
    FCacheDir:=Value;
end;

// Set a new zoom level
procedure TTileMap.setZoom(Value:Integer);
var k     :Integer;
    w, h  :Integer;
begin
  if FZoom <> Value then begin
    // Change the current zoom level
    Value:=Max(FMinZoom, Min(FMaxZoom, Value));

    // Update the area of the map
    FArea:=Rect(0, 0, TileWidth shl Value, TileHeight shl Value);

    // Get the width and the height of the view
    w:=Width div 2;
    h:=Height div 2;

    // Apply the zoom level change on the view area
    if FZoom > Value then begin
      // Zoom out
      k:=1 shl (FZoom - Value);
      moveView((FView.Left + w) div k - w, (FView.Top + h) div k - h);
    end else begin
      // Zoom in
      k:=1 shl (Value - FZoom);
      moveView((FView.Left+ w) * k - w, (FView.Top + h) * k - h);
    end;

    // Store the new value
    FZoom:=Value;

    // Check the view area
    DoCheckView;

    // Update the buffer
    DoRepaint;

    if Assigned(FOnZoom) then
      FOnZoom(Self);
  end;
end;

// Callback if a tile was loaded
procedure TTileMap.DoLoadedTile(Tile:TTile);
var r     :TRect;
    f     :String;
begin
  // Save the tile
  if FCacheEnable then begin
    f:=Format('%s%d-%d-%d.png', [FCacheDir, Tile.Zoom, Tile.X, Tile.Y]);
    Tile.FPicture.SaveToFile(f);
  end;

  // Callback
  if Assigned(FOnLoaded) and (Tile.State = tsCached) then
    FOnLoaded(Self, Tile.X, Tile.Y, Tile.Zoom);

  if Assigned(FOnError) and (Tile.State = tsError) then
    FOnError(Self, Tile.X, Tile.Y, Tile.Zoom);

  // Only redraw needed if zoom level is equal
  if Tile.Zoom <> FZoom then
    Exit;

  // Calculate the visible tile rect
  r:=tileRect;
  // Draw the tile if tile in visible area
  if (r.Left <= Tile.X) and (Tile.X <= r.Right) and(r.Top <= Tile.Y) and (Tile.Y <= r.Bottom) then begin
    // Only draw the new tile not the entire view
    DrawTile(Tile.X, Tile.Y, Tile.Zoom);

    Refresh;
  end;
end;

// Draw a tile on a canvas
procedure TTileMap.DrawTo(ACanvas:TCanvas; x, y, Zoom:Integer);
var idx   :Integer;
    r     :TRect;
begin
  idx:=indexOf(x, y, Zoom);

  if (idx < 0) or (Length(FCache) <= idx) or (FCache[idx] = Nil) then
    Exit;

  if FCache[idx].State <> tsCached then
    Exit;

  // Calculate the position of the tile
  r:=Bounds(x * TileWidth, y * TileHeight, TileWidth, TileHeight);

  // Draw the tile to the backbuffer
  ACanvas.StretchDraw(r, FCache[idx].Picture);
end;

// Draw a tile on another canvas
procedure TTileMap.DrawTile(ACanvas:TCanvas; x, y, Zoom:Integer);overload;
var idx   :Integer;
begin
  idx:=indexOf(x, y, Zoom);

  if (idx < 0) or (Length(FCache) <= idx) or (FCache[idx] = Nil) then
    Exit;

  if FCache[idx].State <> tsCached then
    Exit;

  // Draw the tile to the backbuffer
  ACanvas.Draw(0, 0, FCache[idx].Picture);
end;

// Draw a graphic
procedure TTileMap.Draw(x, y:Integer; const Pic:TGraphic; ScaleIcon:Integer);
var r     :TRect;
    p     :TPoint;
    k     :Integer;
    size  :TPoint;
begin
  k:=1 shl (FMaxZoom - FZoom);
  p:=Point(x div k, y div k);

  // Check if viewable
  if not PtInRect(FView, p) then
    Exit;

  // Apply the scaling
  if ScaleIcon > 1 then
    size:=Point(Pic.Width div ScaleIcon, Pic.Height div ScaleIcon)
  else
    size:=Point(Pic.Width, Pic.Height);

  Dec(p.x, FView.Left + size.x div 2);
  Dec(p.y, FView.Top + size.y div 2);

  r:=Bounds(p.x, p.y, size.x, size.y);

  Canvas.StretchDraw(r, Pic);
end;

// Draw a rectangle
procedure TTileMap.DrawRect(ACanvas:TCanvas; Zoom:Integer; ARect:TRect);
var x, y  :Integer;
    idx   :Integer;
begin
  x:=ARect.Left;
  while x < ARect.Right do begin
    y:=ARect.Top;
    while y < ARect.Bottom do begin
      idx:=indexOf(x div TileWidth, y div TileWidth, Zoom);

      if (0 <= idx) and (idx < Length(FCache)) and (FCache[idx] <> Nil) then
        ACanvas.Draw(x - ARect.Left, y - ARect.Top, FCache[idx].Picture);

      Inc(y, TileHeight);
    end;
    Inc(x, TileWidth);
  end;
end;

// Draw the tile given with x, y and z coordinates
procedure TTileMap.DrawTile(x, y, z:Integer);
var idx   :Integer;
    url   :String;
    dst   :TRect;
    src   :TRect;
    k     :String;
    buf   :TBitmap;
begin
  // Get the index of the cache
  idx:=indexOf(x, y, z);

  // Check the index and the array bounds
  if (idx < 0) or (Length(FCache) <= idx) then
    Exit;

  // Calculate the position of the tile
  dst.Left:=tileToViewX(x, z) - FView.Left;
  dst.Top:=tileToViewY(y, z) - FView.Top;
  dst.Right:=tileToViewX(x + 1, z) - FView.Left;
  dst.Bottom:=tileToViewY(y + 1, z) - FView.Top;

  // If the cache is nil
  if FCache[idx] = Nil then begin
    // Get a random server domain
    if FMinDomain <> FMaxDomain then
      k:=IntToStr(RandomRange(FMinDomain, FMaxDomain + 1))
    else
      k:='';

    // Set up the url
    url:=StringReplace(FUrl, '{s}', k, [rfReplaceAll, rfIgnoreCase]);

    // Create a new tile at the cache index
    FCache[idx]:=TTile.Create(x, y, z, url, 'map', @DoLoadedTile);
  // If the tile is cached successfully
  end else if FCache[idx].State = tsCached then begin
    // Draw the tile to the backbuffer
    FBack.Canvas.StretchDraw(dst, FCache[idx].Picture);

    // Drawn means done
    Exit;
  end;

  src:=Bounds(0, 0, TileWidth, TileHeight);
  while z > 0 do begin
    // Adjust the source rectangle
    if x mod 2 = 1 then
      Dec(src.Left, src.Right - src.Left)
    else
      Inc(src.Right, src.Right - src.Left);

    // Adjust the source rectangle
    if y mod 2 = 1 then
      Dec(src.Top, src.Bottom - src.Top)
    else
      Inc(src.Bottom, src.Bottom - src.Top);

    // Scale the index
    x:=x div 2;
    y:=y div 2;
    Dec(z);

    // Generate the index
    idx:=indexOf(x, y, z);

    // Check the index
    if (idx < 0) or (Length(FCache) <= idx) then
      Break;

    // Check the cache
    if (FCache[idx] = Nil) or (FCache[idx].State <> tsCached) then
      Continue;

    // Create a image buffer
    buf:=TBitmap.Create;
    buf.SetSize(TileWidth, TileHeight);
    buf.Canvas.StretchDraw(src, FCache[idx].Picture);

    // Copy the current scope
    FBack.Canvas.CopyRect(dst, buf.Canvas, Bounds(0, 0, TileWidth, TileHeight));

    // Free the buffer
    buf.Free;

    // Leave the loop
    Break;
  end;
end;

// Redraw the buffers
procedure TTileMap.DoRepaint;
var r     :TRect;
    x, y  :Integer;
begin
  // Fill the buffer black
  with FBack.Canvas do begin
    Brush.Color:=clBlack;
    Brush.Style:=bsSolid;
    Pen.Style:=psClear;

    // Fill with a black rectangle
    Rectangle(0, 0, FBack.Width, FBack.Height);
  end;

  // Get the needed tiles area
  r:=tileRect;

  // Draw the main tile
  //DrawTile(0, 0, 0);

  FBackLoaded:=True;

  // Draw the tiles
  for x:=r.Left to r.Right do
    for y:=r.Top to r.Bottom do
      DrawTile(x, y, FZoom);

  // Refresh
  Refresh;
end;

// Paint of the control
procedure TTileMap.Paint;
begin
  if not FBackLoaded then
    DoRepaint;

  // Draw the back buffer
  Canvas.Draw(0, 0, FBack);

  inherited Paint;
end;

// Resize of the control
procedure TTileMap.Resize;
begin
  inherited Resize;

  // Resize the visible area
  FView.Right:=FView.Left + Width - 1;
  FView.Bottom:=FView.Top + Height - 1;

  // Resize the backbuffer
  FBack.SetSize(Width, Height);

  // DoRepaint the background image
  DoRepaint;
end;

// Load an entire zoom level
procedure TTileMap.LoadZoom(Zoom:Integer);
var x, y  :Integer;
    idx   :Integer;
    count :Integer;
    url   :String;
    k     :String;
begin
  if (Zoom < FMinZoom) or (FMaxZoom < Zoom) then
    Exit;

  count:=1 shl Zoom;

  for x:=0 to count - 1 do
    for y:=0 to count - 1 do begin
      // Get the index of the cache
      idx:=indexOf(x, y, Zoom);

      // Check the index and the array bounds
      if (idx < 0) or (Length(FCache) <= idx) then
        Continue;

      // If the cache is nil
      if FCache[idx] = Nil then begin
        // Get a random server domain
        if FMinDomain <> FMaxDomain then
          k:=IntToStr(RandomRange(FMinDomain, FMaxDomain + 1))
        else
          k:='';

        // Set up the url
        url:=StringReplace(FUrl, '{s}', k, [rfReplaceAll, rfIgnoreCase]);

        // Create a new tile at the cache index
        FCache[idx]:=TTile.Create(x, y, Zoom, url, 'map', @DoLoadedTile);
      end;
    end;
end;

{ TTile }
constructor TTile.Create(x, y, z:Integer; url, key:String; Callback:TTileCallback);
begin
  // Callback
  FCallback:=Callback;

  // Coordinate of the tile
  FX:=x;
  FY:=y;
  FZ:=z;
  FKey:=Key;

  // Default state
  FState:=tsLoading;
  FPicture:=Nil;

  // Generate the url
  url:=StringReplace(url, '{x}', IntToStr(x), [rfIgnoreCase]);
  url:=StringReplace(url, '{y}', IntToStr(y), [rfIgnoreCase]);
  url:=StringReplace(url, '{z}', IntToStr(z), [rfIgnoreCase]);
  WebAPI.RequestUrl(url, @response, Key, 1);
end;

// Destructor
destructor TTile.Destroy;
begin
  // Free the picture
  if Assigned(FPicture) then
    FreeAndNil(FPicture);
  inherited Destroy;
end;

// Response handler
function TTile.response(Data, Key:String; Tries:Integer):Boolean;
var s    :TStringStream;
begin
  Result:=(Data <> '') or (Tries >= MaxTries);

  // Apply the maximal tries
  if Tries >= MaxTries then begin
    FState:=tsError;

    // Call the callback
    if Assigned(FCallback) then
      FCallback(Self);
  end;

  // Data avaiable
  if Data <> '' then begin
    s:=TStringStream.Create(Data);
    s.Seek(0, soFromBeginning);
    try
      FPicture:=TJPEGImage.Create;

      // Load the picture from a stream
      FPicture.LoadFromStream(s);
      FState:=tsCached;

      // Call the callback
      if Assigned(FCallback) then
        FCallback(Self);
    finally
      s.Free;
    end;
  end;
end;

end.

