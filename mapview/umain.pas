unit uMain;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, Menus, uTileMap, uMumble, uMap, uPrinter;

type
  { TViewer }
  TViewer = class(TForm)
    FocusMap: TButton;
    Maps: TComboBox;
    FontMap: TButton;
    FontRegion: TButton;
    FontSector: TButton;
    CheckHeart: TCheckBox;
    CheckHero: TCheckBox;
    CheckWay: TCheckBox;
    CheckVista: TCheckBox;
    CheckPoI: TCheckBox;
    Info: TGroupBox;
    Icons: TGroupBox;
    MainMenu: TMainMenu;
    CloseItem: TMenuItem;
    Infos: TMemo;
    ContItem: TMenuItem;
    MistsItem: TMenuItem;
    TyriaItem: TMenuItem;
    Sc1Item: TMenuItem;
    Sc2Item: TMenuItem;
    Sc4Item: TMenuItem;
    Sc8Item: TMenuItem;
    ScaleItem: TMenuItem;
    OptItem: TMenuItem;
    PrintItem: TMenuItem;
    GerItem: TMenuItem;
    FrItem: TMenuItem;
    SpItem: TMenuItem;
    EngItem: TMenuItem;
    ReloadMenu: TMenuItem;
    PHeart: TImage;
    PHeropoint: TImage;
    PPoI: TImage;
    PVista: TImage;
    PWaypoint: TImage;
    CheckMap: TCheckBox;
    CheckRegion: TCheckBox;
    CheckSector: TCheckBox;
    CBMap: TColorButton;
    CBRegion: TColorButton;
    CBSector: TColorButton;
    CRegion: TColorDialog;
    CMap: TColorDialog;
    CSector: TColorDialog;
    SectorF: TFontDialog;
    Labels: TGroupBox;
    MapF: TFontDialog;
    RegionF: TFontDialog;
    LeftBar: TPanel;
    Clock: TTimer;
    procedure Checked(Sender: TObject);
    procedure CloseClick(Sender: TObject);
    procedure ContinentClick(Sender: TObject);
    procedure FocusMapClick(Sender: TObject);
    procedure FontClick(Sender: TObject);
    procedure MapsSelect(Sender: TObject);
    procedure PrintClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ClockTimer(Sender: TObject);
    procedure ReloadClick(Sender: TObject);
    procedure ScaleItemClick(Sender: TObject);
  private
    // Tile map
    FTyria     :TTileMap;
    FMists     :TTileMap;
    // Tyria database
    FData      :TGw2Continent;
    // Mouse control
    FMouse     :record
      X, Y     :Integer;
      Drag     :Boolean;
    end;

    FTiles     :TTileMap;
    FScale     :Integer;

    // Load procedure
    procedure  LoadedData(Sender:TGw2Continent);
    procedure  LoadMap(const Map:TGw2Map);
    // Draw procedure
    procedure  DrawProc(Sender:TObject);
    // Draw procedure for informations
    procedure  DrawHeart(const Heart:TGw2Task);
    procedure  DrawHeropoint(const Heropoint:TGw2Heropoint);
    procedure  DrawMap(const Map:TGw2Map);
    procedure  DrawPoI(const PoI:TGw2PoI);
    procedure  DrawRegion(const Region:TGw2Region);
    procedure  DrawSector(const Sector:TGw2Sector);
  public
    { public declarations }
  end;

var Viewer: TViewer;

implementation

const
  Continent = 1;
  Floor     = 1;

{$R *.lfm}

// Post processing draw function of the tile map
procedure TViewer.DrawProc(Sender:TObject);
begin
  if not (Sender is TTileMap) then
    Exit;

  // Set up the scope
  FTiles:=Sender as TTileMap;

  // Text with no backgrounds
  FTiles.Canvas.Brush.Style:=bsClear;

  // Draw the regions if checked
  if (FTiles.Zoom >= 2) and CheckRegion.Checked then
    FData.iter(@DrawRegion);

  // Iterate over the maps
  FData.iter(@DrawMap);

  // Clear the scope
  FTiles:=Nil;
end;

// Draw a sector label of the database
procedure TViewer.DrawSector(const Sector:TGw2Sector);
begin
  if FTiles = Nil then
    Exit;

  // Setup the font of the label
  FTiles.Canvas.Font.Assign(SectorF.Font);
  // Draw the label
  FTiles.TextOut(Sector.Name, Round(Sector.X), Round(Sector.Y), clBlack, CSector.Color);
end;

// Draw a heart of the database
procedure TViewer.DrawHeart(const Heart:TGw2Task);
begin
  if FTiles = Nil then
    Exit;

  // Draw the heart
  FTiles.Draw(Round(Heart.X), Round(Heart.Y), PHeart.Picture.Graphic, FScale);
end;

// Draw a heropoint of the database
procedure TViewer.DrawHeropoint(const Heropoint:TGw2Heropoint);
begin
  if FTiles = Nil then
    Exit;

  // Draw the heropoint
  FTiles.Draw(Round(Heropoint.X), Round(Heropoint.Y), PHeropoint.Picture.Graphic, FScale);
end;

// Draw a point of interest, vista or waypoint
procedure TViewer.DrawPoI(const PoI:TGw2PoI);
begin
  if FTiles = Nil then
    Exit;

  // Draw points of interest
  if (PoI.PoIType = poiLandmark) and CheckPoI.Checked then
    FTiles.Draw(Round(PoI.X), Round(PoI.Y), PPoI.Picture.Graphic, FScale);

  // Draw vistas
  if (PoI.PoIType = poiVista) and CheckVista.Checked then
    FTiles.Draw(Round(PoI.X), Round(PoI.Y), PVista.Picture.Graphic, FScale);

  // Draw waypoints
  if (PoI.PoIType = poiWaypoint) and CheckWay.Checked then
    FTiles.Draw(Round(PoI.X), Round(PoI.Y), PWaypoint.Picture.Graphic, FScale);
end;

// Draw a map of the database
procedure TViewer.DrawMap(const Map:TGw2Map);
var x, y  :Integer;
begin
  // Filter maps with less than 2 waypoints with the assumption to filter all story maps
  // Filter not viewable maps out
  if (Map.count(poiWaypoint) < 2) or (not FTyria.viewable(Map.ContinentRect)) then
    Exit;

  if FTiles = Nil then
    Exit;

  // Hearts, Heropoints and points of interests are only viewable in a zoom of 5+
  if FTiles.Zoom >= 3 then begin
    // Iterate over the hearts
    if CheckHeart.Checked then
      FData.iter(@DrawHeart);

    // Iterate over the heropoints
    if CheckHero.Checked then
      FData.iter(@DrawHeropoint);

    // Iterate over the points of interest (points of interest, waypoints and vistas)
    FData.iter(@DrawPoI);
  end;

  // Sectors are only viewable in a zoom 4+
  if (FTiles.Zoom >= 4) and CheckSector.Checked then
    Map.iter(@DrawSector);

  // Print the label
  if (FTiles.Zoom > 1) and CheckMap.Checked then begin
    // Calculate the center of the map to print the label
    with Map.ContinentRect do begin
      x:=(Left + Right) div 2;
      y:=(Top + Bottom) div 2;
    end;

    // Setup the font and draw the label
    FTiles.Canvas.Font.Assign(MapF.Font);
    FTiles.TextOut(Map.Name, x, y, clBlack, CMap.Color);
  end;
end;

// Draw regions of the database
procedure TViewer.DrawRegion(const Region:TGw2Region);
begin
  if FTiles = Nil then
    Exit;

  // Setup the font and draw the label
  FTiles.Canvas.Font.Assign(RegionF.Font);
  FTiles.TextOut(Region.Name, Region.Coordinate.X, Region.Coordinate.Y, clBlack, CRegion.Color);
end;

// Form create
procedure TViewer.FormCreate(Sender: TObject);
begin
  // Setup default values
  FMouse.Drag:=False;
  FScale:=1;

  // Create the database
  FData:=TGw2Continent.Create(Continent, Floor, lGerman);
  FData.OnLoadedRegions:=@LoadedData;

  // Create the tile map
  FTyria:=TTileMap.Create(Format('https://tiles{s}.guildwars2.com/1/%d/{z}/{x}/{y}.jpg', [Floor]), 0, 7, 1, 4);
  // Configurate the tile map
  FTyria.Parent:=Viewer;
  FTyria.Align:=alClient;
  // Setup a cache directory
  //FTyria.Cache:='Cache';
  FTyria.EnableCache:=False; // True if caching should be enabled

  // Set the paint callback
  FTyria.OnPaint:=@DrawProc;

  // Create the mists
  FMists:=TTileMap.Create(Format('https://tiles{s}.guildwars2.com/2/%d/{z}/{x}/{y}.jpg', [Floor]), 0, 6, 1, 4);
  FMists.Parent:=Viewer;
  FMists.Align:=alClient;
  FMists.EnableCache:=False;
  FMists.OnPaint:=@DrawProc;
  FMists.Visible:=False;

  // Setup the default colors
  CBMap.ButtonColor:=CMap.Color;
  CBRegion.ButtonColor:=CRegion.Color;
  CBSector.ButtonColor:=CSector.Color;
end;

// Refresh the view if a checkbox is changed
procedure TViewer.Checked(Sender: TObject);
begin
  if FTyria.Visible then
    FTyria.Refresh;
  if FMists.Visible then
    FMists.Refresh;
end;

procedure TViewer.CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TViewer.ContinentClick(Sender: TObject);
begin
  if Sender = MistsItem then begin
    FMists.Visible:=True;
    FTyria.Visible:=False;
    FData.Reload(2, FData.Language);
  end else if Sender = TyriaItem then begin
    FMists.Visible:=False;
    FTyria.Visible:=True;
    FData.Reload(1, FData.Language);
  end;
end;

procedure TViewer.FocusMapClick(Sender: TObject);
var k     :Integer;
    x, y  :Integer;
begin
  k:=Maps.ItemIndex;
  if (k < 0) or (Maps.Items.Count <= k) then
    Exit;

  if not (Maps.Items.Objects[k] is TGw2Map) then
    Exit;

  with (Maps.Items.Objects[k] as TGw2Map).ContinentRect do begin
    x:=(Left + Right) div 2;
    y:=(Top + Bottom) div 2;
  end;

  if FTyria.Visible then
    FTyria.Move(tmpPosition, x, y);
  if FMists.Visible then
    FMists.Move(tmpPosition, x, y);
end;

// Click on the font buttons
procedure TViewer.FontClick(Sender: TObject);
begin
  // Execute the font dialogs
  if Sender = FontMap then
    MapF.Execute
  else if Sender = FontRegion then
    RegionF.Execute
  else if Sender = FontSector then
    SectorF.Execute;
end;

procedure TViewer.MapsSelect(Sender: TObject);
var k     :Integer;
    map   :TGw2Map;
begin
  k:=Maps.ItemIndex;
  if (k < 0) or (Maps.Items.Count <= k) then
    Exit;

  if not (Maps.Items.Objects[k] is TGw2Map) then
    Exit;

  Infos.Clear;
  map:=Maps.Items.Objects[k] as TGw2Map;

  Infos.Append(Format('X: %d - %d', [map.ContinentRect.Left, map.ContinentRect.Right]));
  Infos.Append(Format('Y: %d - %d', [map.ContinentRect.Top, map.ContinentRect.Bottom]));
  Infos.Append(Format('%d Hearts', [map.Tasks.Count]));
  Infos.Append(Format('%d Heropoints', [map.Skills.Count]));
  Infos.Append(Format('%d Sectors', [map.Sectors.Count]));
  Infos.Append(Format('%d Vistas', [map.count(poiVista)]));
  Infos.Append(Format('%d Waypoints', [map.count(poiWaypoint)]));
  Infos.Append(Format('%d Points of interest', [map.count(poiLandmark)]));
end;


// Print button
procedure TViewer.PrintClick(Sender: TObject);
begin
  // Create the print dialog
  with TPrinter.Create(Self) do
    try
      // Configurate the print dialog
      if FTyria.Visible then
        Settings(FTyria);
      if FMists.Visible then
        Settings(FMists);

      Database:=FData;
      // Label colors
      RegionColor:=CRegion.Color;
      MapColor:=CMap.Color;
      SectorColor:=CSector.Color;
      // Label fonts
      SectorFont:=SectorF.Font;
      MapFont:=MapF.Font;
      RegionFont:=RegionF.Font;
      // Label settings
      Map:=CheckMap.Checked;
      Region:=CheckRegion.Checked;
      Sector:=CheckSector.Checked;
      // Scale icon
      ScaleIcon:=FScale;
      // Icon settings
      if CheckHeart.Checked then
        Heart:=PHeart;
      if CheckHero.Checked then
        Heropoint:=PHeropoint;
      if CheckPoI.Checked then
        PoI:=PPoI;
      if CheckVista.Checked then
        Vista:=PVista;
      if CheckWay.Checked then
        Waypoint:=PWaypoint;

      // Set the bounds of the dialog
      Left:=Self.Left + (Self.Width - Width) div 2;
      Top:=Self.Top + (Self.Height - Height) div 2;

      // Show the dialog
      ShowModal;
    finally
      // Free the dialog
      Free;
    end;
end;

// Destructor
procedure TViewer.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FData);
  FreeAndNil(FTyria);
  FreeAndNil(FMists);
end;

// Timer to refresh
procedure TViewer.ClockTimer(Sender: TObject);
begin
  if FTyria.Visible then
    FTyria.Refresh;
  if FMists.Visible then
    FMists.Refresh;
end;

// Reload the database
procedure TViewer.ReloadClick(Sender: TObject);
begin
  if Sender = GerItem then
    FData.Reload(lGerman)
  else if Sender = EngItem then
    FData.Reload(lEnglish)
  else if Sender = SpItem then
    FData.Reload(lSpanish)
  else if Sender = FrItem then
    FData.Reload(lFrench);

  Refresh;
end;

procedure TViewer.ScaleItemClick(Sender: TObject);
begin
  if Sender = Sc1Item then
    FScale:=1
  else if Sender = Sc2Item then
    FScale:=2
  else if Sender = Sc4Item then
    FScale:=4
  else if Sender = Sc8Item then
    FScale:=8;

  Refresh;
end;

// Load procedure
procedure TViewer.LoadedData(Sender:TGw2Continent);
begin
  Maps.Items.BeginUpdate;
  Maps.Items.Clear;
  FData.iter(@LoadMap);
  Maps.Items.EndUpdate;
end;

// Add a map to the combo list box
procedure TViewer.LoadMap(const Map:TGw2Map);
begin
  if Map.count(poiWaypoint) < 2 then
    Exit;

  Maps.Items.AddObject(Map.Name, Map);
end;

end.

