program mapview;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, uMain;

{$R *.res}

begin
  Application.Title:='Map Viewer';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TViewer, Viewer);
  Application.Run;
end.

