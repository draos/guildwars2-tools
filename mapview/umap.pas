unit uMap;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, types, fpjson, fgl, uWebAPI, jsonparser, math;

type
  TCoordinate   = record x, y:Single;end;
  TID           = Integer;
  TIDList       = specialize TFPGList<TID>;

  { TGw2Heropoint }
  TGw2Heropoint     = class
  private
    FX          :Single;
    FY          :Single;
  public
    constructor Create(Root:TJSONObject);

    property    X:Single read FX;
    property    Y:Single read FY;
  end;
  TGw2SkillList = specialize TFPGList<TGw2Heropoint>;

  { TGw2Task }
  TGw2Task      = class(TGw2Heropoint)
  private
    FID         :TID;
    FObjective  :String;
    FChatLink   :String;
    FLevel      :Integer;
  public
    constructor Create(Root:TJSONObject);

    property    ID:TID read FID;
    property    Objective:String read FObjective;
    property    ChatLink:String read FChatLink;
    property    Level:Integer read FLevel;
  end;
  TGw2TaskList  = specialize TFPGMap<TID, TGw2Task>;

  { TGw2PoI }
  TGw2PoIType   = (poiLandmark, poiWaypoint, poiVista, poiNil);
  TGw2PoI       = class(TGw2Heropoint)
  private
    FID         :TID;
    FName       :String;
    FChatLink   :String;
    FType       :TGw2PoIType;
  public
    constructor Create(Root:TJSONObject);

    property    ID:TID read FID;
    property    Name:String read FName;
    property    ChatLink:String read FChatLink;
    property    PoIType:TGw2PoIType read FType;
  end;
  TGw2PoIList   = specialize TFPGMap<TID, TGw2PoI>;

  { TGw2Sector }
  TGw2Sector    = class(TGw2Heropoint)
  private
    FID         :TID;
    FName       :String;
    FLevel      :Integer;
    FChatLink   :String;
  public
    constructor Create(Root:TJSONObject);

    property    ID:TID read FID;
    property    Name:String read FName;
    property    Level:Integer read FLevel;
    property    ChatLink:String read FChatLink;
  end;
  TGw2SectorList= specialize TFPGMap<TID, TGw2Sector>;

  // Iteration functions
  TGw2SkillIter = procedure(const Skill:TGw2Heropoint) of object;
  TGw2TaskIter  = procedure(const Task:TGw2Task) of object;
  TGw2PoIIter   = procedure(const PoI:TGw2PoI) of object;
  TGw2SectorIter= procedure(const Sector:TGw2Sector) of object;

  { TGw2Map }
  TGw2Map       = class
  private
    FID         :TID;
    FName       :String;
    FMinLvl     :Integer;
    FMaxLvl     :Integer;
    FMapRect    :TRect;
    FContRect   :TRect;
    FPoIs       :TGw2PoIList;
    FTasks      :TGw2TaskList;
    FSkills     :TGw2SkillList;
    FSectors    :TGw2SectorList;

    procedure   loadPoIs(Root:TJSONObject);
    procedure   loadTasks(Root:TJSONObject);
    procedure   loadSkills(Root:TJSONArray);
    procedure   loadSectors(Root:TJSONObject);
  public
    constructor Create(Root:TJSONObject);
    destructor  Destroy;override;

    function    count(PoIType:TGw2PoIType):Integer;

    procedure   iter(Func:TGw2PoIIter);overload;
    procedure   iter(Func:TGw2TaskIter);overload;
    procedure   iter(Func:TGw2SkillIter);overload;
    procedure   iter(Func:TGw2SectorIter);overload;

    property    ID:TID read FID;
    property    Name:String read FName;
    property    MinLevel:Integer read FMinLvl;
    property    MaxLevel:Integer read FMaxLvl;
    property    MapRect:TRect read FMapRect;
    property    ContinentRect:TRect read FContRect;
    property    PoIs:TGw2PoIList read FPoIs;
    property    Tasks:TGw2TaskList read FTasks;
    property    Skills:TGw2SkillList read FSkills;
    property    Sectors:TGw2SectorList read FSectors;
  end;
  TGw2MapList   = specialize TFPGMap<TID, TGw2Map>;

  // Iteration functions
  TGw2MapIter   = procedure(const Map:TGw2Map) of object;

  { TGw2Region }
  TGw2Region    = class
  private
    FName       :String;
    FLabel      :TPoint;
    FMaps       :TGw2MapList;
  public
    constructor Create(Root:TJSONObject);
    destructor  Destroy;override;

    procedure   iter(Func:TGw2PoIIter);overload;
    procedure   iter(Func:TGw2TaskIter);overload;
    procedure   iter(Func:TGw2SkillIter);overload;
    procedure   iter(Func:TGw2SectorIter);overload;
    procedure   iter(Func:TGw2MapIter);overload;

    property    Name:String read FName;
    property    Coordinate:TPoint read FLabel;
    property    Maps:TGw2MapList read FMaps;
  end;
  TGw2RegionList= specialize TFPGObjectList<TGw2Region>;

  // Iteration functions
  TGw2RegionIter= procedure(const Region:TGw2Region) of object;

  { TGw2Continent }
  TGw2Continent = class;
  TGw2Callback  = procedure(Sender:TGw2Continent) of object;

  TLanguage     = (lEnglish, lGerman, lSpanish, lFrench);

  TGw2Continent = class
  private
    FID         :TID;
    FName       :String;
    FLang       :TLanguage;
    FMinZoom    :Integer;
    FMaxZoom    :Integer;
    FDimension  :TPoint;
    FFloor      :TID;
    FFloors     :TIDList;
    FRegions    :TGw2RegionList;

    FOnLC       :TGw2Callback;
    FOnLR       :TGw2Callback;

    function    handleContinents(Data, Key:String; Tries:Integer):Boolean;
    function    handleFloor(Data, Key:String; Tries:Integer):Boolean;

    procedure   loadData(Root:TJSONObject);
  public
    constructor Create(Continent, Floor:TID; Lang:TLanguage);
    destructor  Destroy;override;

    procedure   iter(Func:TGw2PoIIter);overload;
    procedure   iter(Func:TGw2TaskIter);overload;
    procedure   iter(Func:TGw2SkillIter);overload;
    procedure   iter(Func:TGw2SectorIter);overload;
    procedure   iter(Func:TGw2MapIter);overload;
    procedure   iter(Func:TGw2RegionIter);overload;

    procedure   Reload(Lang:TLanguage);overload;
    procedure   Reload(Continent:TID; Lang:TLanguage);overload;
    procedure   Clear;

    property    Dimension:TPoint read FDimension;
    property    Floors:TIDList read FFloors;
    property    ID:TID read FID;
    property    MaxZoom:Integer read FMaxZoom;
    property    MinZoom:Integer read FMinZoom;
    property    Name:String read FName;
    property    Regions:TGw2RegionList read FRegions;
    property    Language:TLanguage read FLang;

    property    OnLoadedData:TGw2Callback write FOnLC;
    property    OnLoadedRegions:TGw2Callback write FOnLR;
  end;
  TGw2ContinentList = specialize TFPGMap<TID, TGw2Continent>;


const Meter2Inch     = 39.3700787;

implementation

function parseJSON(Data:String; out Root:TJSONData):Boolean;
begin
  Result:=False;
  with TJSONParser.Create(Data) do
    try
      Root:=Parse;
      Result:=True;
    finally
      Free;
    end;
end;

function strToPoiType(x:String):TGw2PoIType;
begin
  case x of
    'landmark': Result:=poiLandmark;
    'waypoint': Result:=poiWaypoint;
    'vista': Result:=poiVista;
  else
    Result:=poiNil;
  end;
end;

function CompareID(const a, b:TID):Integer;
begin
  Result:=CompareValue(a, b);
end;

function readPoint(const Arr:TJSONArray):TPoint;
begin
  Result:=Point(0, 0);

  if (Arr = Nil) or (Arr.Count < 2) then
    Exit;

  if Arr.Types[0] = jtNumber then
    Result.x:=Arr.Integers[0];

  if Arr.Types[1] = jtNumber then
    Result.y:=Arr.Integers[1];
end;

function readLabel(const Root:TJSONObject; const ID:String):TPoint;
begin
  Result:=readPoint(Root.Find(ID, jtArray) as TJSONArray);
end;

function readRect(const Root:TJSONObject; const ID:String):TRect;
var arr  :TJSONArray;
begin
  Result:=Bounds(0, 0, 0, 0);

  arr:=Root.Find(ID, jtArray) as TJSONArray;

  if (arr = Nil) or (arr.Count < 2) then
    Exit;

  if arr.Types[0] = jtArray then
    Result.TopLeft:=readPoint(arr.Arrays[0]);

  if arr.Types[1] = jtArray then
    Result.BottomRight:=readPoint(arr.Arrays[1]);
end;

{ TGw2Region }
constructor TGw2Region.Create(Root:TJSONObject);
var i       :Integer;
    obj     :TJSONObject;
    k       :String;
    map     :TGw2Map;
    j       :Integer;
begin
  FName:=Root.Get('name', '');
  FLabel:=readLabel(Root, 'label_coord');

  FMaps:=TGw2MapList.Create;

  obj:=Root.Find('maps', jtObject) as TJSONObject;
  if obj = Nil then
    Exit;

  for i:=0 to obj.Count - 1 do begin
    k:=obj.Names[i];
    if obj.Types[k] <> jtObject then
      Continue;

    map:=TGw2Map.Create(obj.Objects[k]);
    if (map.ID = 0) or FMaps.Find(map.ID, j) then
      map.Free
    else
      FMaps.Add(map.ID, map);
  end;
end;

destructor TGw2Region.Destroy;
var i      :Integer;
begin
  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].Free;

  FreeAndNil(FMaps);
  inherited Destroy;
end;

procedure TGw2Region.iter(Func:TGw2MapIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FMaps.Count - 1 do
    Func(FMaps.Data[i]);
end;

procedure TGw2Region.iter(Func:TGw2PoIIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].iter(Func);
end;

procedure TGw2Region.iter(Func:TGw2TaskIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].iter(Func);
end;

procedure TGw2Region.iter(Func:TGw2SkillIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].iter(Func);
end;

procedure TGw2Region.iter(Func:TGw2SectorIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].iter(Func);
end;

{ TGw2Map }
constructor TGw2Map.Create(Root:TJSONObject);
begin
  FID:=Root.Get('id', 0);
  FName:=Root.Get('name', '');
  FMinLvl:=Root.Get('min_level', 0);
  FMaxLvl:=Root.Get('max_level', 0);
  FMapRect:=readRect(Root, 'map_rect');
  FContRect:=readRect(Root, 'continent_rect');

  FPoIs:=TGw2PoIList.Create;
  with FPoIs do begin
    Sorted:=True;
    OnKeyCompare:=@CompareID;
  end;

  FTasks:=TGw2TaskList.Create;
  with FTasks do begin
    Sorted:=True;
    OnKeyCompare:=@CompareID;
  end;

  FSectors:=TGw2SectorList.Create;
  with FSectors do begin
    Sorted:=True;
    OnKeyCompare:=@CompareID;
  end;

  FSkills:=TGw2SkillList.Create;

  loadSkills(Root.Find('skill_challenges', jtArray) as TJSONArray);
  loadPoIs(Root.Find('points_of_interest', jtObject) as TJSONObject);
  loadTasks(Root.Find('tasks', jtObject) as TJSONObject);
  loadSectors(Root.Find('sectors', jtObject) as TJSONObject);
end;

destructor TGw2Map.Destroy;
var i      :Integer;
begin
  for i:=0 to FPoIs.Count - 1 do
    FPoIs.Data[i].Free;
  for i:=0 to FTasks.Count - 1 do
    FTasks.Data[i].Free;
  for i:=0 to FSkills.Count - 1 do
    FSkills[i].Free;
  for i:=0 to FSectors.Count - 1 do
    FSectors.Data[i].Free;

  FreeAndNil(FPoIs);
  FreeAndNil(FTasks);
  FreeAndNil(FSkills);
  FreeAndNil(FSectors);

  inherited Destroy;
end;

function TGw2Map.count(PoIType:TGw2PoIType):Integer;
var i    :Integer;
begin
  Result:=0;

  for i:=0 to FPoIs.Count - 1 do
    if FPoIs.Data[i].PoIType = PoIType then
      Inc(Result);
end;

procedure TGw2Map.iter(Func:TGw2PoIIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FPoIs.Count - 1 do
    Func(FPoIs.Data[i]);
end;

procedure TGw2Map.iter(Func:TGw2TaskIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FTasks.Count - 1 do
    Func(FTasks.Data[i]);
end;

procedure TGw2Map.iter(Func:TGw2SkillIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FSkills.Count - 1 do
    Func(FSkills[i]);
end;

procedure TGw2Map.iter(Func:TGw2SectorIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FSectors.Count - 1 do
    Func(FSectors.Data[i]);
end;

procedure TGw2Map.loadPoIs(Root:TJSONObject);
var i     :Integer;
    obj   :TJSONObject;
    key   :String;
    poi   :TGw2PoI;
    k     :Integer;
begin
  if Root = Nil then
    Exit;

  for i:=0 to Root.Count - 1 do begin
    key:=Root.Names[i];
    if Root.Types[key] <> jtObject then
      Continue;

    obj:=Root.Objects[key];
    if obj = Nil then
      Continue;

    poi:=TGw2PoI.Create(obj);
    if (poi.ID = 0) or FPoIs.Find(poi.ID, k) then begin
      poi.Free;
      Continue;
    end else
      FPoIs.Add(poi.ID, poi);
  end;
end;

procedure TGw2Map.loadTasks(Root:TJSONObject);
var i     :Integer;
    obj   :TJSONObject;
    key   :String;
    task  :TGw2Task;
    k     :Integer;
begin
  if Root = Nil then
    Exit;

  for i:=0 to Root.Count - 1 do begin
    key:=Root.Names[i];
    if Root.Types[key] <> jtObject then
      Continue;

    obj:=Root.Objects[key];
    if obj = Nil then
      Continue;

    task:=TGw2Task.Create(obj);
    if (task.ID = 0) or FTasks.Find(task.ID, k) then begin
      task.Free;
      Continue;
    end else
      FTasks.Add(task.ID, task);
  end;
end;

procedure TGw2Map.loadSkills(Root:TJSONArray);
var i     :Integer;
begin
  if Root = Nil then
    Exit;

  for i:=0 to Root.Count - 1 do
    if Root.Types[i] = jtObject then
      FSkills.Add(TGw2Heropoint.Create(Root.Objects[i]));
end;

procedure TGw2Map.loadSectors(Root:TJSONObject);
var i     :Integer;
    obj   :TJSONObject;
    key   :String;
    sec   :TGw2Sector;
    k     :Integer;
begin
  if Root = Nil then
    Exit;

  for i:=0 to Root.Count - 1 do begin
    key:=Root.Names[i];
    if Root.Types[key] <> jtObject then
      Continue;

    obj:=Root.Objects[key];
    if obj = Nil then
      Continue;

    sec:=TGw2Sector.Create(obj);
    if (sec.ID = 0) or FSectors.Find(sec.ID, k) then begin
      sec.Free;
      Continue;
    end else
      FSectors.Add(sec.ID, sec);
  end;
end;


{ TGw2PoI }
constructor TGw2PoI.Create(Root:TJSONObject);
begin
  FID:=Root.Get('id', 0);
  FName:=Root.Get('name', '');
  FChatLink:=Root.Get('chat_link', '');
  FType:=strToPoiType(Root.Get('type', ''));

  inherited Create(Root);
end;

{ TGw2Sector }
constructor TGw2Sector.Create(Root:TJSONObject);
begin
  FID:=Root.Get('id', 0);
  FName:=Root.Get('name', '');
  FLevel:=Root.Get('level', 0);
  FChatLink:=Root.Get('chat_link', '');

  inherited Create(Root);
end;

{ TGw2Task }
constructor TGw2Task.Create(Root:TJSONObject);
begin
  FID:=Root.Get('id', 0);
  FObjective:=Root.Get('objective', '');
  FChatLink:=Root.Get('chat_link', '');
  FLevel:=Root.Get('level', 0);

  inherited Create(Root);
end;

{ TGw2Heropoint }
constructor TGw2Heropoint.Create(Root:TJSONObject);
var arr     :TJSONArray;
begin
  FX:=0;
  FY:=0;

  arr:=Root.Find('coord', jtArray) as TJSONArray;
  if (arr = Nil) or (arr.Count < 2) then
    Exit;

  if arr.Types[0] = jtNumber then
    FX:=arr.Floats[0];

  if arr.Types[1] = jtNumber then
    FY:=arr.Floats[1];
end;

{ TGw2Continent }
constructor TGw2Continent.Create(Continent, Floor:TID; Lang:TLanguage);
begin
  FID:=Continent;
  FFloor:=Floor;
  FRegions:=TGw2RegionList.Create;
  FFloors:=TIDList.Create;

  FOnLC:=Nil;
  FOnLR:=Nil;

  Reload(Lang);
end;

destructor TGw2Continent.Destroy;
begin
  FreeAndNil(FRegions);
  FreeAndNil(FFloors);

  inherited Destroy;
end;

function TGw2Continent.handleContinents(Data, Key:String; Tries:Integer):Boolean;
var root :TJSONData;
begin
  Result:=parseJSON(Data, root);
  if Result and (root <> Nil) and (root.JSONType = jtObject) then begin
    loadData(root as TJSONObject);

    if Assigned(FOnLC) then
      FOnLC(Self);
  end;
end;

function TGw2Continent.handleFloor(Data, Key:String; Tries:Integer):Boolean;
var root :TJSONData;
    arr  :TJSONArray;
    i    :Integer;
begin
  Result:=parseJSON(Data, root);
  if Result and (root <> Nil) and (root.JSONType = jtArray) then begin
    arr:=root as TJSONArray;

    for i:=0 to arr.Count - 1 do
      if arr[i].JSONType = jtObject then
        FRegions.Add(TGw2Region.Create(arr.Objects[i]));

    if Assigned(FOnLR) then
      FOnLR(Self);
  end;
end;

procedure TGw2Continent.iter(Func:TGw2RegionIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FRegions.Count - 1 do
    Func(FRegions[i]);
end;

procedure TGw2Continent.iter(Func:TGw2PoIIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FRegions.Count - 1 do
    FRegions[i].iter(Func);
end;

procedure TGw2Continent.iter(Func:TGw2TaskIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FRegions.Count - 1 do
    FRegions[i].iter(Func);
end;

procedure TGw2Continent.iter(Func:TGw2SkillIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FRegions.Count - 1 do
    FRegions[i].iter(Func);
end;

procedure TGw2Continent.iter(Func:TGw2SectorIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FRegions.Count - 1 do
    FRegions[i].iter(Func);
end;

procedure TGw2Continent.iter(Func:TGw2MapIter);
var i     :Integer;
begin
  if not Assigned(Func) then
    Exit;

  for i:=0 to FRegions.Count - 1 do
    FRegions[i].iter(Func);
end;

procedure TGw2Continent.Reload(Lang:TLanguage);
var l     :String;
const
  fmtC = '/v2/continents/%d?lang=%s';
  fmtR = '/v2/continents/%d/floors/%d/regions?ids=all&lang=%s';
begin
  Clear;

  FLang:=Lang;
  case Lang of
    lEnglish: l:='en';
    lGerman:  l:='de';
    lFrench:  l:='fr';
    lSpanish: l:='es';
  else
    l:='en';
  end;

  FOnLC:=Nil;
  FOnLR:=Nil;

  WebAPI.RequestJSON(Format(fmtC, [FID, l]), @handleContinents);
  WebAPI.RequestJSON(Format(fmtR, [FID, FFloor, l]), @handleFloor);
end;

procedure TGw2Continent.Reload(Continent:TID; Lang:TLanguage);
var l     :String;
const
  fmtC = '/v2/continents/%d?lang=%s';
  fmtR = '/v2/continents/%d/floors/%d/regions?ids=all&lang=%s';
begin
  Clear;
  FID:=Continent;

  case Lang of
    lEnglish: l:='en';
    lGerman:  l:='de';
    lFrench:  l:='fr';
    lSpanish: l:='es';
  else
    l:='en';
  end;

  FOnLC:=Nil;
  FOnLR:=Nil;

  WebAPI.RequestJSON(Format(fmtC, [FID, l]), @handleContinents);
  WebAPI.RequestJSON(Format(fmtR, [FID, FFloor, l]), @handleFloor);
end;

procedure TGw2Continent.Clear;
begin
  FRegions.Clear;
end;

procedure TGw2Continent.loadData(Root:TJSONObject);
var i       :Integer;
    arr     :TJSONArray;
begin
  if FID <> Root.Get('id', 0) then
    Exit;

  FFloors.Clear;

  FName:=Root.Get('name', '');
  FMinZoom:=Root.Get('min_zoom', 0);
  FMaxZoom:=Root.Get('max_zoom', 0);

  FDimension:=readLabel(Root, 'continent_dims');

  arr:=Root.Find('floors', jtArray) as TJSONArray;

  if arr = Nil then
    Exit;

  for i:=0 to arr.Count - 1 do
    if arr.Types[i] = jtNumber then
      FFloors.Add(arr.Integers[i]);
end;

end.

