unit uStream;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

type
  TBitmapHeader      = packed record
    bfType           :Word;
    bfSize           :DWord;
    bfReserved       :DWord;
    bfOffBits        :DWord;
  end;

  TBitmapInformation = packed record
    biSize           :DWord;
    biWidth          :LongInt;
    biHeight         :LongInt;
    biPlanes         :Word;
    biBitCount       :Word;
    biCompression    :DWord;
    biSizeImage      :DWord;
    biXPelsPerMeter  :LongInt;
    biYPelsPerMeter  :LongInt;
    biClrUsed        :DWord;
    biClrImportant   :DWord;
  end;

  TRGBA              = packed record
    r, g, b, a       :Byte;
  end;
  PRGBA              = ^TRGBA;

  TBitmapStream = class(TFileStream)
  private
    FWidth      :Integer;
    FHeight     :Integer;

    FHeader     :TBitmapHeader;
    FInfo       :TBitmapInformation;

    function    offset(x, y:Int64):Int64;
    procedure   InitHeader;
    procedure   InitInformation;
  public
    constructor Create(const AFileName:String);overload;
    constructor Create(const AFileName:String; AWidth, AHeight:Integer);overload;
    destructor  Destroy;override;

    procedure   WritePixel(x, y:Integer; RGB:TRGBA);
    procedure   WritePixels(x, y, Num:Integer; const Data:PRGBA);
    procedure   Copy(x, y:Integer; Data:TBitmap);

    procedure   FillPixels(x, y, Num:Integer; RGB:TRGBA);
    procedure   Reset;

    property    Width:Integer read FWidth;
    property    Height:Integer read FHeight;
  end;

implementation


constructor TBitmapStream.Create(const AFileName:String; AWidth, AHeight:Integer);
begin
  FWidth:=AWidth;
  FHeight:=AHeight;

  inherited Create(AFileName, fmCreate or fmOpenReadWrite);
  Reset;
end;

constructor TBitmapStream.Create(const AFileName:String);
begin
  inherited Create(AFileName, fmOpenReadWrite);

  Seek(0, soBeginning);
  Read(FHeader, sizeof(TBitmapHeader));
  Read(FInfo, sizeof(TBitmapInformation));

  FWidth:=LEtoN(FInfo.biWidth);
  FHeight:=LEtoN(FInfo.biHeight);
  if FHeight < 0 then
    FHeight:=-FHeight;
end;

destructor TBitmapStream.Destroy;
begin
  inherited Destroy;
end;

function TBitmapStream.offset(x, y:Int64):Int64;
begin
  Result:=sizeof(TBitmapHeader) + sizeof(TBitmapInformation);
  Inc(Result, sizeof(TRGBA) * ((FHeight - y + 1) * FWidth + x));
end;

procedure TBitmapStream.Reset;
begin
  InitHeader;
  InitInformation;

  Size:=sizeof(TBitmapHeader) + sizeof(TBitmapInformation) +
       sizeof(DWord) * Int64(FWidth) * Int64(FHeight);

  // Write the bitmap header
  Seek(0, soBeginning);
  Write(FHeader, sizeof(TBitmapHeader));
  // Write the bitmap informations
  Seek(sizeof(TBitmapHeader), soBeginning);
  Write(FInfo, sizeof(TBitmapInformation));
end;

procedure TBitmapStream.InitHeader;
begin
  with FHeader do begin
    bfType:=NToLE($4D42);
    bfSize:=NToLE(FWidth * FHeight);
    bfReserved:=NToLE(0);
    bfOffBits:=NToLE(sizeof(TBitmapHeader) + sizeof(TBitmapInformation));
  end;
end;

procedure TBitmapStream.InitInformation;
begin
  with FInfo do begin
    biSize:=NToLE(sizeof(TBitmapInformation));
    biWidth:=NToLE(FWidth);
    biHeight:=NToLE(FHeight);
    biPlanes:=NToLE(1);
    biBitCount:=NToLE(32);
    biCompression:=NToLE(0);
    biSizeImage:=NToLE(sizeof(DWord) * FWidth * FHeight);
    biXPelsPerMeter:=NToLE(0);
    biYPelsPerMeter:=NToLE(0);
    biClrUsed:=NToLE(0);
    biClrImportant:=NToLE(0);
  end;
end;

procedure TBitmapStream.WritePixel(x, y:Integer; RGB:TRGBA);
begin
  if (x < 0) or (FWidth <= x) or (y < 0) or (FHeight <= y) then
    Exit;

  Seek(offset(x, y), soBeginning);
  WriteBuffer(RGB, sizeof(TRGBA));
end;

procedure TBitmapStream.WritePixels(x, y, Num:Integer; const Data:PRGBA);
begin
  if (x < 0) or (FWidth <= x) or (y < 0) or (FHeight <= y) then
    Exit;

  Seek(offset(x, y), soBeginning);
  WriteBuffer(Data^, Num * sizeof(TRGBA));
end;

procedure TBitmapStream.FillPixels(x, y, Num:Integer; RGB:TRGBA);
var i     :Integer;
begin
  if (x < 0) or (FWidth <= x) or (y < 0) or (FHeight <= y) then
    Exit;

  Seek(offset(x, y), soBeginning);
  for i:=0 to Num - 1 do
    WriteBuffer(RGB, sizeof(TRGBA));
end;

procedure TBitmapStream.Copy(x, y:Integer; Data:TBitmap);
var i     :Integer;
begin
  if Data.PixelFormat <> pf32bit then
    Exit;

  for i:=0 to Data.Height - 1 do
    WritePixels(x, y + i, Data.Width, PRGBA(Data.RawImage.GetLineStart(i)));
end;

end.

