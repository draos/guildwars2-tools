unit uWebAPI;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, simpleinternet, CustomTimer, fgl;

type
  TRequestState    = (rsIdle, rsDownloading, rsDownloaded);

  // True => Terminate Thread
  // False=> Redo the request
  TRequestCallback = function(Data, Key:String;TryNum:Integer):Boolean of object;

  TRequested    = class
    Url         :String;
    Callback    :TRequestCallback;
    Delay       :Integer;
    Key         :String;

    constructor Create(Url_:String;Callback_:TRequestCallback;Delay_:Integer;Key_:String='');
  end;

  TRequestThread   = class(TThread)
  private
    FCallback   :TRequestCallback;
    FData       :String;
    FDelay      :Integer;
    FKey        :String;
    FState      :TRequestState;
    FUrl        :String;
    FRedo       :Boolean;
    FTry        :Integer;
  protected
    procedure   Execute;override;
  public
    constructor Create(Url:String;Callback:TRequestCallback;Delay:Integer;Key:String='');overload;
    constructor Create(Requested:TRequested);overload;

    function    Response:Boolean;
  end;

  TRequestQueue     = specialize TFPGObjectList<TRequested>;
  TRequestList      = specialize TFPGList<TRequestThread>;

  { TWebAPI }
  TWebAPI       = class
  private
    FLimit      :Integer;
    FRequests   :TRequestList;
    FQueue      :TRequestQueue;
    FKey        :String;
    FTimer      :TCustomTimer;

    function    getOpenRequests:Integer;

    procedure   setKey(Value:String);
  private
    procedure   TimerProc(Sender:TObject);
  public
    constructor Create(Limit:Integer=10);
    destructor  Destroy;override;

    // Endpoint .. API Endpoint with needed API version and arguments
    // Callback .. Will be called if data is downloaded
    // Auth     .. The access token will automatically added if true
    // Delay    .. Delay in seconds until the downloading starts
    procedure   RequestJSON(Endpoint:String;Callback:TRequestCallback;Key:String='';Auth:Boolean=False;Delay:Integer=0);

    procedure   RequestUrl(Url:String;Callback:TRequestCallback;Key:String='';Delay:Integer=0);

    procedure   FilterOutKeys(Key:String);

    property    AccessKey:String read FKey write setKey;
    property    Limit:Integer read FLimit write FLimit;
    property    OpenRequests:Integer read getOpenRequests;
  end;

// Web API
var WebAPI      :TWebAPI = Nil;

implementation

const
  SERVER = 'https://api.guildwars2.com';

function HTTPEncode(const s:String):String;
const
  allowed = ['A'..'Z', 'a'..'z', '0'..'9', '*', '@', '.', '-', '_', '=', '/', '?', '&'];
var pS, pD :PChar;
begin
  SetLength(Result, 3 * Length(s));

  pS:=PChar(s);
  pD:=PChar(Result);

  while pS^ <> #0 do begin
    if pS^ in allowed then
      pD^:=pS^
    else begin
      FormatBuf(pD^, 3, '%%%.2x', 6, [Ord(pS^)]);
      Inc(pD, 2);
    end;

    Inc(pS);
    inc(pD);
  end;

  SetLength(Result, pD - PChar(Result));
end;

constructor TWebAPI.Create(Limit:Integer);
begin
  FLimit:=Limit;

  FKey:='';

  FRequests:=TRequestList.Create;
  FQueue:=TRequestQueue.Create;

  FTimer:=TCustomTimer.Create(Nil);
  FTimer.Interval:=100;
  FTimer.Enabled:=True;
  FTimer.OnTimer:=@TimerProc;
end;

destructor TWebAPI.Destroy;
var i      :Integer;
begin
  FTimer.Enabled:=False;
  i:=0;
  while i < FRequests.Count do begin
    FRequests[i].Terminate;
    Inc(i);
  end;

  FreeAndNil(FRequests);
  FreeAndNil(FQueue);
  inherited Destroy;
end;

function TWebAPI.getOpenRequests:Integer;
begin
  Result:=FQueue.Count + FRequests.Count;
end;

procedure TWebAPI.setKey(Value:String);
var pS, pD :PChar;
begin
  SetLength(FKey, Length(Value));
  pD:=PChar(FKey);
  pS:=PChar(Value);

  while pS^ <> #0 do begin
    if pS^ in ['0'..'9', 'A'..'F', 'a'..'f', '-'] then begin
      pD^:=pS^;
      Inc(pD);
    end;

    Inc(pS);
  end;

  SetLength(FKey, pD - PChar(FKey));
end;

procedure TWebAPI.FilterOutKeys(Key:String);
var i     :Integer;
begin
  // Delete all queued requests with the key
  for i:=FQueue.Count - 1 downto 0 do
    if FQueue[i].Key = Key then
      FQueue.Delete(i);

  // Delete all current requests
  for i:=FRequests.Count - 1 downto 0 do
    if FRequests[i].FKey = Key then begin
      FRequests[i].Terminate;
      FRequests.Delete(i);
    end;
end;

procedure TWebAPI.TimerProc(Sender:TObject);
var i     :Integer;
begin
  i:=FRequests.Count;

  while 0 < i do begin
    Dec(i);

    if FRequests[i].Response then begin
      FRequests[i].Terminate;
      FRequests.Delete(i);
    end;
  end;

  while (FRequests.Count < FLimit) and (FQueue.Count > 0) do begin
    FRequests.Add(TRequestThread.Create(FQueue[0]));
    FQueue.Delete(0);
  end;
end;

procedure TWebAPI.RequestJSON(Endpoint:String;Callback:TRequestCallback;Key:String;Auth:Boolean;Delay:Integer);
var url   :String;
begin
  if (Callback <> Nil) and ((not Auth) or (FKey <> '')) then begin
    if (Pos('?', Endpoint) > 0) and Auth then
      url:=Format('%s&access_token=%s', [Endpoint, FKey])
    else if Auth then
      url:=Format('%s?access_token=%s', [Endpoint, FKey])
    else
      url:=Endpoint;

    if (url <> '') and (url[1] <> '/') then
      url:=SERVER + '/' + HTTPEncode(url)
    else
      url:=SERVER + HTTPEncode(url);

    if FRequests.Count >= FLimit then
      FQueue.Add(TRequested.Create(url, Callback, Delay, Key))
    else
      FRequests.Add(TRequestThread.Create(url, Callback, Delay, Key));
  end;
end;

procedure TWebAPI.RequestUrl(Url:String;Callback:TRequestCallback;Key:String;Delay:Integer);
begin
  if Callback <> Nil then begin
    if FRequests.Count >= FLimit then
      FQueue.Add(TRequested.Create(url, Callback, Delay, Key))
    else
      FRequests.Add(TRequestThread.Create(url, Callback, Delay, Key));
  end;
end;

{ TRequestThread }
constructor TRequestThread.Create(Url:String;Callback:TRequestCallback;Delay:Integer;Key:String);
begin
  inherited Create(False);
  FreeOnTerminate:=True;

  FCallback:=Callback;
  FData:='';
  FDelay:=Delay;
  FKey:=Key;
  FRedo:=False;
  FState:=rsIdle;
  FUrl:=Url;
end;

constructor TRequestThread.Create(Requested:TRequested);
begin
  inherited Create(False);
  FreeOnTerminate:=True;

  FCallback:=Requested.Callback;
  FData:='';
  FDelay:=Requested.Delay;
  FKey:=Requested.Key;
  FRedo:=False;
  FState:=rsIdle;
  FTry:=0;
  FUrl:=Requested.Url;
end;

procedure TRequestThread.Execute;
begin
  FTry:=0;

  repeat
    if FRedo then
      Sleep(1000);

    if FDelay > 0 then
      Sleep(FDelay * 1000);

    FRedo:=False;
    FState:=rsDownloading;
    FData:='';
    Inc(FTry);

    try
      FData:=retrieve(FUrl);
    except
    end;

    FState:=rsDownloaded;

    while (not Terminated) and (not FRedo) do
      Sleep(250);
  until Terminated;
end;

function TRequestThread.Response:Boolean;
begin
  Result:=FState = rsDownloaded;

  if Result and (FCallback <> Nil) then begin
    Result:=FCallback(FData, FKey, FTry);

    if not Result then
      FRedo:=True;
  end;
end;

{ TRequested }
constructor TRequested.Create(Url_:String;Callback_:TRequestCallback;Delay_:Integer;Key_:String);
begin
  Url:=Url_;
  Callback:=Callback_;
  Delay:=Delay_;
  Key:=Key_;
end;

initialization
  WebAPI:=TWebAPI.Create;

finalization
  FreeAndNil(WebAPI);

end.


