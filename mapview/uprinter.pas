unit uPrinter;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, Spin, ExtDlgs, uTileMap, uMap, ExtCtrls;

type
  { TPrinter }
  TPrinter = class(TForm)
    LabX: TLabel;
    LabSize: TLabel;
    LabY: TLabel;
    LSize: TLabel;
    Save: TButton;
    LabLevel: TLabel;
    Load: TButton;
    Progress: TProgressBar;
    LvlEdit: TSpinEdit;
    Diag: TSavePictureDialog;
    LftEdit: TSpinEdit;
    RghtEdit: TSpinEdit;
    TpEdit: TSpinEdit;
    BttmEdit: TSpinEdit;
    procedure RectChange(Sender: TObject);
    procedure LoadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LvlEditChange(Sender: TObject);
    procedure SaveClick(Sender: TObject);
  private
    // Reference to condinent
    FDb       :TGw2Continent;
    // Tile printer
    FTiles    :TTilePrinter;
    // Label font colors
    FRColor   :TColor;
    FMColor   :TColor;
    FSColor   :TColor;
    // Label fonts
    FFSector  :TFont;
    FFMap     :TFont;
    FFRegion  :TFont;
    // References to the images
    FHeart    :TImage;
    FHero     :TImage;
    FPoI      :TImage;
    FVista    :TImage;
    FWaypoint :TImage;
    // Labels
    FRegion   :Boolean;
    FMap      :Boolean;
    FSector   :Boolean;
    // Scaling
    FScIcon   :Integer;
    // Last level
    FLastLevel:Integer;
    // Procedures
    procedure Change(Sender:TTilePrinter);
    procedure PreProcess(Sender:TTilePrinter);
    procedure PostProcess(Sender:TTilePrinter);
    // Post processing for every item
    procedure PostHeart(const Heart:TGw2Task);
    procedure PostMap(const Map:TGw2Map);
    procedure PostPoI(const PoI:TGw2PoI);
    procedure PostSector(const Sector:TGw2Sector);
    procedure PostHeropoint(const Hero:TGw2Heropoint);
    procedure PostRegion(const Region:TGw2Region);
  public
    // Settings
    procedure Settings(Tiles:TTileMap);
    procedure Settings(Url:String; MinZoom, MaxZoom, MinDomain, MaxDomain:Integer);

    // Properties
    property  Database:TGw2Continent read FDb write FDb;
    // Label fonts
    property  MapFont:TFont read FFMap write FFMap;
    property  RegionFont:TFont read FFRegion write FFRegion;
    property  SectorFont:TFont read FFSector write FFSector;
    // Label colors
    property  MapColor:TColor read FMColor write FMColor;
    property  RegionColor:TColor read FRColor write FRColor;
    property  SectorColor:TColor read FSColor write FSColor;
    // Visible labels
    property  Map:Boolean read FMap write FMap;
    property  Region:Boolean read FRegion write FRegion;
    property  Sector:Boolean read FSector write FSector;
    property  ScaleIcon:Integer read FScIcon write FScIcon;
    // Visible icons and used images
    property  Heart:TImage read FHeart write FHeart;
    property  Heropoint:TImage read FHero write FHero;
    property  PoI:TImage read FPoI write FPoI;
    property  Vista:TImage read FVista write FVista;
    property  Waypoint:TImage read FWaypoint write FWaypoint;
  end;

implementation

{$R *.lfm}

{ TPrinter }
procedure TPrinter.FormCreate(Sender: TObject);
begin
  // Default values of the references
  FTiles:=Nil;
  FDb:=Nil;
  FFSector:=Nil;
  FFMap:=Nil;
  FFRegion:=Nil;
  FHeart:=Nil;
  FPoI:=Nil;
  FHero:=Nil;
  FVista:=Nil;
  FWaypoint:=Nil;

  FScIcon:=1;

  // Default level
  FLastLevel:=LvlEdit.Value;

  // Default colors
  FRColor:=$0081BAD6;
  FMColor:=$0081BAD6;
  FSColor:=clWhite;
end;

// Loading function to start the downloading of the tiles
procedure TPrinter.LoadClick(Sender: TObject);
var r     :TRect;
begin
  // Exit if not assigned
  if not Assigned(FTiles) then
    Exit;

  // Needed rectangle
  r:=Rect(LftEdit.Value, TpEdit.Value, RghtEdit.Value, BttmEdit.Value);
  // Start the loading of the rectangle
  FTiles.LoadLevel(LvlEdit.Value, r);
  // Setup the progress bar
  Progress.Min:=0;
  Progress.Max:=FTiles.CountTiles;
  Progress.Position:=0;

  // Disable the spin edits
  Load.Enabled:=False;
  LvlEdit.Enabled:=False;
  LftEdit.Enabled:=False;
  TpEdit.Enabled:=False;
  RghtEdit.Enabled:=False;
  BttmEdit.Enabled:=False;
end;

// Procedure if the rectangle is changed
procedure TPrinter.RectChange(Sender: TObject);
var w, h  :Integer;
begin
  // Adjust the bounds
  if Sender = LftEdit then
    RghtEdit.MinValue:=LftEdit.Value;
  if Sender = RghtEdit then
    LftEdit.MaxValue:=RghtEdit.Value;
  if Sender = TpEdit then
    BttmEdit.MinValue:=TpEdit.Value;
  if Sender = BttmEdit then
    TpEdit.MaxValue:=BttmEdit.Value;

  // Calculate the image size
  w:=RghtEdit.Value - LftEdit.Value;
  h:=BttmEdit.Value - TpEdit.Value;
  LSize.Caption:=Format('%d x %d Pixels', [w, h]);
end;

// Destructor
procedure TPrinter.FormDestroy(Sender: TObject);
begin
  if Assigned(FTiles) then
    FreeAndNil(FTiles);
end;

// Procedure if the level is changed
procedure TPrinter.LvlEditChange(Sender: TObject);
var k     :Integer;
begin
  k:=LvlEdit.Value - FLastLevel;

  if k > 0 then begin
    // Change the upper bounds
    RghtEdit.MaxValue:=TileWidth shl LvlEdit.Value;
    BttmEdit.MaxValue:=TileHeight shl LvlEdit.Value;

    // Scale the upper values
    RghtEdit.Value:=RghtEdit.Value shl k;
    BttmEdit.Value:=BttmEdit.Value shl k;

    // Adjust the lower bounds
    LftEdit.MaxValue:=RghtEdit.Value;
    TpEdit.MaxValue:=BttmEdit.Value;

    // Scale the lower bounds
    LftEdit.Value:=LftEdit.Value shl k;
    TpEdit.Value:=TpEdit.Value shl k;

    // Set the lower bounds
    RghtEdit.MinValue:=LftEdit.Value;
    BttmEdit.MinValue:=TpEdit.Value;
  end else if k < 0 then begin
    // Scale the lower bounds
    LftEdit.Value:=LftEdit.Value shr (-k);
    TpEdit.Value:=TpEdit.Value shr (-k);

    // Adjust the lower bounds
    RghtEdit.MinValue:=LftEdit.Value;
    BttmEdit.MinValue:=TpEdit.Value;

    // Scale the upper bounds
    RghtEdit.Value:=RghtEdit.Value shr (-k);
    BttmEdit.Value:=BttmEdit.Value shr (-k);

    // Adjust the upper bounds
    LftEdit.MaxValue:=RghtEdit.Value;
    TpEdit.MaxValue:=BttmEdit.Value;

    // Change the upper bounds
    RghtEdit.MaxValue:=TileWidth shl LvlEdit.Value;
    BttmEdit.MaxValue:=TileHeight shl LvlEdit.Value;
  end;

  FLastLevel:=LvlEdit.Value;

  RectChange(Self);
end;

// Save the image
procedure TPrinter.SaveClick(Sender: TObject);
begin
  // Close if printer is not loaded
  if not Assigned(FTiles) then begin
    Load.Enabled:=True;
    Save.Enabled:=False;
    Exit;
  end;

  // Execute save dialog
  if Diag.Execute then begin
    FTiles.Save(Diag.FileName);
    Close;
  end;
end;

// Set up the printer
procedure TPrinter.Settings(Url: String; MinZoom, MaxZoom, MinDomain, MaxDomain: Integer);
begin
  FTiles:=TTilePrinter.Create(Url, MinZoom, MaxZoom, MinDomain, MaxDomain);
  FTiles.OnChange:=@Change;
  FTiles.OnPreProcessing:=@PreProcess;
  FTiles.OnPostProcessing:=@PostProcess;

  // Setup the levels
  LvlEdit.MinValue:=MinZoom;
  LvlEdit.MaxValue:=MaxZoom;
  LvlEdit.Value:=MinZoom;
  LvlEditChange(Self);
end;

// Set up the printer and copy from tile map
procedure TPrinter.Settings(Tiles:TTileMap);
begin
  FTiles:=TTilePrinter.Create(Tiles);
  FTiles.OnChange:=@Change;
  FTiles.OnPreProcessing:=@PreProcess;
  FTiles.OnPostProcessing:=@PostProcess;

  // Setup the levels
  LvlEdit.MinValue:=Tiles.MinZoom;
  LvlEdit.MaxValue:=Tiles.MaxZoom;
  LvlEdit.Value:=Tiles.MinZoom;
  LvlEditChange(Self);
end;

// New tile
procedure TPrinter.Change(Sender:TTilePrinter);
begin
  // Setup progress
  Progress.Position:=Sender.DamagedTiles + Sender.PrintedTiles;
end;

// Pre processing method
procedure TPrinter.PreProcess(Sender:TTilePrinter);
begin
  // Fill with black
  Sender.Fill(clBlack);
end;

// Post processing method
procedure TPrinter.PostProcess(Sender:TTilePrinter);
begin
  // Process if db is assigned
  if Assigned(FDb) then
    FDb.iter(@PostRegion);

  // Enable the save button
  Save.Enabled:=True;
end;

// Post processing of heropoints
procedure TPrinter.PostHeropoint(const Hero:TGw2Heropoint);
begin
  FTiles.Draw(Round(Hero.X), Round(Hero.Y), FHero.Picture.Graphic, FScIcon);
end;

// Post processing of hearts
procedure TPrinter.PostHeart(const Heart:TGw2Task);
begin
  FTiles.Draw(Round(Heart.X), Round(Heart.Y), FHeart.Picture.Graphic, FScIcon);
end;

// Post processing of the points of interest
procedure TPrinter.PostPoI(const PoI:TGw2PoI);
begin
  // Points of interest
  if (PoI.PoIType = poiLandmark) and Assigned(FPoI) then
    FTiles.Draw(Round(PoI.X), Round(PoI.Y), FPoI.Picture.Graphic, FScIcon);

  // Waypoints
  if (PoI.PoIType = poiWaypoint) and Assigned(FWaypoint) then
    FTiles.Draw(Round(PoI.X), Round(PoI.Y), FWaypoint.Picture.Graphic, FScIcon);

  // Vistas
  if (PoI.PoIType = poiVista) and Assigned(FVista) then
    FTiles.Draw(Round(PoI.X), Round(PoI.Y), FVista.Picture.Graphic, FScIcon);
end;

// Post processing of the maps
procedure TPrinter.PostMap(const Map:TGw2Map);
var x, y  :Integer;
begin
  // Filter maps with less than 2 waypoints
  if Map.count(poiWaypoint) < 2 then
    Exit;

  // Process heropoints
  if Assigned(FHero) then
    Map.iter(@PostHeropoint);

  // Process hearts
  if Assigned(FHeart) then
    Map.iter(@PostHeart);

  // Process points of interest
  Map.iter(@PostPoI);

  // Process sector labels
  if FSector then
    Map.iter(@PostSector);

  // Process the map
  if FMap then begin
    if Assigned(FFMap) then
      FTiles.SetupFont(FFMap);

    with Map.ContinentRect do begin
      x:=(Left + Right) div 2;
      y:=(Top + Bottom) div 2;
    end;

    FTiles.TextOut(Map.Name, x, y, clBlack, FMColor);
  end;
end;

// Post processing of the sectors
procedure TPrinter.PostSector(const Sector:TGw2Sector);
begin
  if Assigned(FFSector) then
    FTiles.SetupFont(FFSector);

  FTiles.TextOut(Sector.Name, Round(Sector.X), Round(Sector.Y), clBlack, FSColor);
end;

// Post processing of the regions
procedure TPrinter.PostRegion(const Region:TGw2Region);
begin
  Region.iter(@PostMap);

  if FRegion then begin
    if Assigned(FFRegion) then
      FTiles.SetupFont(FFRegion);
    FTiles.TextOut(Region.Name, Region.Coordinate.X, Region.Coordinate.Y, clBlack, FRColor);
  end;
end;

end.

