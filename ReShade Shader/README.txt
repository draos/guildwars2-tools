 This shader uses ReShade (http://reshade.me (27.10.2015)) to perform color manipulations. ReShade is an advanced, fully generic post-processing injector for games and video software. This software implements the algorithm from http://daltonize.org (22.10.2015) and Hu, Yinghua. "Visual simulating dichromatic vision in CIE space." GRAPP. 2006 to perform simulations of color blindnesses.
   

INSTALLATION:
-------------
 - Install ReShade from reshade.me
 - Copy the ColorBlindness folder, the ColorBlindness.cfg and the ColorBlindness.undef into the ReShade folder
 - Append the following line to the Pipeline.cfg file
#include EFFECT(ColorBlindness, Effect)
 - Open the ColorBlindness.cfg to configurate the shader

SETTINGS:
---------
 - USE_ColorBlindness:
    Activates (1) or deactivates (0) the shader

The following settings are sorted and can only be activated if the previous ones are
disabled.
 - PROTANOPIA, DEUTERANOPIA and TRITANOPIA:
    Simulates the red, green or blue color blindness.

 - L_CONE_MONOCHROMACY, M_CONE_MONOCHROMACY and S_CONE_MONOCHROMACY:
    Simulates a monochromacy where only the red, green or blue cones are the only left ones.

 - ACHROMATOPIA:
    Simulates a monochromacy where no cones are left.

 - MATRIX:
    Enables the color matrix mode and use the settings for the red, green and blue
    manipulation.

The following options can be used to configurate the behaviour of the protanopia, deuteranopia and tritanopia settings.
 - EASY:
   Activate it to use an easy matrix for the simulation of the red, green and blue color blindness.

 - DALTONIZE
   Use a daltonizing algorithm to make the differences between colors better viewable.

The following settings will only be used in the MATRIX mode and define the red manipulation:
  RED_R - Influence of the red color to the red output channel
  RED_G - Influence of the green color to the red output channel
  RED_B - Influence of the blue color to the red output channel
  RED_C - Constant influenceto the red channel of the output color

The following settings will only be used in the MATRIX mode and define the green manipulation:
  GREEN_R - Influence of the red color to the green output channel
  GREEN_G - Influence of the green color to the green output channel
  GREEN_B - Influence of the blue color to the green output channel
  GREEN_C - Constant influenceto the green channel of the output color

The following settings will only be used in the MATRIX mode and define the blue manipulation:
  BLUE_R - Influence of the red color to the blue output channel
  BLUE_G - Influence of the green color to the blue output channel
  BLUE_B - Influence of the blue color to the blue output channel
  BLUE_C - Constant influenceto the red channel of the output color