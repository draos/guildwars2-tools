NAMESPACE_ENTER(CMFX)

#include "ReShade/ColorBlindness.cfg"

#if USE_ColorBlindness == 1

// Definition of dichromacy
#define DICHROMACY (PROTANOPIA || DEUTERANOPIA || TRITANOPIA)
// Definition of monochromacy as cone and rod monochromacy
#define MONOCHROMACY (L_CONE_MONOCHROMACY || M_CONE_MONOCHROMACY || S_CONE_MONOCHROMACY || ACHROMATOPIA)

// Check if the daltonization of a dichromancy is chosen
#if DALTONIZE && DICHROMACY
#include "Daltonize.h"

// Check for normal dichromacy
#elif DICHROMACY
// Choose the simulation method
#if EASY
#include "Simulate_Easy.h"
#else
#include "Simulate.h"
#endif

// Check for L-cone monochromacy
#elif L_CONE_MONOCHROMACY
float4 simulate(float4 col)
{
    // Just erase g and b channel
    return float4(col.r, 0.0, 0.0, col.a);
}

// Check for M-cone monochromacy
#elif M_CONE_MONOCHROMACY
float4 simulate(float4 col)
{
    // Just erase r and b channel
    return float4(0.0, col.g, 0.0, col.a);
}

// Check for S-cone monochromacy
#elif S_CONE_MONOCHROMACY
float4 simulate(float4 col)
{
    // Just erase r and g channel
    return float4(0.0, 0.0, col.b, col.a);
}
#elif ACHROMATOPIA
float4 simulate(float4 col)
{
    // Convert to monochrome using sRGB WhitePoint
    float x = col.r * 0.212656 + col.g * 0.715158 + col.b * 0.072186;
    return float4(x, x, x, col.a);
}
#else
#define A float3x3(RED_R, RED_G, RED_B, GREEN_R, GREEN_G, GREEN_B, BLUE_R, BLUE_G, BLUE_B)
#define B float3(RED_C, GREEN_C, BLUE_C)
#endif

// Main function of the post-processor
float4 FilterBlend(in float4 pos : SV_Position, in float2 texcoord : TEXCOORD) : COLOR
{
    float4 col = tex2D(RFX_backbufferColor, texcoord);

#if DICHROMACY && DALTONIZE
    col = daltonize(col);

#elif DICHROMACY || MONOCHROMACY
    col = simulate(col);

#elif MATRIX // Apply the color matrix
    col.rgb = clamp(mul(col.rgb, A) + B, 0.0, 1.0);
#endif

    // Return the saturated color
    return saturate(col);
};


technique Filter_Tech <bool enabled = true; int toggle = CB_ToggleKey; >
{

    pass G 
    {
        VertexShader = RFX_VS_PostProcess;
        PixelShader = FilterBlend;
    }

}

#undef DICHROMACY
#undef MONOCHROMACY

#endif

#include "ReShade/ColorBlindness.undef"

NAMESPACE_LEAVE()
