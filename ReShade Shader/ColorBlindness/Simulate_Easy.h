// Declaration of the conversion matrices between RGB and LMS
#define RGB2LMS_ float3x3(17.8824, 43.5161,4.11935, 3.45565,27.1554,3.86714, 0.0299566,0.184309,1.46709)
#define LMS2RGB_ float3x3(0.08094, -0.13054, 0.11672, -0.01025, 0.05402, -0.11361, -0.00037, -0.00412, 0.69351)

// Conversion macro between RGB and LMS
#define LMS2RGB(X) (mul(LMS2RGB_, X))
#define RGB2LMS(X) (mul(RGB2LMS_, X))

#if PROTANOPIA
#define CVD float3x3(0.0, 2.02344, -2.52581, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)
#elif DEUTERANOPIA
#define CVD float3x3(1.0, 0.0, 0.0, 0.494207, 0.0, 1.24827, 0.0, 0.0, 1.0)
#elif TRITANOPIA
#define CVD float3x3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, -0.395913, 0.801109, 0.0)
#endif


float4 simulate(float4 col)
{
    // Convert to LMS space
    float3 lms = RGB2LMS(col.rgb);
    // Simulate color blindness
    lms = mul(CVD, lms);
    // Convert back to rgb
    col.rgb = LMS2RGB(lms);
    return col;
}

#undef RGB2LMS_
#undef LMS2RGB_

#undef LMS2RGB
#undef RGB2LMS

#undef CVD