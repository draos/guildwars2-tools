
// Algorithm from:
//   - http://daltonize.org (22.10.2015)
//   - Hu, Yinghua. "Visual simulating dichromatic vision in CIE space." GRAPP. 2006

// Declaration of the conversion matrices between RGB and XYZ
#define XYZ2RGB_ float3x3(3.2405, -1.5372, -0.4985, -0.9693, 1.8760, 0.0416, 0.0556, -0.2040, 1.0573)
#define RGB2XYZ_ float3x3(0.4125,  0.3576,  0.1804,  0.2127, 0.7152, 0.0721, 0.0193,  0.1192, 0.9502)

// Conversion macro between RGB and XYZ
#define XYZ2RGB(X) (mul(XYZ2RGB_, X))
#define RGB2XYZ(X) (mul(RGB2XYZ_, X))

// Confuse point declaration
#if PROTANOPIA
#define Confuse float2(0.747, 0.253)
#define M float(1.273463)
#define YInt float(-0.073894)
#elif DEUTERANOPIA
#define Confuse float2(1.080, -0.080)
#define M float(0.968437)
#define YInt float(0.003331)
#elif TRITANOPIA
#define Confuse float2(0.171, 0.0)
#define M float(0.062921)
#define YInt float(0.292119)
#endif

// Simulate a color blindness
float4 simulate(float4 col)
{
    // Convert source color into XYZ color space
    float3 rgb = pow(max(0.0, col.rgb), 2.2);
    float3 xyz = RGB2XYZ(rgb);

    // Convert XYZ into xyY Chromacity Coordinates(xy) and Liminance (Y)
    float s = xyz.x + xyz.y + xyz.z;
    float2 chroma = xyz.xy / s;

    // Generate the "Confusion Line" between the source color and the confusion Point
    float m = (chroma.y - Confuse.y) / (chroma.x - Confuse.x);
    float yint = chroma.y - chroma.x * m;

    // How far the xy coords deviate from the simulation
    float deviate_x = (YInt - yint) / (m - M);
    float deviate_y = m * deviate_x + yint;

    // Compute the simulated color's XYZ coords
    xyz.xz = float2(deviate_x, 1.0 - deviate_x - deviate_y) * xyz.y / deviate_y;

    // Difference between simulated color and neutral gray
    float3 diff = float3(0.312713, 0.0, 0.352871) * xyz.y / 0.329016 - float3(xyz.x, 0.0, xyz.z);

    // Convert to RGB color space
    diff = XYZ2RGB(diff);
    rgb = XYZ2RGB(xyz);

    // Compensate simulated color towards a neutral fit in RGB space
    float adjust = 0.0;
    for (int i=0; i<2; i++)
    {
        float fit = ((rgb[i] < 0.0 ? 0.0 : 1.0) - rgb[i]) / diff[i];
        fit = (fit > 1.0 || fit < 0.0) ? 0.0 : fit;
        adjust = max(adjust, fit);
    }

    // Shift proportional to the greatest shift
    rgb += adjust * diff;
    rgb = clamp(rgb, 0.0, 1.0);

    // Apply gamma correction
    col.rgb = pow(rgb, 1.0 / 2.2);
    return col;
}

#undef XYZ2RGB_
#undef RGB2XYZ_

#undef XYZ2RGB
#undef RGB2XYZ

#undef Confuse
#undef M
#undef YInt