unit uEmblem;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, GraphType, StdCtrls, Menus, ExtDlgs, BGRABitmap, BGRABitmapTypes,
  simpleinternet, fpjson, jsonparser, uColorBox, uImageBox;


type
  { TAPIThread }
  TAPIEvent      = procedure(root:TJSONArray) of object;
  TAPIThread     = class(TThread)
  private
    FRoot        :TJSONData;
    FEvent       :TAPIEvent;
    FUrl         :String;
    FDone        :Boolean;
  protected
    procedure    Execute;override;
  public
    constructor  Create(Url:String;Event:TAPIEvent);overload;

    function     CallAction:Boolean;
  end;

  // Enumeration for all scheme colors
  TScheme         = (sPrimary, sBack, sFirst, sSecond);

  { TEmblem }
  TEmblem         = class(TForm)
    BackItem      : TMenuItem;
    ForeItem      : TMenuItem;
    BackHori      : TMenuItem;
    BackVert      : TMenuItem;
    ForeHori      : TMenuItem;
    ForeVert      : TMenuItem;
    Load          : TButton;
    Material      : TMenuItem;
    Cloth         : TMenuItem;
    Leather       : TMenuItem;
    German        : TMenuItem;
    English       : TMenuItem;
    spacer0       : TMenuItem;
    Language      : TMenuItem;
    Metal         : TMenuItem;
    SortBySat     : TMenuItem;
    SortByName    : TMenuItem;
    SortByHue     : TMenuItem;
    SortSpecial   : TMenuItem;
    SortItem      : TMenuItem;
    SaveDialog    : TSavePictureDialog;
    Clock         : TTimer;
    ValidColors   : TMenuItem;
    Gw2Colors     : TMenuItem;
    PaletteItem   : TMenuItem;
    Spacer        : TMenuItem;
    OptionMenu    : TPopupMenu;
    Save          : TButton;
    Options       : TButton;
    Emblem        : TPaintBox;
    ColorBox      : TGroupBox;
    OptionBox     : TGroupBox;
    Scheme        : TGroupBox;
    procedure     Change(Sender: TObject);
    procedure     ClockTimer(Sender: TObject);
    procedure     languageClick(Sender: TObject);
    procedure     materialClick(Sender: TObject);
    procedure     FlipClick(Sender: TObject);
    procedure     FormCreate(Sender: TObject);
    procedure     FormDestroy(Sender: TObject);
    procedure     EmblemPaint(Sender: TObject);
    procedure     DragDrop(Sender,Source:TObject;X,Y:Integer);overload;
    procedure     DragOver(Sender,Source:TObject;X,Y:Integer;State:TDragState;var Accept:Boolean);overload;
    procedure     Gw2ColorsClick(Sender: TObject);
    procedure     LoadClick(Sender: TObject);
    procedure     OptionsClick(Sender: TObject);
    procedure     SaveClick(Sender: TObject);
    procedure     SortByClick(Sender: TObject);
  private
    // A api thread
    FThread       :TAPIThread;
    // The current emblem
    FEmblem       :TBGRACustomBitmap;

    // The colored preview for the backgrounds and foregrounds
    FBacks        :TImageBox;
    FFores        :TImageBox;

    // Flipping flags
    FFlip         :record
      // Background vertical
      BackVert,
      // Background horizontal
      BackHori,
      // Foreground vertical
      ForeVert,
      // Foreground horizontal
      ForeHori    :Boolean;
    end;

    // Color box
    FColorBox     :TColorBox;
    // Current scheme colors
    FScheme       :Array[TScheme] of TColorItem;

    // Reload the emblem
    procedure     Rebuild;
    // Update the emblem given by the fore- and background images
    procedure     BuildEmblem(Fore, Back:TBGRACustomBitmap);
    // Load a guild from the web api given by name
    procedure     LoadGuild(GuildName:String);
    // Load an emblem from the JSON data from the api
    procedure     LoadEmblem(root:TJSONObject);
    // Update the colors
    procedure     UpdateColor;
    // Update and add new colors
    procedure     UpdateColors(root:TJSONArray);
    // Update the current scheme
    procedure     UpdateScheme;
  end;

var cEmblem       :TEmblem;

    Language     :String = '';

implementation

{$R *.lfm}

function min(a, b:Integer):Integer;
begin if a < b then Result:=a else Result:=b;end;

function max(a, b:Integer):Integer;
begin if a > b then Result:=a else Result:=b;end;

function max(a, b:Byte):Byte;
begin if a > b then Result:=a else Result:=b;end;

// Blending function which uses the alpha channel of the first pixel
function blend(a, b:TBGRAPixel):TBGRAPixel;
var k    :Integer;
begin
  k:=a.alpha;
  // New alpha channel as maximum
  Result.alpha:=max(a.alpha, b.alpha);
  // Blend with alpha channel
  Result.red:=  (k * a.red   + (255 - k) * b.red) div 255;
  Result.green:=(k * a.green + (255 - k) * b.green) div 255;
  Result.blue:= (k * a.blue  + (255 - k) * b.blue) div 255;
end;

// Apply a color using a scale value and return the bgra-pixel representation
function colorize(Color:TRGB; Scale:Integer):TBGRAPixel;
begin
  // Alpha channel uses the scale value
  Result.alpha:=Scale and $FF;
  // Multiply the color with the alpha and divide through the greatest value
  Result.red:=(Color.R * Result.alpha) div 255;
  Result.green:=(Color.G * Result.alpha) div 255;
  Result.blue:=(Color.B * Result.alpha) div 255;
end;

{ TAPIThread }
constructor TAPIThread.Create(Url:String;Event:TAPIEvent);
begin
  inherited Create(False);

  FRoot:=Nil;
  FUrl:=Url;
  FEvent:=Event;
end;

procedure TAPIThread.Execute;
var res   :String;
begin
  FDone:=False;
  // Download the data
  res:=retrieve(FUrl);

  // Parse the data
  with TJSONParser.Create(res) do
    try
      FRoot:=Parse;
    finally
      Free;
    end;

  // Download done
  FDone:=True;

  // Wait til termination
  while not Terminated do
    Sleep(1000);

  // Free the internal data
  if FRoot <> Nil then
    FRoot.Free;
end;

// Must be called from the main thread
function TAPIThread.CallAction:Boolean;
begin
  Result:=FDone;

  if (FDone)and(FEvent <> Nil)and(FRoot.JSONType = jtArray) then
    FEvent(Froot as TJSONArray);
end;

{ TEmblem }
// Rebuild the cEmblem
procedure TEmblem.Rebuild;
var fore, back  :TBGRACustomBitmap;
begin
  // Get the foreground image from the imagebox
  fore:=FFores.SelectedImage;
  if fore <> Nil then begin
    // Flip the foreground image using the flipping flags
    if FFlip.ForeHori then fore.HorizontalFlip;
    if FFlip.ForeVert then fore.VerticalFlip;
  end;

  // Get the background image from the imagebox
  back:=FBacks.SelectedImage;
  if back <> Nil then begin
    // Flip the background image using the flipping flags
    if FFlip.BackHori then back.HorizontalFlip;
    if FFlip.BackVert then back.VerticalFlip;
  end;

  // Only build the emblem if both image exists
  if (fore <> Nil) and (back <> Nil) then
    BuildEmblem(fore, back);

  // Free the images
  if fore <> Nil then fore.Free;
  if back <> Nil then back.Free;
end;

// Update the colors using the JSON array from the api
procedure TEmblem.UpdateColors(root:TJSONArray);
var i       :Integer;
    shm     :TScheme;
    remover :TColorItem;
begin
  // Clear the colorbox
  FColorBox.Clear;

  i:=0;

  FColorBox.BeginUpdate;
  // Iterate over all colors in the array and append to the colorbox
  while i < root.Count do begin
    if root[i].JSONType = jtObject then
      FColorBox.AddColor(root[i] as TJSONObject);

    Inc(i);
  end;

  // Resort the colorbox
  FColorBox.Sort;

  // Try to read the 'dye remover' color as default
  remover:=FColorBox.ColorByID[1];
  if remover <> Nil then begin
    // Apply the default color on the color scheme
    for shm in TScheme do
      FScheme[shm].Copy(remover);
    // Background should be abyss
    FScheme[sBack].Copy(FColorBox.ColorByID[473]);

    // Update the schemes and apply on the previews
    UpdateScheme;
  end;
  Rebuild;
  Refresh;
end;

// Download the colors from the api and update the colors
procedure TEmblem.UpdateColor;
begin
  // Download from api
  FThread:=TAPIThread.Create('https://api.guildwars2.com/v2/colors?ids=all&lang='+uEmblem.Language, @UpdateColors);

  Clock.Enabled:=True;
end;

// Load an cEmblem from the JSON data from the api
procedure TEmblem.LoadEmblem(root:TJSONObject);
var flags :TJSONArray;
    i     :Integer;
begin
  // Only if valid
  if root <> Nil then begin
    // Reset the flipping flags
    FFlip.BackHori:=False;
    FFlip.BackVert:=False;
    FFlip.ForeHori:=False;
    FFlip.ForeVert:=False;

    // Read the flags from the JSON data
    flags:=root.Find('flags', jtArray) as TJSONArray;
    if flags <> Nil then begin
      i:=0;
      // Parse the flags
      while i < flags.Count do begin
        // Update the flipping flags if they exists in the JSON array
        if (flags[i].JSONType = jtString) and (flags[i].AsString = 'FlipBackgroundHorizontal') then
          FFlip.BackHori:=True
        else if (flags[i].JSONType = jtString) and (flags[i].AsString = 'FlipBackgroundVertical') then
          FFlip.BackVert:=True
        else if (flags[i].JSONType = jtString) and (flags[i].AsString = 'FlipForegroundHorizontal') then
          FFlip.ForeHori:=True
        else if (flags[i].JSONType = jtString) and (flags[i].AsString = 'FlipForegroundVertical') then
          FFlip.ForeVert:=True;
        Inc(i);
      end;
   end;

   // Read the fore- and background id from the JSON object
   FFores.Selected:=root.Get('foreground_id', 0);
   FBacks.Selected:=root.Get('background_id', 0);

   // Read the color scheme from the JSON object
   FScheme[sFirst].Copy(FColorBox.ColorByID[root.Get('foreground_primary_color_id', 0)]);
   FScheme[sSecond].Copy(FColorBox.ColorByID[root.Get('foreground_secondary_color_id', 0)]);
   FScheme[sPrimary].Copy(FColorBox.ColorByID[root.Get('background_color_id', 0)]);

   // Update the interface
   UpdateScheme;
   // Rebuild the emblem
   Rebuild;
  end;
end;

// Load guild informations from api
procedure TEmblem.LoadGuild(GuildName:String);
var res   :String;
    root  :TJSONData;
begin
  // Get the guild information from the api
  try
    res:=retrieve('https://api.guildwars2.com/v1/guild_details?guild_name='+GuildName);
  except
    Exit;
  end;

  root:=Nil;
  // Parse the information
  with TJSONParser.Create(res) do
    try
      root:=Parse;
    finally
      Free;
    end;

  // Load the emblem if possible
  if (root <> Nil) and (root.JSONType = jtObject) then
    LoadEmblem((root as TJSONObject).Find('emblem', jtObject) as TJSONObject);
end;

// Update the current color scheme
procedure TEmblem.UpdateScheme;
begin
  // Apply the first foreground color to the previews
  FFores.ForeColorA:=FScheme[sFirst].Color;
  FBacks.ForeColorA:=FScheme[sFirst].Color;

  // Apply the second foreground color to the previews
  FFores.ForeColorB:=FScheme[sSecond].Color;
  FBacks.ForeColorB:=FScheme[sSecond].Color;

  // Apply the primary background color to the previews
  FFores.PrimaryColor:=FScheme[sPrimary].Color;
  FBacks.PrimaryColor:=FScheme[sPrimary].Color;

  // Apply the background color to the previews
  FFores.BackColor:=FScheme[sBack].Color;
  FBacks.BackColor:=FScheme[sBack].Color;
end;

// Recreate the guild cEmblem
procedure TEmblem.BuildEmblem(Fore, Back:TBGRACustomBitmap);
var dst, srcF, srcB :PBGRAPixel;
    i               :Integer;
    count           :Integer;
    mapFor1, mapFor2,
    mapBack         :Array[0..255] of TBGRAPixel;
begin
  // Precalculate the color mapping
  for i:=0 to 255 do begin
    mapFor1[i]:=colorize(FScheme[sFirst].Color, i);
    mapFor2[i]:=colorize(FScheme[sSecond].Color, i);
    mapBack[i]:=colorize(FScheme[sPrimary].Color, i);
  end;

  // Get the pointer to the image data
  dst:=FEmblem.Data;
  srcF:=fore.Data;
  srcB:=back.Data;

  // Determine the number of pixels
  count:=min(min(back.NbPixels, FEmblem.NbPixels), fore.NbPixels);

  // Iterate over all pixels
  i:=0;
  while i < count do begin
    // Blend the first layer over the background
    dst^:=blend(mapFor1[srcF^.red], mapBack[srcB^.red]);
    // Blend the second layer over the result
    dst^:=blend(mapFor2[srcF^.green], dst^);

    // Increment the counter
    Inc(i);
    // Increment the pointer
    Inc(dst);
    Inc(srcF);
    Inc(srcB);
  end;

  // Refresh the emblem view
  Emblem.Refresh;
end;

// Create the form and initialize the data and controls
procedure TEmblem.FormCreate(Sender: TObject);
var shm     :TScheme;
begin
  // Enable double buffering
  DoubleBuffered:=True;

  // Create the emblem image
  FEmblem:=TBGRABitmap.Create(256, 256);

  // Create the imagebox for the background images
  FBacks:=TImageBox.Create(Self, IT_BACK);
  FBacks.Parent:=Self;
  FBacks.SetBounds(0, Emblem.Height, 512, 85);
  FBacks.OnChange:=@Change;

  // Create the imagebox for the foreground images
  FFores:=TImageBox.Create(Self, IT_FORE);
  FFores.Parent:=Self;
  FFores.SetBounds(0, Emblem.Height + FBacks.Height, 512, 85);
  FFores.OnChange:=@Change;

  // Create the scheme colors
  for shm in TScheme do begin
    FScheme[shm]:=TColorItem.Create(Self);
    with FScheme[shm] do begin
      Parent:=Scheme;
      ShowHint:=True;
      // Color changes are working over drag and drop
      OnDragDrop:=@Self.DragDrop;
      OnDragOver:=@Self.DragOver;
      case shm of
        sFirst:
          SetBounds(8, 0, 32, 32);
        sSecond:
          SetBounds(48, 0, 32, 32);
        sPrimary:
          SetBounds(8, 40, 32, 32);
        sBack:
          SetBounds(48, 40, 32, 32);
      end;
    end;
  end;

  // Create the colorbox
  FColorBox:=TColorBox.Create(Self);
  with FColorBox do begin
    Parent:=ColorBox;
    Align:=alClient;
    SetBounds(Emblem.Width, Scheme.Height, 256, Emblem.Height - Scheme.Height);
  end;

  languageClick(German);
end;

// Rebuild on changes
procedure TEmblem.Change(Sender: TObject);
begin
  Rebuild;
  Refresh;
end;

procedure TEmblem.ClockTimer(Sender: TObject);
begin
  if (FThread <> Nil) and (FThread.CallAction) then begin
    FThread.Terminate;
    FThread:=Nil;
    Clock.Enabled:=False;

    Rebuild;
    Refresh;
  end;
end;

// Change the current Language
procedure TEmblem.languageClick(Sender: TObject);
begin
  if Sender = German then begin
    Scheme.Caption:='Farbschema';
    ColorBox.Caption:='Farben';
    OptionBox.Caption:='Optionen';
    Options.Caption:='Einstellungen';
    Save.Caption:='Speichern';
    Load.Caption:='Lade Gilde';
    Language.Caption:='Sprache';
    German.Caption:='Deutsch';
    English.Caption:='Englisch';
    PaletteItem.Caption:='Farbpalette';
    Gw2Colors.Caption:='Alle Gw2 Farben';
    ValidColors.Caption:='Nur gültige Farben';
    Material.Caption:='Material';
    Cloth.Caption:='Stoff';
    Leather.Caption:='Leder';
    Metal.Caption:='Metall';
    SortItem.Caption:='Sortiere Farben';
    SortByName.Caption:='nach Namen';
    SortBySat.Caption:='nach Sättigung';
    SortByHue.Caption:='nach Farbwert';
    SortSpecial.Caption:='nach Helligkeit und Farbwert';
    BackItem.Caption:='Hintergrund spiegeln';
    BackHori.Caption:='Horizontal';
    BackVert.Caption:='Vertikal';
    ForeItem.Caption:='Vordergrund spiegeln';
    ForeHori.Caption:='Horizontal';
    ForeVert.Caption:='Vertikal';

    if uEmblem.Language <> 'de' then begin
      uEmblem.Language:='de';
      UpdateColor;
    end;

  end else if Sender = English then begin
    Scheme.Caption:='Color scheme';
    ColorBox.Caption:='Colors';
    OptionBox.Caption:='Options';
    Options.Caption:='Settings';
    Save.Caption:='Save emblem';
    Load.Caption:='Load Guild';
    Language.Caption:='Language';
    German.Caption:='German';
    English.Caption:='English';
    PaletteItem.Caption:='Color palette';
    Gw2Colors.Caption:='All Gw2 colors';
    ValidColors.Caption:='Only valid colors';
    Material.Caption:='Material';
    Cloth.Caption:='Cloth';
    Leather.Caption:='Leather';
    Metal.Caption:='Metal';
    SortItem.Caption:='Sort colors';
    SortByName.Caption:='by name';
    SortBySat.Caption:='by saturation';
    SortByHue.Caption:='by hue';
    SortSpecial.Caption:='by lightness and hue';
    BackItem.Caption:='Flip background';
    BackHori.Caption:='Horizontal';
    BackVert.Caption:='Vertical';
    ForeItem.Caption:='Flip foreground';
    ForeHori.Caption:='Horizontal';
    ForeVert.Caption:='Vertical';

    if uEmblem.Language <> 'en' then begin
      uEmblem.Language:='en';
      UpdateColor;
    end;
  end;
end;

// Handle a click on the Material properites
procedure TEmblem.materialClick(Sender: TObject);
begin
  // Change the Material
  if Sender = Cloth then
    FColorBox.Material:=M_CLOTH
  else if Sender = Leather then
    FColorBox.Material:=M_LEATHER
  else if Sender = Metal then
    FColorBox.Material:=M_METAL;

  Rebuild;
  Refresh;
end;

// Handle the click on the flipping settings
procedure TEmblem.FlipClick(Sender: TObject);
begin
  if Sender = BackHori then
    FFlip.BackHori:=not FFlip.BackHori;
  if Sender = BackVert then
    FFlip.BackVert:=not FFlip.BackVert;

  if Sender = ForeHori then
    FFlip.ForeHori:=not FFlip.ForeHori;
  if Sender = ForeVert then
    FFlip.ForeVert:=not FFlip.ForeVert;

  Rebuild;
  Refresh;
end;

// Destroy and free the controls
procedure TEmblem.FormDestroy(Sender: TObject);
var shm   :TScheme;
begin
  for shm in TScheme do
    FreeAndNil(FScheme[shm]);

  FEmblem.Free;
  FreeAndNil(FColorBox);
  FreeAndNil(FFores);
  FreeAndNil(FBacks);
end;

// Repaint the cEmblem
procedure TEmblem.EmblemPaint(Sender: TObject);
begin
  with FScheme[sBack].Color do
    Emblem.Canvas.Brush.Color:=RGBToColor(R, G, B);

  with Emblem do begin
    Canvas.Brush.Style:=bsSolid;
    Canvas.Pen.Style:=psClear;
    Canvas.Rectangle(0, 0, Width, Height);

    FEmblem.Draw(Canvas, 0, 0, False);
  end;
end;

// Drop the dragged color
procedure TEmblem.DragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  if (Source is TColorItem) and (Sender is TColorItem)then begin
    // Copy the color from the source of the drag
    (Sender as TColorItem).Copy(Source as TColorItem);

    // Update the schemes
    UpdateScheme;
    // Rebuild
    Rebuild;
  end;
end;

// Enable dragging for color items
procedure TEmblem.DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=(Source is TColorItem) and (Sender is TColorItem);
end;

// Rebuild the colorbox using all colors or only valid colors
procedure TEmblem.Gw2ColorsClick(Sender: TObject);
begin
  if Sender = Gw2Colors then
    FColorBox.HideInvalid:=False
  else if Sender = ValidColors then
    FColorBox.HideInvalid:=True;
end;

// Load a guild from the api
procedure TEmblem.LoadClick(Sender: TObject);
var s     :String;
begin
  if Sender is TButton then begin
    s:=InputBox((Sender as TButton).Caption, 'Welche Gilde wollen Sie laden?', '');

    if s <> '' then
      LoadGuild(s);
  end;
end;

// Open the settings menu
procedure TEmblem.OptionsClick(Sender: TObject);
var p     :TPoint;
begin
  if Sender is TButton then begin
    with Sender as TButton do
      p:=OptionBox.ClientToScreen(Point(Left, Top+ Height));
    OptionMenu.PopUp(p.x, p.y);
  end;
end;

// Save the current cEmblem to a chosen file
procedure TEmblem.SaveClick(Sender: TObject);
begin
  if SaveDialog.Execute then
    FEmblem.SaveToFile(SaveDialog.FileName);
end;

// Resort the colors
procedure TEmblem.SortByClick(Sender: TObject);
begin
  if Sender = SortByName then
    FColorBox.Sortation:=CS_NAME
  else if Sender = SortBySat then
    FColorBox.Sortation:=CS_SATURATION
  else if Sender = SortByHue then
    FColorBox.Sortation:=CS_HUE
  else if Sender = SortSpecial then
    FColorBox.Sortation:=CS_STANDARD;

  // Resort
  FColorBox.Sort;
end;

end.

