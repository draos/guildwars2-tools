unit uImageBox;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, Forms, BGRAGraphicControl, uColorBox, BGRABitmap,
  Graphics, fgl, Controls, windows, BGRABitmapTypes;

type
  // Image types for back- and foreground
  TImageType     = (IT_BACK, IT_FORE);

  { TImageItem }
  TImageItem     = class(TBGRAGraphicControl)
  private
    // Internal values
    FBack        :TRGB;
    FPrimary     :TRGB;
    FForA        :TRGB;
    FForB        :TRGB;
    FImg         :TBGRABitmap;
    FSelected    :Boolean;
    FType        :TImageType;
    FID          :Integer;

    // Reload the image
    procedure    Reload;
  protected
    // Return the preferred size
    procedure    CalculatePreferredSize(var PreferredWidth,PreferredHeight:integer;WithThemeSpace:Boolean);override;
    // Override the paint method
    procedure    Paint;override;
  public
    // Constructor
    constructor  Create(TheOwner:TComponent;Typ:TImageType;ID:Integer);overload;
    // Destructor
    destructor   Destroy;override;

    // Properties
    property     ImageType:TImageType read FType;
    property     ID:Integer read FID;
    property     BackColor:TRGB read FBack write FBack;
    property     PrimaryColor:TRGB read FPrimary write FPrimary;
    property     ForeAColor:TRGB read FForA write FForA;
    property     ForeBColor:TRGB read FForB write FForB;
    property     Selected:Boolean read FSelected write FSelected;
    property     Image:TBGRABitmap read FImg;
  end;

  // List of imageitems
  TImageItemList = specialize TFPGObjectList<TImageItem>;

  { TImageBox }
  TImageBox      = class(TScrollBox)
  private
    // Internal storage
    FObj         :TImageItemList;
    FBackA       :TRGB;
    FBackB       :TRGB;
    FForA        :TRGB;
    FForB        :TRGB;
    FType        :TImageType;
    FSelected    :Integer;
    FChange      :TNotifyEvent;

    // Getter
    function     getImage(ID:Integer):TBGRACustomBitmap;
    function     getSelected:TBGRACustomBitmap;

    // Setter
    procedure    setBack(Value:TRGB);
    procedure    setPrimary(Value:TRGB);
    procedure    setForeA(Value:TRGB);
    procedure    setForeB(Value:TRGB);
    procedure    select(Value:Integer);
    procedure    SelectItem(Sender:TObject);

    // Reload the images
    procedure    Reload;
  public
    // Constructor
    constructor  Create(TheOwner:TComponent;Typ:TImageType);overload;
    // Destructor
    destructor   Destroy;override;

    // Properties
    property     BackColor:TRGB read FBackB write setBack;
    property     PrimaryColor:TRGB read FBackA write setPrimary;
    property     ForeColorA:TRGB read FForA write setForeA;
    property     ForeColorB:TRGB read FForB write setForeB;
    property     BoxType:TImageType read FType;
    property     Selected:Integer read FSelected write select;
    property     SelectedImage:TBGRACustomBitmap read getSelected;
    property     OnChange:TNotifyEvent read FChange write FChange;
    property     Image[ID:Integer]:TBGRACustomBitmap read getImage;
  end;

implementation

// Constant values
const
  MAX_FORE = 243;
  MAX_BACK = 28;

// Build a rgb value
function RGB(r,g,b:Byte):TRGB;
begin
  Result.r:=r;
  Result.g:=g;
  Result.b:=b;
end;

{ TImageBox }
constructor TImageBox.Create(TheOwner:TComponent;Typ:TImageType);
begin
  inherited Create(TheOwner);

  FType:=Typ;
  FChange:=Nil;

  // Create the image list
  FObj:=TImageItemList.Create;

  // Apply the child sizing
  with ChildSizing do begin
    ControlsPerLine:=255;
    HorizontalSpacing:=2;
    Layout:=cclLeftToRightThenTopToBottom;

    VerticalSpacing:=2;
  end;

  // Set up the appearence
  HorzScrollBar.Smooth:=True;
  HorzScrollBar.Tracking:=True;
  VertScrollBar.Visible:=False;
  HorzScrollBar.Increment:=25;
  AutoSize:=False;

  // Reload the image box
  Reload;

  // Select the first image
  Select(1);
end;

// Destructor
destructor TImageBox.Destroy;
begin
  FreeAndNil(FObj);
  inherited Destroy;
end;

// Return a copy of the image given by the ID
function TImageBox.getImage(ID:Integer):TBGRACustomBitmap;
var i    :Integer;
begin
  Result:=Nil;
  i:=0;
  while i < FObj.Count do begin
    if FObj[i].ID = ID then begin
      Result:=FObj[i].Image;
      // Copy the image
      Result:=Result.Resample(Result.Width, Result.Height, rmSimpleStretch);
    end;
    Inc(i);
  end;
end;

// Return the current selected image
function TImageBox.getSelected:TBGRACustomBitmap;
begin
  Result:=getImage(FSelected);
end;

// Select a new image given by the OnClick event
procedure TImageBox.SelectItem(Sender:TObject);
begin
  if Sender is TImageItem then
    select((Sender as TImageItem).ID);
end;

// Select a item
procedure TImageBox.select(Value:Integer);
var i     :Integer;
begin
  if FSelected <> Value then begin
    i:=0;
    // Update the appearence
    while i < FObj.Count do begin
      // Deselect the old one
      if FObj[i].ID = FSelected then
        FObj[i].Selected:=False;

      // Select the new one
      if FObj[i].ID = Value then
        FObj[i].Selected:=True;

      Inc(i);
    end;
  end;

  // Update the variable
  if (FSelected <> Value)and(FChange <> Nil) then begin
    FSelected:=Value;
    // Call the change event if possible
    FChange(Self);
  end else
   FSelected:=Value;

  // Refresh the control
  Refresh;
end;

// Set the background color
procedure TImageBox.setBack(Value:TRGB);
var i     :Integer;
begin
  FBackB:=Value;

  // Update the color in all image items
  i:=0;
  while i < FObj.Count do begin
    FObj[i].BackColor:=Value;
    Inc(i);
  end;

  // Refresh the control
  Refresh;
end;

// Set the primary color
procedure TImageBox.setPrimary(Value:TRGB);
var i     :Integer;
begin
  FBackA:=Value;

  // Update the color in all image items
  i:=0;
  while i < FObj.Count do begin
    FObj[i].PrimaryColor:=Value;
    Inc(i);
  end;

  // Refresh the control
  Refresh;
end;

// Set the first foreground color
procedure TImageBox.setForeA(Value:TRGB);
var i     :Integer;
begin
  FForA:=Value;

  // Update the color to all image items
  i:=0;
  while i < FObj.Count do begin
    FObj[i].ForeAColor:=Value;
    Inc(i);
  end;

  // Refresh the control
  Refresh;
end;

// Set the second foreground color
procedure TImageBox.setForeB(Value:TRGB);
var i     :Integer;
begin
  FForB:=Value;

  // Update the color to all image items
  i:=0;
  while i < FObj.Count do begin
    FObj[i].ForeBColor:=Value;
    Inc(i);
  end;

  // Refresh the control
  Refresh;
end;

// Reload the iamge box
procedure TImageBox.Reload;
var i, k  :Integer;
    item  :TImageItem;
begin
  // Clear the list
  FObj.Clear;

  // Take the maximum value
  if FType = IT_BACK then
    k:=MAX_BACK
  else
    k:=MAX_FORE;

  // Iterate over all image id
  i:=1;
  while i <= k do begin
    // The background with ID 9 don't exist
    if (i <> 9) or (FType <> IT_BACK) then begin
      // Create the item from the resource
      item:=TImageItem.Create(Self, FType, i);
      item.Parent:=Self;
      item.OnClick:=@SelectItem;
      item.Selected:=False;
      FObj.Add(item);
    end;

    Inc(i);
  end;

  // Update the child sizing
  ChildSizing.ControlsPerLine:=FObj.Count;
end;

{ TImageItem }
constructor TImageItem.Create(TheOwner:TComponent;Typ:TImageType;ID:Integer);
begin
  inherited Create(TheOwner);

  // Create the image and set up the default type
  FType:=Typ;
  FImg:=TBGRABitmap.Create;

  // Set the default values
  FID:=ID;
  FBack:=RGB(0, 0, 0);
  FPrimary:=RGB(255, 255, 255);
  FForA:=RGB(255, 0, 0);
  FForB:=RGB(0, 255, 0);
  FType:=Typ;

  // Reload
  Reload;
end;

// Destructor
destructor TImageItem.Destroy;
begin
  FreeAndNil(FImg);
  inherited Destroy;
end;

// Paint procedure of the item
procedure TImageItem.Paint;
  // Blending function
  function blend(a, b:Integer;alpha:Byte):Byte;
  begin
    Result:=(alpha * a + (255 - alpha) * b) div 255;
  end;
var img     :TBGRACustomBitmap;
    pix     :PBGRAPixel;
    i       :Integer;
    r, g, b :Byte;
begin
  // Set up the canvas options
  Canvas.Brush.Style:=bsSolid;
  if FSelected then
    Canvas.Brush.Color:=clWhite
  else
    Canvas.Brush.Color:=clBlack;

  // Draw the background rectangle
  Canvas.Rectangle(0, 0, Width, Height);

  // Resample the image and create a temporary image
  img:=FImg.Resample(Width - 8, Height - 8, rmSimpleStretch);

  // Iterate over the pixel data
  pix:=img.Data;
  i:=0;
  while i < img.NbPixels do begin
    // Background
    if FType = IT_BACK then begin
      pix^.alpha:=pix^.red;
      pix^.green:=blend((FPrimary.G * pix^.red) div 255, FBack.G, pix^.alpha);
      pix^.blue:= blend((FPrimary.B * pix^.red) div 255, FBack.B, pix^.alpha);
      pix^.red:=  blend((FPrimary.R * pix^.red) div 255, FBack.R, pix^.alpha);
    end else begin
      // Foreground
      pix^.alpha:=max(pix^.red, pix^.green);
      r:=blend((FForB.R * pix^.green) div 255, FBack.R, pix^.green);
      r:=blend((FForA.R * pix^.red) div 255, r, pix^.red);

      g:=blend((FForB.G * pix^.green) div 255, FBack.G, pix^.green);
      g:=blend((FForA.G * pix^.red) div 255, g, pix^.red);

      b:=blend((FForB.B * pix^.green) div 255, FBack.b, pix^.green);
      b:=blend((FForA.B * pix^.red) div 255, b, pix^.red);

      pix^.green:=g;
      pix^.blue:=b;
      pix^.red:=r;
    end;

    Inc(i);
    Inc(pix);
  end;

  // Draw the image
  img.Draw(Canvas, 4, 4);

  // Free the image
  img.Free;
end;

// Return the preferred size
procedure TImageItem.CalculatePreferredSize(var PreferredWidth,PreferredHeight:integer;WithThemeSpace:Boolean);
begin
  PreferredWidth:=64;
  PreferredHeight:=64;
end;

// Reload the item
procedure TImageItem.Reload;
var res   :TResourceStream;
begin
  // Read the image from the resource
  if FType = IT_FORE then
    res:=TResourceStream.Create(HINSTANCE, 'FORE'+IntToStr(FID), RT_RCDATA)
  else
    res:=TResourceStream.Create(HINSTANCE, 'BACK'+IntToStr(FID), RT_RCDATA);

  try
    // Load the image
    FImg.LoadFromStream(res);
  finally
    // Free the resource
    res.Free;
  end;
end;

end.

