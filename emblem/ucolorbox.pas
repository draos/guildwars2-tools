unit uColorBox;

{$mode objfpc}{$H+}

{ GuildWars2 Tools - Collection of tools around the game Guild Wars 2.
    Emblem - Guild Emblem Creator and Editor
    Mumble Reader - Viewer of the data from the Mumble Interface
    Map Viewer - Tile-based map viewer and printer for Guild Wars 2
    Prediction - Tool for prediction of WvW rankings and scores
  Copyright (C) 2015-2016  draos.9574 or draos23@yahoo.de

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.}

interface

uses
  Classes, SysUtils, ExtCtrls, Controls, fpjson, jsonparser, forms,
  BGRABitmapTypes, Graphics, fgl, Windows;

type
  // RGB Representation
  TRGB           = record
    R, G, B      :Byte;
  end;
  PRGB           =^TRGB;

  // Container for the material from the api
  TAPIMaterial   = record
    RGB          :TRGB;
    Hue          :Integer;
    Saturation   :Single;
    Lightness    :Single;
  end;
  PAPIMaterial   = ^TAPIMaterial;

  // Container for the colors from the api
  TAPIColor      = record
    ID           :Integer;
    Name         :String;
    Cloth        :TAPIMaterial;
    Leather      :TAPIMaterial;
    Metal        :TAPIMaterial;
  end;
  PAPIColor      =^TAPIColor;

  // Enumeration of different materials
  TMaterial      = (M_CLOTH, M_LEATHER, M_METAL);

  // Sortation methods
  TColorSort     = (CS_GRAY, CS_HUE, CS_SATURATION, CS_NAME, CS_STANDARD);

  { TColorItem }
  TColorItem    = class(TShape)
  private
    // Internal storage
    FID          :Integer;
    FName        :String;
    FCloth       :TAPIMaterial;
    FLeather     :TAPIMaterial;
    FMetal       :TAPIMaterial;
    FMaterial    :TMaterial;

    // Getter
    function     getColor:TRGB;
    function     getGray:Byte;
    function     getHue:Integer;
    function     getSaturation:Single;
  protected
    // Overriden methods
    procedure    CalculatePreferredSize(var PreferredWidth,PreferredHeight:integer;WithThemeSpace:Boolean);override;
    procedure    MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);override;
  public
    // Constructors
    constructor  Create(Root:TJSONObject;TheOwner:TComponent);overload;
    constructor  Create(Data:TAPIColor;TheOwner:TComponent);overload;
    constructor  Create(Data:TColorItem;TheOwner:TComponent);overload;

    // Check if the color is valid (shown in the ingame emblem editor)
    function     Valid:Boolean;

    // Copy the internal data from another coloritem
    procedure    Copy(Value:TColorItem);

    // Properties
    property     Color:TRGB read getColor;
    property     Hue:Integer read getHue;
    property     Saturation:Single read getSaturation;
    property     ID:Integer read FID;
    property     Name:String read FName;
    property     Gray:Byte read getGray;
    property     Cloth:TAPIMaterial read FCloth;
    property     Leather:TAPIMaterial read FLeather;
    property     Metal:TAPIMaterial read FMetal;
    property     Material:TMaterial read FMaterial write FMaterial default M_CLOTH;
  end;

  // ObjectList of coloritems
  TColorList     = specialize TFPGObjectList<TColorItem>;

  TIteratorFunc  = procedure(Color:TColorItem) of object;

  { TColorBox }
  TColorBox      = class(TScrollBox)
  private
    // Internal storage
    FColors      :TColorList;
    FSort        :TColorSort;
    FHideInvalid :Boolean;

    // Getter
    function     getColorByID(ID:Integer):TColorItem;
    function     getColorByName(CName:String):TColorItem;
    procedure    setHideInvalid(HideInvalid:Boolean);
    // Setter
    procedure    setSort(Value:TColorSort);
    procedure    setMaterial(Value:TMaterial);
    // Add a new coloritem
    procedure    Add(Obj:TColorItem);
  protected
    // Overriden methods
    function     DoMouseWheel(Shift:TShiftState;WheelDelta:Integer;MousePos:TPoint):Boolean;override;
    procedure    MouseWheel(Sender:TObject;Shift:TShiftState;WheelDelta:Integer;MousePos:TPoint;var Handled:Boolean);
  public
    // Constructor
    constructor  Create(TheOwner:TComponent);override;
    // Destructor
    destructor   Destroy;override;

    // Add a new color
    procedure    AddColor(Data:TAPIColor);overload;
    procedure    AddColor(Data:TJSONObject);overload;

    // Begin and end update functions
    procedure    BeginUpdate;
    procedure    EndUpdate;

    // Methods
    procedure    Clear;
    procedure    Sort;

    // Iterate over all colors and apply a function
    procedure    Iterate(Func:TIteratorFunc);

    // Properties
    property     ColorByID[ID:Integer]:TColorItem read getColorByID;
    property     ColorByName[CName:String]:TColorItem read getColorByName;
    property     Sortation:TColorSort read FSort write setSort;
    property     HideInvalid:Boolean read FHideInvalid write setHideInvalid;
    property     Material:TMaterial write setMaterial;
  end;

implementation

// Function to build a rgb container
function RGB(R, G, B:Byte):TRGB;
begin
  Result.R:=R;
  Result.G:=G;
  Result.B:=B;
end;

// Compare 2 coloritems by the gray value
function compareGray(const a, b:TColorItem):Integer;
begin
  Result:=b.Gray - a.Gray;
end;

// Compare 2 coloritems by the hue value
function compareHue(const a, b:TColorItem):Integer;
begin
  Result:=b.Hue - a.Hue;
end;

// Compare 2 coloritems by the saturation
function compareSaturation(const a, b:TColorItem):Integer;
  function sgn(a:Single):Integer;
  begin
    if a < 0.0 then
      Result:=-1
    else if a > 0.0 then
      Result:=1
    else
      Result:=0;
  end;

begin
  Result:=sgn(b.Saturation - a.Saturation);
end;

// Compare 2 coloritems by the name
function compareName(const a, b:TColorItem):Integer;
begin
  Result:=CompareStr(b.Name, a.Name);
end;

// Compare 2 coloritems by the gray and if the gray values are equal by the hue
function compareSpecial(const a, b:TColorItem):Integer;
begin
  Result:=(Integer(b.Gray shl 9) + b.Hue) - (Integer(a.Gray shl 9) + a.Hue);
end;

{ TColorBox }
constructor TColorBox.Create(TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  // Create the coloritem list
  FColors:=TColorList.Create(True);

  // Set up the childsizing
  with ChildSizing do begin
    ControlsPerLine:=9;
    HorizontalSpacing:=2;
    Layout:=cclLeftToRightThenTopToBottom;

    VerticalSpacing:=2;
  end;

  // Default options
  FSort:=CS_STANDARD;
  FHideInvalid:=True;

  // Edit the appearence
  BorderStyle:=bsNone;
  VertScrollBar.Smooth:=True;
  VertScrollBar.Tracking:=True;
  HorzScrollBar.Visible:=False;
  AutoSize:=False;
end;

// Destructor
destructor TColorBox.Destroy;
begin
  FreeAndNil(FColors);
  inherited Destroy;
end;

// Scroll if the mouse wheel event triggers
function TColorBox.DoMouseWheel(Shift:TShiftState;WheelDelta:Integer;MousePos:TPoint):Boolean;
begin
  if WheelDelta < 0 then
    VertScrollBar.Position:=VertScrollBar.Position + 24
  else
    VertScrollBar.Position:=VertScrollBar.Position - 24;

  Result:=True;
end;

// Return a coloritem given by the ID
function TColorBox.getColorByID(ID:Integer):TColorItem;
var i    :Integer;
begin
  i:=0;
  Result:=Nil;
  while i < FColors.Count do begin
    if (FColors[i] as TColorItem).ID = ID then
      Result:=FColors[i] as TColorItem;

    Inc(i);
  end;
end;

// Return a coloritem given by the Name
function TColorBox.getColorByName(CName:String):TColorItem;
var i    :Integer;
begin
  i:=0;
  Result:=Nil;
  while i < FColors.Count do begin
    if (FColors[i] as TColorItem).Name = CName then
      Result:=FColors[i] as TColorItem;

    Inc(i);
  end;
end;

// Apply a new material
procedure TColorBox.setMaterial(Value:TMaterial);
var i     :Integer;
begin
  i:=0;
  while i < FColors.Count do begin
    FColors[i].Material:=Value;

    Inc(i);
  end;

  Invalidate;
end;

// Iterate over the colors and apply the function
procedure TColorBox.Iterate(Func:TIteratorFunc);
var i     :Integer;
begin
  if Func <> Nil then begin
    i:=0;
    while i < FColors.Count do begin
      Func(FColors[i]);
      Inc(i);
    end;
  end;
end;

// Set the HideInvalid option and resort
procedure TColorBox.setHideInvalid(HideInvalid:Boolean);
begin
  FHideInvalid:=HideInvalid;
  Sort;
end;

// Set the sort method and resort
procedure TColorBox.setSort(Value:TColorSort);
begin
  FSort:=Value;
  Sort;
end;

// Sort the list
procedure TColorBox.Sort;
var i     :Integer;
begin
  // Pick the right compare function
  case FSort of
    CS_GRAY: FColors.Sort(@compareGray);
    CS_HUE: FColors.Sort(@compareHue);
    CS_SATURATION: FColors.Sort(@compareSaturation);
    CS_NAME: FColors.Sort(@compareName);
  else
    FColors.Sort(@compareSpecial);
  end;

  // Reorder the colors and hide
  i:=0;
  BeginUpdate;
  try
    while i < FColors.Count do begin
      with FColors[i] do begin
        Visible:=(not FHideInvalid) or Valid;
        if Visible then begin
          Parent:=Self;
          SendToBack;
        end else
          Parent:=Nil;
      end;

      Inc(i);
    end;
  finally
    EndUpdate;
  end;

  // ReAlign;
  ReAlign;

  // Refresh
  Refresh;
end;

// Handle the mouse wheel
procedure TColorBox.MouseWheel(Sender:TObject;Shift:TShiftState;WheelDelta:Integer;MousePos:TPoint;var Handled:Boolean);
begin
  Handled:=DoMouseWheel(Shift, WheelDelta, MousePos);
end;

// begin the update
procedure TColorBox.BeginUpdate;
begin
  SendMessage(Handle, WM_SETREDRAW, 0, 0);
end;

// End the update
procedure TColorBox.EndUpdate;
begin
  SendMessage(Handle, WM_SETREDRAW, 1, 0);
  RedrawWindow(Handle, nil, 0, RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
end;

// Clear the color list
procedure TColorBox.Clear;
begin
  FColors.Clear;
end;

// Add a new item to the color list given by coloritem
procedure TColorBox.Add(Obj:TColorItem);
begin
  FColors.Add(Obj);
  //Obj.Parent:=Self;
  Obj.Visible:=(not FHideInvalid) or Obj.Valid;
  Obj.OnMouseWheel:=@MouseWheel;
end;

// Add a new color to the color list given by an api color structure
procedure TColorBox.AddColor(Data:TAPIColor);
begin
  Add(TColorItem.Create(Data, Self));
end;

// Add a new color to the color list given by a JSON object
procedure TColorBox.AddColor(Data:TJSONObject);
begin
  Add(TColorItem.Create(Data, Self));
end;

{ TColorItem }
constructor TColorItem.Create(Data:TColorItem;TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  // Set up the default values
  FID:=Data.FID;
  FName:=Data.FName;
  FCloth:=Data.FCloth;
  FLeather:=Data.FLeather;
  FMetal:=Data.FMetal;
  FMaterial:=Data.FMaterial;

  // Update the hint
  Hint:=FName;
  ShowHint:=True;

  // Update the color of the shape
  Brush.Color:=RGBToColor(FCloth.RGB.R, FCloth.RGB.G, FCloth.RGB.B);
end;

constructor TColorItem.Create(Root:TJSONObject;TheOwner:TComponent);
  // Read a RGB value from a JSON array
  procedure ReadRGB(data:TJSONArray;dst:PAPIMaterial);
    begin
      if (data <> Nil) and (data.Count >= 3) then begin
        if data[0].JSONType = jtNumber then
          dst^.RGB.R:=data[0].AsInteger;
        if data[1].JSONType = jtNumber then
          dst^.RGB.G:=data[1].AsInteger;
        if data[2].JSONType = jtNumber then
          dst^.RGB.B:=data[2].AsInteger;
      end;
    end;

    // Read a material from a JSON object
    procedure ReadMaterial(data:TJSONObject;dst:PAPIMaterial);
    begin
      if data <> Nil then
        ReadRGB(data.Find('rgb', jtArray) as TJSONArray, dst);
      dst^.Hue:=data.Get('hue', 0);
      dst^.Saturation:=data.Get('saturation', 0.0);
      dst^.Lightness:=data.Get('lightness', 0.0);
    end;
begin
  inherited Create(TheOwner);

  // Set up the default values
  FMaterial:=M_CLOTH;
  // Read the id and name
  FID:=root.Get('id', 0);
  FName:=root.Get('name', '');

  // Read the 3 materials
  ReadMaterial(root.Find('cloth', jtObject) as TJSONObject, @FCloth);
  ReadMaterial(root.Find('leather', jtObject) as TJSONObject, @FLeather);
  ReadMaterial(root.Find('metal', jtObject) as TJSONObject, @FMetal);

  // Update hte hint
  Hint:=FName;
  ShowHint:=True;

  // Update the shape color
  Brush.Color:=RGBToColor(FCloth.RGB.R, FCloth.RGB.G, FCloth.RGB.B);
end;

constructor TColorItem.Create(Data:TAPIColor;TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  // Update the default values
  FMaterial:=M_METAL;
  FID:=Data.ID;
  FName:=Data.Name;
  FCloth:=Data.Cloth;
  FLeather:=Data.Leather;
  FMetal:=Data.Metal;

  // Update the hint
  Hint:=FName;
  ShowHint:=True;

  // Update the color of the shape
  Brush.Color:=RGBToColor(FCloth.RGB.R, FCloth.RGB.G, FCloth.RGB.B);
end;

// Return of the color is valid (shown in the ingame emblem editor)
function TColorItem.Valid:Boolean;
begin
  case FID of
    // Only the following colors are valid
    4, 9, 11, 27, 47, 48, 93, 114, 121, 125, 130, 132, 136, 137, 139, 340, 443, 473, 584, 617, 673:
      Result:=True;
  else
    Result:=False;
  end;
end;

// Return the current color
function TColorItem.getColor:TRGB;
begin
  case FMaterial of
    M_CLOTH:
      Result:=FCloth.RGB;
    M_LEATHER:
      Result:=FLeather.RGB;
  else
    Result:=FMetal.RGB;
  end;
end;

// Return the current hue
function TColorItem.getHue:Integer;
begin
  case FMaterial of
    M_CLOTH:
      Result:=FCloth.Hue;
    M_LEATHER:
      Result:=FLeather.Hue;
  else
    Result:=FMetal.Hue;
  end;
end;

// Return the current saturation
function TColorItem.getSaturation:Single;
begin
  case FMaterial of
    M_CLOTH:
      Result:=FCloth.Saturation;
    M_LEATHER:
      Result:=FLeather.Saturation;
  else
    Result:=FMetal.Saturation;
  end;
end;

// Return the current gray value
function TColorItem.getGray:Byte;
begin
  case FMaterial of
    M_CLOTH:
      Result:=(FCloth.RGB.R + FCloth.RGB.G + FCloth.RGB.B) div 3;

    M_LEATHER:
      Result:=(FLeather.RGB.R + FLeather.RGB.G + FLeather.RGB.B) div 3;

    M_METAL:
      Result:=(FMetal.RGB.R + FMetal.RGB.G + FMetal.RGB.B) div 3;
  else
    Result:=0;
  end;
end;

// Copy the values of the given coloritem
procedure TColorItem.Copy(Value:TColorItem);
begin
  if Value <> Nil then begin
    FCloth:=Value.FCloth;
    FLeather:=Value.FLeather;
    FMetal:=Value.FMetal;
    FMaterial:=Value.FMaterial;
    FID:=Value.FID;
    FName:=Value.FName;
    // Apply hint
    Hint:=FName;
    // Apply color
    Brush.Color:=(Color.B shl 16) or (Color.G shl 8) or Color.R;
  end;
end;

// Calculate the preferred size
procedure TColorItem.CalculatePreferredSize(var PreferredWidth,PreferredHeight:integer;WithThemeSpace:Boolean);
begin
  PreferredHeight:=24;
  PreferredWidth:=24;
end;

// Use drag and drop on mouse down
procedure TColorItem.MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);

  BeginDrag(True);
end;

end.

